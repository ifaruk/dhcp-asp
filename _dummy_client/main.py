# !/usr/bin/env python3
import os
import threading
import time
from collections import namedtuple
from pprint import pprint

import grpc
import client_server_pb2
import client_server_pb2_grpc

Solution = namedtuple('Solution',
                      ['clingo_time', 'total_time', 'fluent', 'act_actuation', 'act_sensing', 'outcome', 'additional'])


def worker(func, arg, period):
    while (True):
        func(*arg)
        time.sleep(period)


def ping(stub):
    ''':type stub:client_server_pb2_grpc.ServerStub'''
    print()
    print("Ping")
    try:
        start = time.time()
        pong = stub.ping(client_server_pb2.Ping())
        duration = time.time() - start
        print("Ping: Delay " + str(duration * 1000) + "ms.")
    except Exception as e:
        print(e)


def get_machine_features(stub):
    ''':type stub:client_server_pb2_grpc.ServerStub'''
    print()
    print("Get Machine Features:")
    try:
        print("Features:" + str(stub.get_machine_features(client_server_pb2.Machine_feature_req())))
    except Exception as e:
        print(e)


def get_machine_state(stub):
    ''':type stub:client_server_pb2_grpc.ServerStub'''
    print()
    print("Get Machine State:")
    try:
        print("State:" + str(stub.get_machine_state(client_server_pb2.Machine_state_req())))
    except Exception as e:
        print(e)


def server_configuration(stub):
    ''':type stub:client_server_pb2_grpc.ServerStub'''
    print()
    print("Configuring Server:")
    try:
        print(stub.configure_server(client_server_pb2.Configure_server_req(
            domain=open("/home/f/Projects/dhcp-asp/problem_examples/doors/domain.lp", "r").read(),
            goal=open("/home/f/Projects/dhcp-asp/problem_examples/doors/goal.lp", "r").read(),
            dif=open("/home/f/Projects/dhcp-asp/problem_examples/doors/dif.dhcp", "r").read(),
            parallel_capacity=2)))
    except Exception as e:
        print(e)


def server_reset(stub):
    ''':type stub:client_server_pb2_grpc.ServerStub'''
    print()
    print("Resetting Server:")
    try:
        print(stub.reset_server(client_server_pb2.Reset_server_req()))
    except Exception as e:
        print(e)


def server_terminate(stub):
    ''':type stub:client_server_pb2_grpc.ServerStub'''
    print()
    print("Terminating Server:")
    try:
        print(stub.terminate_server(client_server_pb2.Terminate_server_req()))
    except Exception as e:
        print(e)


def new_time_solver(stub, pts, cts):
    '''
    :type stub:client_server_pb2_grpc.ServerStub
    :type pts:int Problem time span
    :type cts:int Constraint time span
    '''
    print()
    print("Creating time solver (PTS:" + str(pts) + ",CTS:" + str(cts) + "):")
    try:
        nt_solver_resp = stub.new_time_solver(client_server_pb2.New_time_solver_req(problem_time_span=pts,
                                                                                    constraint_time_span=cts))
        print(str(nt_solver_resp))
        return nt_solver_resp.solver_id
    except Exception as e:
        print(e)
        return None


def kill_time_solver(stub, solver_id):
    '''
    :type stub:client_server_pb2_grpc.ServerStub
    :type solver_id:int
    '''
    print()
    print("Kill time solver (Solver ID:" + str(solver_id) + "):")
    try:
        print(stub.kill_time_solver(client_server_pb2.Kill_time_solver_req(solver_id=solver_id)))
    except Exception as e:
        print(e)


def solve(stub, solver_id, constraints):
    '''
    :type stub:client_server_pb2_grpc.ServerStub
    :type solver_id:int
    :type constraints:list[list[str]]
    '''

    const_str = "{\n\t" + "\n\t".join(["[" + ";".join(time_step) + "]" for time_step in constraints]) + "\n}"
    print()
    print("Solve (Solver ID:" + str(solver_id) + ")\n" + const_str + "\n")

    try:
        solve_req = client_server_pb2.Solve_req()
        solve_req.solver_id = solver_id
        for time_step in constraints:
            new_ts = client_server_pb2.Time_step()
            new_ts.atoms.extend(time_step)
            solve_req.constraints.extend([new_ts])
        solve_resp = stub.solve(solve_req)

        s = Solution(solve_resp.clingo_time_ms,
                     solve_resp.processing_time_ms,
                     [[x for x in time_step.atoms] for time_step in solve_resp.fluent],
                     [[x for x in time_step.atoms] for time_step in solve_resp.act_actuation],
                     [[x for x in time_step.atoms] for time_step in solve_resp.act_sensing],
                     [[x for x in time_step.atoms] for time_step in solve_resp.outcome],
                     [[x for x in time_step.atoms] for time_step in solve_resp.additional])
        print("Solution:")
        pprint(s)
        return s
    except Exception as e:
        print(e)
        return None


def cancel(stub, solver_id):
    '''
    :type stub:client_server_pb2_grpc.ServerStub
    :type solver_id:int
    '''
    print()
    print("Cancel (Solver ID:" + str(solver_id) + "):")
    try:
        print(stub.cancel(client_server_pb2.Cancel_req(solver_id=solver_id)))
    except Exception as e:
        print(e)


if __name__ == '__main__':
    os.system("./generate.sh")
    channel = grpc.insecure_channel('localhost:10007')
    stub = client_server_pb2_grpc.ServerStub(channel)

    # get_machine_features(stub)
    # # server_configuration(stub)
    #
    # # threading.Thread(target=worker, args=(ping, (stub,), 2.0)).start()
    # # threading.Thread(target=worker, args=(get_machine_state, (stub,), 10.0)).start()
    #
    # server_reset(stub)
    # server_configuration(stub)

    # server_terminate(stub)

    # s1 = new_time_solver(stub, 70, 0)
    # s2 = new_time_solver(stub, 9, 1)
    # s3 = new_time_solver(stub, 10, 1)
    # s4 = new_time_solver(stub, 11, 1)
    sol = solve(stub, 9, [
        ["-opened(5)", "-opened(7)", "-opened(8)", "-opened(9)", "at(12)", "opened(0)", "opened(1)", "opened(10)",
         "opened(11)", "opened(12)", "opened(13)", "opened(14)", "opened(2)", "opened(20)", "opened(21)", "opened(22)",
         "opened(23)", "opened(24)", "opened(3)", "opened(4)", "opened(6)", "sense(opened(17))"],
        ["sensed(-opened(17))"]])
    pprint(sol.act_actuation)

    sol = solve(stub, 9, [
        ["-opened(19)", "-opened(6)", "-opened(7)", "-opened(8)", "-opened(9)", "at(13)", "opened(0)", "opened(1)",
         "opened(10)", "opened(11)", "opened(12)", "opened(13)", "opened(14)", "opened(2)", "opened(20)", "opened(21)",
         "opened(22)", "opened(23)", "opened(24)", "opened(3)", "opened(4)", "opened(5)", "sense(opened(18))"],
        ["sensed(-opened(18))"]])
    pprint(sol.act_actuation)

    sol = solve(stub, 9, [
        ["-opened(18)", "-opened(19)", "-opened(6)", "-opened(7)", "-opened(8)", "-opened(9)", "at(12)", "opened(0)",
         "opened(1)", "opened(10)", "opened(11)", "opened(12)", "opened(13)", "opened(14)", "opened(2)", "opened(20)",
         "opened(21)", "opened(22)", "opened(23)", "opened(24)", "opened(3)", "opened(4)", "opened(5)",
         "sense(opened(17))"], ["sensed(-opened(17))"]])
    pprint(sol.act_actuation)

    while True:
        sol = solve(stub, s1, [["at(0)", "opened(21)", "opened(33)", "opened(65)", "opened(77)", "opened(109)"]])
        pprint(sol.act_actuation)
        sol = solve(stub, s1, [["at(0)", "opened(15)", "opened(33)", "opened(65)", "opened(77)", "opened(109)"]])
        pprint(sol.act_actuation)
        sol = solve(stub, s1, [["at(0)", "opened(21)", "opened(33)", "opened(56)", "opened(79)", "opened(109)"]])
        pprint(sol.act_actuation)
        sol = solve(stub, s1, [["at(0)", "opened(15)", "opened(33)", "opened(56)", "opened(79)", "opened(109)"]])
        pprint(sol.act_actuation)
        sol = solve(stub, s1, [["at(0)", "opened(21)", "opened(33)", "opened(65)", "opened(79)", "opened(109)"]])
        pprint(sol.act_actuation)

    # pprint(solve(stub, s1, [["at(0)", "opened(5)", "opened(15)"], ["move(0)"]]))
    # pprint(solve(stub, s2, [["at(0)", "opened(5)", "opened(15)"], ["move(0)"]]))
    # pprint(solve(stub, s3, [["at(0)", "opened(5)", "opened(15)"], ["move(0)"]]))
    # pprint(solve(stub, s4, [["at(0)", "opened(5)", "opened(15)"], ["move(0)"]]))

    # threading.Thread(target=solve,args=(stub, s1, [["at(0)", "opened(5)", "opened(15)"], ["move(0)"]])).start()
    # print("Thread started")
    # time.sleep(1)
    # print("Canceling")
    # cancel(stub,s1)
    # print("Canceled")
    #
    #
    # while (True):
    #     time.sleep(1000)

    pass
