# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: client_server.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='client_server.proto',
  package='Scontrol',
  syntax='proto3',
  serialized_pb=_b('\n\x13\x63lient_server.proto\x12\x08Scontrol\"\x06\n\x04Ping\"\x06\n\x04Pong\"\x14\n\x12Server_feature_req\"\x8d\x01\n\x13Server_feature_resp\x12\x15\n\rno_processors\x18\x01 \x01(\r\x12\x1e\n\x16no_cores_per_processor\x18\x02 \x01(\r\x12\x1b\n\x13no_threads_per_core\x18\x03 \x01(\r\x12\x0f\n\x07\x63pu_MHz\x18\x04 \x01(\r\x12\x11\n\tmemory_MB\x18\x05 \x01(\r\"\x12\n\x10Server_state_req\"\x83\x01\n\x11Server_state_resp\x12\x18\n\x10\x61vg_load_one_min\x18\x01 \x01(\r\x12\x19\n\x11\x61vg_load_five_min\x18\x02 \x01(\r\x12\x1c\n\x14\x61vg_load_fifteen_min\x18\x03 \x01(\r\x12\x1b\n\x13\x61vailable_memory_MB\x18\x04 \x01(\r\"v\n\x14\x43onfigure_server_req\x12\x18\n\x10\x63lingo_arguments\x18\x01 \x03(\t\x12\x0e\n\x06\x64omain\x18\x02 \x01(\t\x12\x0c\n\x04goal\x18\x03 \x01(\t\x12\x0b\n\x03\x64if\x18\x04 \x01(\t\x12\x19\n\x11parallel_capacity\x18\x05 \x01(\r\"\x17\n\x15\x43onfigure_server_resp\"\x12\n\x10Reset_server_req\"\x13\n\x11Reset_server_resp\"\x16\n\x14Terminate_server_req\"\x17\n\x15Terminate_server_resp\"N\n\x13New_time_solver_req\x12\x19\n\x11problem_time_span\x18\x01 \x01(\r\x12\x1c\n\x14\x63onstraint_time_span\x18\x02 \x01(\r\")\n\x14New_time_solver_resp\x12\x11\n\tsolver_id\x18\x01 \x01(\r\")\n\x14Kill_time_solver_req\x12\x11\n\tsolver_id\x18\x01 \x01(\r\"\x17\n\x15Kill_time_solver_resp\"\x1a\n\tTime_step\x12\r\n\x05\x61toms\x18\x01 \x03(\t\"H\n\tSolve_req\x12\x11\n\tsolver_id\x18\x01 \x01(\r\x12(\n\x0b\x63onstraints\x18\x02 \x03(\x0b\x32\x13.Scontrol.Time_step\"\x9f\x02\n\nSolve_resp\x12\x13\n\x0bsatisfiable\x18\x01 \x01(\x08\x12#\n\x06\x66luent\x18\x02 \x03(\x0b\x32\x13.Scontrol.Time_step\x12*\n\ract_actuation\x18\x03 \x03(\x0b\x32\x13.Scontrol.Time_step\x12(\n\x0b\x61\x63t_sensing\x18\x04 \x03(\x0b\x32\x13.Scontrol.Time_step\x12$\n\x07outcome\x18\x05 \x03(\x0b\x32\x13.Scontrol.Time_step\x12\'\n\nadditional\x18\x06 \x03(\x0b\x32\x13.Scontrol.Time_step\x12\x16\n\x0e\x63lingo_time_ms\x18\x07 \x01(\r\x12\x1a\n\x12processing_time_ms\x18\x08 \x01(\r\"\x1f\n\nCancel_req\x12\x11\n\tsolver_id\x18\x01 \x01(\r\" \n\x0b\x43\x61ncel_resp\x12\x11\n\tcancelled\x18\x01 \x01(\x08\x32\xea\x05\n\x06Server\x12(\n\x04ping\x12\x0e.Scontrol.Ping\x1a\x0e.Scontrol.Pong\"\x00\x12T\n\x13get_server_features\x12\x1c.Scontrol.Server_feature_req\x1a\x1d.Scontrol.Server_feature_resp\"\x00\x12M\n\x10get_server_state\x12\x1a.Scontrol.Server_state_req\x1a\x1b.Scontrol.Server_state_resp\"\x00\x12U\n\x10\x63onfigure_server\x12\x1e.Scontrol.Configure_server_req\x1a\x1f.Scontrol.Configure_server_resp\"\x00\x12I\n\x0creset_server\x12\x1a.Scontrol.Reset_server_req\x1a\x1b.Scontrol.Reset_server_resp\"\x00\x12U\n\x10terminate_server\x12\x1e.Scontrol.Terminate_server_req\x1a\x1f.Scontrol.Terminate_server_resp\"\x00\x12R\n\x0fnew_time_solver\x12\x1d.Scontrol.New_time_solver_req\x1a\x1e.Scontrol.New_time_solver_resp\"\x00\x12U\n\x10kill_time_solver\x12\x1e.Scontrol.Kill_time_solver_req\x1a\x1f.Scontrol.Kill_time_solver_resp\"\x00\x12\x34\n\x05solve\x12\x13.Scontrol.Solve_req\x1a\x14.Scontrol.Solve_resp\"\x00\x12\x37\n\x06\x63\x61ncel\x12\x14.Scontrol.Cancel_req\x1a\x15.Scontrol.Cancel_resp\"\x00\x62\x06proto3')
)




_PING = _descriptor.Descriptor(
  name='Ping',
  full_name='Scontrol.Ping',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=33,
  serialized_end=39,
)


_PONG = _descriptor.Descriptor(
  name='Pong',
  full_name='Scontrol.Pong',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=41,
  serialized_end=47,
)


_SERVER_FEATURE_REQ = _descriptor.Descriptor(
  name='Server_feature_req',
  full_name='Scontrol.Server_feature_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=49,
  serialized_end=69,
)


_SERVER_FEATURE_RESP = _descriptor.Descriptor(
  name='Server_feature_resp',
  full_name='Scontrol.Server_feature_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='no_processors', full_name='Scontrol.Server_feature_resp.no_processors', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='no_cores_per_processor', full_name='Scontrol.Server_feature_resp.no_cores_per_processor', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='no_threads_per_core', full_name='Scontrol.Server_feature_resp.no_threads_per_core', index=2,
      number=3, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='cpu_MHz', full_name='Scontrol.Server_feature_resp.cpu_MHz', index=3,
      number=4, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='memory_MB', full_name='Scontrol.Server_feature_resp.memory_MB', index=4,
      number=5, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=213,
)


_SERVER_STATE_REQ = _descriptor.Descriptor(
  name='Server_state_req',
  full_name='Scontrol.Server_state_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=215,
  serialized_end=233,
)


_SERVER_STATE_RESP = _descriptor.Descriptor(
  name='Server_state_resp',
  full_name='Scontrol.Server_state_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='avg_load_one_min', full_name='Scontrol.Server_state_resp.avg_load_one_min', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='avg_load_five_min', full_name='Scontrol.Server_state_resp.avg_load_five_min', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='avg_load_fifteen_min', full_name='Scontrol.Server_state_resp.avg_load_fifteen_min', index=2,
      number=3, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='available_memory_MB', full_name='Scontrol.Server_state_resp.available_memory_MB', index=3,
      number=4, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=236,
  serialized_end=367,
)


_CONFIGURE_SERVER_REQ = _descriptor.Descriptor(
  name='Configure_server_req',
  full_name='Scontrol.Configure_server_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='clingo_arguments', full_name='Scontrol.Configure_server_req.clingo_arguments', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='domain', full_name='Scontrol.Configure_server_req.domain', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='goal', full_name='Scontrol.Configure_server_req.goal', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='dif', full_name='Scontrol.Configure_server_req.dif', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='parallel_capacity', full_name='Scontrol.Configure_server_req.parallel_capacity', index=4,
      number=5, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=369,
  serialized_end=487,
)


_CONFIGURE_SERVER_RESP = _descriptor.Descriptor(
  name='Configure_server_resp',
  full_name='Scontrol.Configure_server_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=489,
  serialized_end=512,
)


_RESET_SERVER_REQ = _descriptor.Descriptor(
  name='Reset_server_req',
  full_name='Scontrol.Reset_server_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=514,
  serialized_end=532,
)


_RESET_SERVER_RESP = _descriptor.Descriptor(
  name='Reset_server_resp',
  full_name='Scontrol.Reset_server_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=534,
  serialized_end=553,
)


_TERMINATE_SERVER_REQ = _descriptor.Descriptor(
  name='Terminate_server_req',
  full_name='Scontrol.Terminate_server_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=555,
  serialized_end=577,
)


_TERMINATE_SERVER_RESP = _descriptor.Descriptor(
  name='Terminate_server_resp',
  full_name='Scontrol.Terminate_server_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=579,
  serialized_end=602,
)


_NEW_TIME_SOLVER_REQ = _descriptor.Descriptor(
  name='New_time_solver_req',
  full_name='Scontrol.New_time_solver_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='problem_time_span', full_name='Scontrol.New_time_solver_req.problem_time_span', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='constraint_time_span', full_name='Scontrol.New_time_solver_req.constraint_time_span', index=1,
      number=2, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=604,
  serialized_end=682,
)


_NEW_TIME_SOLVER_RESP = _descriptor.Descriptor(
  name='New_time_solver_resp',
  full_name='Scontrol.New_time_solver_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='solver_id', full_name='Scontrol.New_time_solver_resp.solver_id', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=684,
  serialized_end=725,
)


_KILL_TIME_SOLVER_REQ = _descriptor.Descriptor(
  name='Kill_time_solver_req',
  full_name='Scontrol.Kill_time_solver_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='solver_id', full_name='Scontrol.Kill_time_solver_req.solver_id', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=727,
  serialized_end=768,
)


_KILL_TIME_SOLVER_RESP = _descriptor.Descriptor(
  name='Kill_time_solver_resp',
  full_name='Scontrol.Kill_time_solver_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=770,
  serialized_end=793,
)


_TIME_STEP = _descriptor.Descriptor(
  name='Time_step',
  full_name='Scontrol.Time_step',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='atoms', full_name='Scontrol.Time_step.atoms', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=795,
  serialized_end=821,
)


_SOLVE_REQ = _descriptor.Descriptor(
  name='Solve_req',
  full_name='Scontrol.Solve_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='solver_id', full_name='Scontrol.Solve_req.solver_id', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='constraints', full_name='Scontrol.Solve_req.constraints', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=823,
  serialized_end=895,
)


_SOLVE_RESP = _descriptor.Descriptor(
  name='Solve_resp',
  full_name='Scontrol.Solve_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='satisfiable', full_name='Scontrol.Solve_resp.satisfiable', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='fluent', full_name='Scontrol.Solve_resp.fluent', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='act_actuation', full_name='Scontrol.Solve_resp.act_actuation', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='act_sensing', full_name='Scontrol.Solve_resp.act_sensing', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='outcome', full_name='Scontrol.Solve_resp.outcome', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='additional', full_name='Scontrol.Solve_resp.additional', index=5,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='clingo_time_ms', full_name='Scontrol.Solve_resp.clingo_time_ms', index=6,
      number=7, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='processing_time_ms', full_name='Scontrol.Solve_resp.processing_time_ms', index=7,
      number=8, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=898,
  serialized_end=1185,
)


_CANCEL_REQ = _descriptor.Descriptor(
  name='Cancel_req',
  full_name='Scontrol.Cancel_req',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='solver_id', full_name='Scontrol.Cancel_req.solver_id', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1187,
  serialized_end=1218,
)


_CANCEL_RESP = _descriptor.Descriptor(
  name='Cancel_resp',
  full_name='Scontrol.Cancel_resp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='cancelled', full_name='Scontrol.Cancel_resp.cancelled', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1220,
  serialized_end=1252,
)

_SOLVE_REQ.fields_by_name['constraints'].message_type = _TIME_STEP
_SOLVE_RESP.fields_by_name['fluent'].message_type = _TIME_STEP
_SOLVE_RESP.fields_by_name['act_actuation'].message_type = _TIME_STEP
_SOLVE_RESP.fields_by_name['act_sensing'].message_type = _TIME_STEP
_SOLVE_RESP.fields_by_name['outcome'].message_type = _TIME_STEP
_SOLVE_RESP.fields_by_name['additional'].message_type = _TIME_STEP
DESCRIPTOR.message_types_by_name['Ping'] = _PING
DESCRIPTOR.message_types_by_name['Pong'] = _PONG
DESCRIPTOR.message_types_by_name['Server_feature_req'] = _SERVER_FEATURE_REQ
DESCRIPTOR.message_types_by_name['Server_feature_resp'] = _SERVER_FEATURE_RESP
DESCRIPTOR.message_types_by_name['Server_state_req'] = _SERVER_STATE_REQ
DESCRIPTOR.message_types_by_name['Server_state_resp'] = _SERVER_STATE_RESP
DESCRIPTOR.message_types_by_name['Configure_server_req'] = _CONFIGURE_SERVER_REQ
DESCRIPTOR.message_types_by_name['Configure_server_resp'] = _CONFIGURE_SERVER_RESP
DESCRIPTOR.message_types_by_name['Reset_server_req'] = _RESET_SERVER_REQ
DESCRIPTOR.message_types_by_name['Reset_server_resp'] = _RESET_SERVER_RESP
DESCRIPTOR.message_types_by_name['Terminate_server_req'] = _TERMINATE_SERVER_REQ
DESCRIPTOR.message_types_by_name['Terminate_server_resp'] = _TERMINATE_SERVER_RESP
DESCRIPTOR.message_types_by_name['New_time_solver_req'] = _NEW_TIME_SOLVER_REQ
DESCRIPTOR.message_types_by_name['New_time_solver_resp'] = _NEW_TIME_SOLVER_RESP
DESCRIPTOR.message_types_by_name['Kill_time_solver_req'] = _KILL_TIME_SOLVER_REQ
DESCRIPTOR.message_types_by_name['Kill_time_solver_resp'] = _KILL_TIME_SOLVER_RESP
DESCRIPTOR.message_types_by_name['Time_step'] = _TIME_STEP
DESCRIPTOR.message_types_by_name['Solve_req'] = _SOLVE_REQ
DESCRIPTOR.message_types_by_name['Solve_resp'] = _SOLVE_RESP
DESCRIPTOR.message_types_by_name['Cancel_req'] = _CANCEL_REQ
DESCRIPTOR.message_types_by_name['Cancel_resp'] = _CANCEL_RESP
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Ping = _reflection.GeneratedProtocolMessageType('Ping', (_message.Message,), dict(
  DESCRIPTOR = _PING,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Ping)
  ))
_sym_db.RegisterMessage(Ping)

Pong = _reflection.GeneratedProtocolMessageType('Pong', (_message.Message,), dict(
  DESCRIPTOR = _PONG,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Pong)
  ))
_sym_db.RegisterMessage(Pong)

Server_feature_req = _reflection.GeneratedProtocolMessageType('Server_feature_req', (_message.Message,), dict(
  DESCRIPTOR = _SERVER_FEATURE_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Server_feature_req)
  ))
_sym_db.RegisterMessage(Server_feature_req)

Server_feature_resp = _reflection.GeneratedProtocolMessageType('Server_feature_resp', (_message.Message,), dict(
  DESCRIPTOR = _SERVER_FEATURE_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Server_feature_resp)
  ))
_sym_db.RegisterMessage(Server_feature_resp)

Server_state_req = _reflection.GeneratedProtocolMessageType('Server_state_req', (_message.Message,), dict(
  DESCRIPTOR = _SERVER_STATE_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Server_state_req)
  ))
_sym_db.RegisterMessage(Server_state_req)

Server_state_resp = _reflection.GeneratedProtocolMessageType('Server_state_resp', (_message.Message,), dict(
  DESCRIPTOR = _SERVER_STATE_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Server_state_resp)
  ))
_sym_db.RegisterMessage(Server_state_resp)

Configure_server_req = _reflection.GeneratedProtocolMessageType('Configure_server_req', (_message.Message,), dict(
  DESCRIPTOR = _CONFIGURE_SERVER_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Configure_server_req)
  ))
_sym_db.RegisterMessage(Configure_server_req)

Configure_server_resp = _reflection.GeneratedProtocolMessageType('Configure_server_resp', (_message.Message,), dict(
  DESCRIPTOR = _CONFIGURE_SERVER_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Configure_server_resp)
  ))
_sym_db.RegisterMessage(Configure_server_resp)

Reset_server_req = _reflection.GeneratedProtocolMessageType('Reset_server_req', (_message.Message,), dict(
  DESCRIPTOR = _RESET_SERVER_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Reset_server_req)
  ))
_sym_db.RegisterMessage(Reset_server_req)

Reset_server_resp = _reflection.GeneratedProtocolMessageType('Reset_server_resp', (_message.Message,), dict(
  DESCRIPTOR = _RESET_SERVER_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Reset_server_resp)
  ))
_sym_db.RegisterMessage(Reset_server_resp)

Terminate_server_req = _reflection.GeneratedProtocolMessageType('Terminate_server_req', (_message.Message,), dict(
  DESCRIPTOR = _TERMINATE_SERVER_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Terminate_server_req)
  ))
_sym_db.RegisterMessage(Terminate_server_req)

Terminate_server_resp = _reflection.GeneratedProtocolMessageType('Terminate_server_resp', (_message.Message,), dict(
  DESCRIPTOR = _TERMINATE_SERVER_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Terminate_server_resp)
  ))
_sym_db.RegisterMessage(Terminate_server_resp)

New_time_solver_req = _reflection.GeneratedProtocolMessageType('New_time_solver_req', (_message.Message,), dict(
  DESCRIPTOR = _NEW_TIME_SOLVER_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.New_time_solver_req)
  ))
_sym_db.RegisterMessage(New_time_solver_req)

New_time_solver_resp = _reflection.GeneratedProtocolMessageType('New_time_solver_resp', (_message.Message,), dict(
  DESCRIPTOR = _NEW_TIME_SOLVER_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.New_time_solver_resp)
  ))
_sym_db.RegisterMessage(New_time_solver_resp)

Kill_time_solver_req = _reflection.GeneratedProtocolMessageType('Kill_time_solver_req', (_message.Message,), dict(
  DESCRIPTOR = _KILL_TIME_SOLVER_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Kill_time_solver_req)
  ))
_sym_db.RegisterMessage(Kill_time_solver_req)

Kill_time_solver_resp = _reflection.GeneratedProtocolMessageType('Kill_time_solver_resp', (_message.Message,), dict(
  DESCRIPTOR = _KILL_TIME_SOLVER_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Kill_time_solver_resp)
  ))
_sym_db.RegisterMessage(Kill_time_solver_resp)

Time_step = _reflection.GeneratedProtocolMessageType('Time_step', (_message.Message,), dict(
  DESCRIPTOR = _TIME_STEP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Time_step)
  ))
_sym_db.RegisterMessage(Time_step)

Solve_req = _reflection.GeneratedProtocolMessageType('Solve_req', (_message.Message,), dict(
  DESCRIPTOR = _SOLVE_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Solve_req)
  ))
_sym_db.RegisterMessage(Solve_req)

Solve_resp = _reflection.GeneratedProtocolMessageType('Solve_resp', (_message.Message,), dict(
  DESCRIPTOR = _SOLVE_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Solve_resp)
  ))
_sym_db.RegisterMessage(Solve_resp)

Cancel_req = _reflection.GeneratedProtocolMessageType('Cancel_req', (_message.Message,), dict(
  DESCRIPTOR = _CANCEL_REQ,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Cancel_req)
  ))
_sym_db.RegisterMessage(Cancel_req)

Cancel_resp = _reflection.GeneratedProtocolMessageType('Cancel_resp', (_message.Message,), dict(
  DESCRIPTOR = _CANCEL_RESP,
  __module__ = 'client_server_pb2'
  # @@protoc_insertion_point(class_scope:Scontrol.Cancel_resp)
  ))
_sym_db.RegisterMessage(Cancel_resp)



_SERVER = _descriptor.ServiceDescriptor(
  name='Server',
  full_name='Scontrol.Server',
  file=DESCRIPTOR,
  index=0,
  options=None,
  serialized_start=1255,
  serialized_end=2001,
  methods=[
  _descriptor.MethodDescriptor(
    name='ping',
    full_name='Scontrol.Server.ping',
    index=0,
    containing_service=None,
    input_type=_PING,
    output_type=_PONG,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='get_server_features',
    full_name='Scontrol.Server.get_server_features',
    index=1,
    containing_service=None,
    input_type=_SERVER_FEATURE_REQ,
    output_type=_SERVER_FEATURE_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='get_server_state',
    full_name='Scontrol.Server.get_server_state',
    index=2,
    containing_service=None,
    input_type=_SERVER_STATE_REQ,
    output_type=_SERVER_STATE_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='configure_server',
    full_name='Scontrol.Server.configure_server',
    index=3,
    containing_service=None,
    input_type=_CONFIGURE_SERVER_REQ,
    output_type=_CONFIGURE_SERVER_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='reset_server',
    full_name='Scontrol.Server.reset_server',
    index=4,
    containing_service=None,
    input_type=_RESET_SERVER_REQ,
    output_type=_RESET_SERVER_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='terminate_server',
    full_name='Scontrol.Server.terminate_server',
    index=5,
    containing_service=None,
    input_type=_TERMINATE_SERVER_REQ,
    output_type=_TERMINATE_SERVER_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='new_time_solver',
    full_name='Scontrol.Server.new_time_solver',
    index=6,
    containing_service=None,
    input_type=_NEW_TIME_SOLVER_REQ,
    output_type=_NEW_TIME_SOLVER_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='kill_time_solver',
    full_name='Scontrol.Server.kill_time_solver',
    index=7,
    containing_service=None,
    input_type=_KILL_TIME_SOLVER_REQ,
    output_type=_KILL_TIME_SOLVER_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='solve',
    full_name='Scontrol.Server.solve',
    index=8,
    containing_service=None,
    input_type=_SOLVE_REQ,
    output_type=_SOLVE_RESP,
    options=None,
  ),
  _descriptor.MethodDescriptor(
    name='cancel',
    full_name='Scontrol.Server.cancel',
    index=9,
    containing_service=None,
    input_type=_CANCEL_REQ,
    output_type=_CANCEL_RESP,
    options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_SERVER)

DESCRIPTOR.services_by_name['Server'] = _SERVER

# @@protoc_insertion_point(module_scope)
