MESSAGE(STATUS "======================CLIENT======================")

set(HEADER_FILES
        ${cs_proto_hdrs}
        ${cs_grpc_hdrs}
        ${CMAKE_SOURCE_DIR}/inc/utils.h
        ${CMAKE_SOURCE_DIR}/inc/logger.h
        ${CMAKE_SOURCE_DIR}/inc/thread_safe_queue.h
        ${CMAKE_SOURCE_DIR}/inc/option_printer.h
        ${CMAKE_SOURCE_DIR}/inc/custom_option_description.h
        inc/task.h
        inc/planner.h
        inc/server_stub.h
        inc/solver.h
        inc/server.h
        inc/server_handler.h
        inc/makespan_queue.h)


set(SOURCE_FILES
        ${HEADER_FILES}
        ${cs_proto_srcs}
        ${cs_grpc_srcs}
        ${CMAKE_SOURCE_DIR}/src/utils.cpp
        ${CMAKE_SOURCE_DIR}/src/logger.cpp
        ${CMAKE_SOURCE_DIR}/src/consumer_producer_sync.cpp
        ${CMAKE_SOURCE_DIR}/src/option_printer.cpp
        ${CMAKE_SOURCE_DIR}/src/custom_option_description.cpp
        src/task.cpp
        src/planner.cpp
        src/server_stub.cpp
        src/solver.cpp
        src/server.cpp
        src/server_handler.cpp
        src/main.cpp
        src/planner_reporting.cpp )

set_source_files_properties(${cs_grpc_srcs} PROPERTIES GENERATED TRUE)
set_source_files_properties(${cs_grpc_hdrs} PROPERTIES GENERATED TRUE)
set_source_files_properties(${cs_proto_srcs} PROPERTIES GENERATED TRUE)
set_source_files_properties(${cs_proto_hdrs} PROPERTIES GENERATED TRUE)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/inc)

add_executable(dhcp-asp-client ${SOURCE_FILES})

set_target_properties(dhcp-asp-client PROPERTIES LINKER_LANGUAGE CXX)

add_dependencies(dhcp-asp-client cs_grpc-generate)

target_link_libraries(dhcp-asp-client PRIVATE pthread)
target_link_libraries(dhcp-asp-client PRIVATE stdc++fs)
target_link_libraries(dhcp-asp-client PRIVATE ${Protobuf_LIBRARIES})
target_link_libraries(dhcp-asp-client PRIVATE grpc++_unsecure)
target_link_libraries(dhcp-asp-client PRIVATE ${G3LOG})
target_link_libraries(dhcp-asp-client PRIVATE clingo)
target_link_libraries(dhcp-asp-client PRIVATE ${Boost_LIBRARIES})

INSTALL(TARGETS dhcp-asp-client RUNTIME DESTINATION bin)