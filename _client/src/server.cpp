//
// Created by f on 15/10/17.
//

#include <server.h>

#include <algorithm>
#include <chrono>
#include <utility>
#include <iomanip>

namespace DHCP::Client
{
	Server::Server(std::string domain,
	               std::string goal,
	               std::string dif,
	               uint32_t id,
	               Server_stub_sp server_stub)
		: _domain(std::move(domain)),
		  _goal(std::move(goal)),
		  _dif(std::move(dif)),
		  _id(std::move(id)),
		  _server_stub(std::move(server_stub)),
		  _done(false),
		  _no_processors(0),
		  _no_cores_per_processor(0),
		  _no_threads_per_core(0),
		  _cpu_MHz(0),
		  _memory_MB(0),
		  _com_delay(0)
	{
		//Try connection
		
		if(_server_stub->is_alive())
		{
			
			auto ping = _server_stub->rpc_ping();
			if(ping.first)
			{
				_com_delay = ping.second;
				
				auto features = _server_stub->rpc_get_server_features();
				if(std::get<0>(features))
				{
					_no_processors          = std::get<1>(features);
					_no_cores_per_processor = std::get<2>(features);
					_no_threads_per_core    = std::get<3>(features);
					_cpu_MHz                = std::get<4>(features);
					_memory_MB              = std::get<5>(features);
					
					//					//TODO accept different work_capacity multiplier as parameter
					//					_abs_max_workload_capacity = _no_processors * _no_cores_per_processor * _no_threads_per_core;
					
					_available_workload = _max_workload = _no_processors * _no_cores_per_processor;
					if(_server_stub->rpc_reset_server() && _server_stub->rpc_configure_server({},
					                                                            _domain,
					                                                            _goal,
					                                                            _dif,
					                                                            _max_workload))
					{
						//==============================================================================
						//ALL SUCCESSFUL
						//						_machine_listener_th = std::thread(& Server::server_listener,
						//						                                   this);
						return;
					}
				}
			}
		}
	}
	
	Server::~Server()
	{
		_done = true;
		//		_machine_listener_th.join();
		_workload_cv.notify_all();
		for(auto& sv:_solvers)
		{
			for(auto& s:sv)
			{
				_server_stub->rpc_cancel(s->solver_id());
			}
		}
		_server_stub->rpc_reset_server();
	}
	
	Solver_sp
	Server::add_and_reserve_solver(uint32_t makespan)
	{
		std::chrono::steady_clock::time_point start;
		if(program_instance_data->_verbosity >= 3)
		{
			start = std::chrono::steady_clock::now();
		}
		auto solver_id = _server_stub->rpc_new_makespan_solver(makespan,
		                                                1);
		
		Solver_sp solver;
		if(solver_id >= 0)
		{
			solver = std::make_shared<Solver>(solver_id,
			                                  makespan,
			                                  1,
			                                  _server_stub);
			if(program_instance_data->_verbosity >= 3)
			{
				
				LOG(INFO) << "Created new solver of makespan " << std::setfill('0') << std::setw(4) << makespan << " in "
				          << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count()
				          << " ms";
			}
			std::unique_lock<std::mutex> lock(_solver_record_mtx);
			while(_solvers.size() <= makespan)
			{
				_solvers.emplace_back();
				_solver_counts.emplace_back(0);
			}
			solver->try_reserve();
			_solvers[makespan].push_back(solver);
			_can_process_time.insert(makespan);
			return solver;
		}
		else
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Creating new solver of makespan " << std::setfill('0') << std::setw(4) << makespan
				             << " is failed";
			}
			return Solver_sp();
		}
	}
	
	bool
	Server::operator<(Server const& other)
	{
		//TODO implement better priority
		return _id < other._id;
	}
	
	void
	Server::server_listener()
	{
		//		{
		//			_workload_mtx.lock();
		//			_cur_max_workload_capacity = _abs_max_workload_capacity;
		//			_workload_mtx.unlock();
		//		}
		//		auto                            wake_up = std::chrono::system_clock::now();
		//		std::chrono::milliseconds const sleep_duration(1000);//TODO make listening period parametric
		//		while(!_done)
		//		{
		//			auto state = rpc_get_server_state();
		//			if(std::get<0>(state))
		//			{
		//				_avg_load_one_min     = std::get<1>(state);
		//				_avg_load_five_min    = std::get<2>(state);
		//				_avg_load_fifteen_min = std::get<3>(state);
		//				_available_memory_MB  = std::get<4>(state);
		//
		//
		//
		//
		//				//TODO fix scale, (_avg_load_one_min) comes in 100 up scale for single load
		//				_last_minute_workload.pop_front();
		//				_last_minute_workload.push_back(_cur_workload * 100);
		//
		//				auto avg_dhcp_asp_load = std::accumulate(_last_minute_workload.begin(),
		//				                                         _last_minute_workload.end(),
		//				                                         0) / static_cast<double> (_last_minute_workload.size());
		//
		//				auto avg_machine_load              = _avg_load_one_min - avg_dhcp_asp_load;
		//				auto desired_max_workload_capacity = _abs_max_workload_capacity * 100 - avg_machine_load;
		//				_workload_mtx.lock();
		//				_cur_max_workload_capacity += ((desired_max_workload_capacity - _cur_max_workload_capacity * 100) * 0.001);//TODO make parametric
		//				_workload_mtx.unlock();
		//
		//				#ifdef DHCP_DEBUG_Z
		//				std::string log_str;
		//				log_str += "Server Listener (" + _ip + ")\n";
		//				log_str += "Remote Average Last Minute Workload = " + std::to_string(_avg_load_one_min) + "\n";
		//				log_str += "Remote Available Memory MB = " + std::to_string(_available_memory_MB) + "\n";
		//				log_str += "Average Last Minute Workload = " + std::to_string(avg_dhcp_asp_load) + "\n";
		//				LOG(DEBUG) << "Thread ID(" << std::this_thread::get_id() << ") " <<log_str;
		//				#endif
		//			}
		//			std::this_thread::sleep_until(wake_up);
		//			wake_up += sleep_duration;
		//		}
	}
	
	bool
	Server::blocking_reserve_workload()
	{
		
		std::unique_lock<std::mutex> lock(_workload_mtx);
		_workload_cv.wait(lock,
		                  [this]()
		                  {return _done || _available_workload > 0;});
		
		if(_done)
		{
			return false;
		}
		_available_workload--;
		return true;
	}
	
	void
	Server::release_workload()
	{
		std::unique_lock<std::mutex> lock(_workload_mtx);
		_available_workload++;
		if(_available_workload > _max_workload)
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Max Workload is exceeded!";
			}
			throw std::logic_error("Max Workload is exceeded!");
		}
		_workload_cv.notify_one();
	}
}

