//
// Created by f on 13/02/18.
//
#include <logger.h>
#include <server_stub.h>

namespace DHCP::Client
{
	extern Problem_data_sp program_instance_data;
}

namespace DHCP::Client
{
	Server_stub::Server_stub(std::string ip)
		: _ip(std::move(ip)),
		  _channel(grpc::CreateChannel(_ip,
		                               grpc::InsecureChannelCredentials())),
		  _stub(Scontrol::Server::NewStub(_channel)),
		  _alive(false)
	{
		grpc_connectivity_state ret;
		ret = _channel->GetState(true);
		std::chrono::system_clock::time_point deadline(std::chrono::system_clock::now() + std::chrono::seconds(1));
		_channel->WaitForConnected(deadline);
		ret = _channel->GetState(true);
		
		if(GRPC_CHANNEL_READY == _channel->GetState(true))
		{
			_alive = true;
		}
		
		if(program_instance_data->_verbosity >= 3 && _alive)
		{
			LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
			          << "Successfully established machine connection.";
		}
		if(program_instance_data->_verbosity >= 3 && !_alive)
		{
			LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
			             << "Server connection Failed.";
		}
	}
	
	bool
	Server_stub::is_alive()
	{return _alive;}
	
	std::pair<bool,
	          std::chrono::duration<long,
	                                std::ratio<1,
	                                           1000>>>
	Server_stub::rpc_ping()
	{
		Scontrol::Ping      ping;
		Scontrol::Pong      pong;
		grpc::ClientContext context;
		auto                start = std::chrono::system_clock::now();//Take wall time
		try
		{
			//Wait maximum of 1 seconds for response
			context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + std::chrono::seconds(1)));
			grpc::Status stat  = _stub->ping(& context,
			                                 ping,
			                                 & pong);
			auto         delay = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
			
			if(!stat.ok())
			{
				if(program_instance_data->_verbosity >= 2)
				{
					LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
					             << "RPC FAIL. Couldn't Ping." << "\n\t\t (" << +stat.error_code() << ")" << stat.error_details()
					             << " : " << stat.error_message();
				}
			}
			
			return std::make_pair(stat.ok(),
			                      delay);
		}
		catch(std::exception const& e)
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "PING Failed with" << std::string(e.what());
			}
			return std::make_pair(false,
			                      std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start));
		}
	}
	
	std::tuple<bool, //rpc_success
	           uint32_t,//no_processors
	           uint32_t,//no_cores_per_processor
	           uint32_t,//no_threads_per_core
	           uint32_t,//cpu_MHz
	           uint32_t>//memory_MB
	Server_stub::rpc_get_server_features()
	{
		Scontrol::Server_feature_req  f_req;
		Scontrol::Server_feature_resp f_resp;
		grpc::ClientContext           context;
		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + std::chrono::seconds(1)));
		
		grpc::Status stat = _stub->get_server_features(& context,
		                                               f_req,
		                                               & f_resp);
		if(!stat.ok())
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't Get Features." << "\n\t\t (" << +stat.error_code() << ")"
				             << stat.error_details() << " : " << stat.error_message();
			}
		}
		
		return std::make_tuple(stat.ok(),//TODO fix
		                       f_resp.no_processors(),
		                       f_resp.no_cores_per_processor(),
		                       f_resp.no_threads_per_core(),
		                       f_resp.cpu_mhz(),
		                       f_resp.memory_mb());
	}
	
	std::tuple<bool,//rpc_success
	           uint32_t,//avg_load_one_min
	           uint32_t,//avg_load_five_min
	           uint32_t,//avg_load_fifteen_min
	           uint32_t>//available_memory_mb
	Server_stub::rpc_get_server_state()
	{
		Scontrol::Server_state_req  s_req;
		Scontrol::Server_state_resp s_resp;
		grpc::ClientContext         context;
		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + std::chrono::seconds(1)));
		
		grpc::Status stat = _stub->get_server_state(& context,
		                                            s_req,
		                                            & s_resp);
		if(!stat.ok())
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't Get State." << "\n\t\t (" << +stat.error_code() << ")"
				             << stat.error_details() << " : " << stat.error_message();
			}
		}
		
		return std::make_tuple(stat.ok(),
		                       s_resp.avg_load_one_min(),
		                       s_resp.avg_load_five_min(),
		                       s_resp.avg_load_fifteen_min(),
		                       s_resp.available_memory_mb());
	}
	
	bool
	Server_stub::rpc_configure_server(std::vector<std::string> clingo_arguments,
	                                  std::string domain,
	                                  std::string goal,
	                                  std::string dif,
	                                  uint32_t parallel_capacity)
	{
		Scontrol::Configure_server_req  c_req;
		Scontrol::Configure_server_resp c_resp;
		grpc::ClientContext             context;
		//		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + 5s));
		
		//TODO send clingo arguments too
		c_req.set_domain(domain);
		c_req.set_goal(goal);
		c_req.set_dif(dif);
		c_req.set_parallel_capacity(parallel_capacity);
		
		grpc::Status stat = _stub->configure_server(& context,
		                                            c_req,
		                                            & c_resp);
		
		if(!stat.ok())
		{
			
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't Configure." << "\n\t\t (" << +stat.error_code() << ")"
				             << stat.error_details() << " : " << stat.error_message();
			}
		}
		
		return stat.ok();
	}
	
	bool
	Server_stub::rpc_reset_server()
	{
		Scontrol::Reset_server_req  r_req;
		Scontrol::Reset_server_resp r_resp;
		grpc::ClientContext         context;
		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + std::chrono::seconds(1)));
		
		grpc::Status stat = _stub->reset_server(& context,
		                                        r_req,
		                                        & r_resp);
		
		if(!stat.ok())
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't Reset. " << "\n\t\t (" << +stat.error_code() << ")" << stat.error_details()
				             << " : " << stat.error_message();
			}
		}
		
		return stat.ok();
	}
	
	int64_t
	Server_stub::rpc_new_makespan_solver(uint32_t plan_makespan,
	                                     uint32_t constraint_makespan)
	{
		Scontrol::New_time_solver_req  ns_req;
		Scontrol::New_time_solver_resp ns_resp;
		grpc::ClientContext            context;
		//TODO cannot have deadline since the grounding depends on makespan (may be adjust deadline w.r.t plan_makespan)
		//		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + _max_delay));
		
		ns_req.set_problem_time_span(plan_makespan);
		ns_req.set_constraint_time_span(constraint_makespan);
		
		grpc::Status stat = _stub->new_time_solver(& context,
		                                           ns_req,
		                                           & ns_resp);
		
		if(!stat.ok())
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't create new time solver. " << "\n\t\t (" << +stat.error_code() << ")"
				             << stat.error_details() << " : " << stat.error_message();
			}
			return -1;
		}
		else
		{
			return ns_resp.solver_id();
		}
	}
	
	bool
	Server_stub::rpc_kill_solver(uint32_t solver_id)
	{
		Scontrol::Kill_time_solver_req  ks_req;
		Scontrol::Kill_time_solver_resp ks_resp;
		grpc::ClientContext             context;
		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + std::chrono::seconds(1)));
		
		ks_req.set_solver_id(solver_id);
		
		grpc::Status stat = _stub->kill_time_solver(& context,
		                                            ks_req,
		                                            & ks_resp);
		
		if(!stat.ok())
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't Kill Server." << "\n\t\t (" << +stat.error_code() << ")"
				             << stat.error_details() << " : " << stat.error_message();
			}
		}
		
		return stat.ok();
	}
	
	std::tuple<bool,//operation successful
	           bool,//satisfiable
	           Time_sequence_sp,//Fluent
	           Time_sequence_sp,//Act_actuation
	           Time_sequence_sp,//Act_sensing
	           Time_sequence_sp,//outcome
	           Time_sequence_sp,//additional
	           uint32_t,//clingo time
	           uint32_t>//total time
	Server_stub::rpc_solve(uint32_t solver_id,
	                       Time_sequence const& constraints)
	{
		Scontrol::Solve_req  s_req;
		Scontrol::Solve_resp s_resp;
		grpc::ClientContext  context;
		//		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + _max_delay));
		
		//=========================================================
		s_req.set_solver_id(solver_id);
		
		//time_sequence_to_grpc_repeated_time_step
		auto tseq_to_grpc_tstep = [](DHCP::Time_sequence const& from,
		                             std::function<::Scontrol::Time_step*(void)> get_field)
		{
			for(DHCP::Time_step_sp const& a:from)
			{
				auto time_sequence = get_field();
				std::for_each(a->begin(),
				              a->end(),
				              [&](DHCP::Atom const& a)
				              {time_sequence->add_atoms(a.to_string());});
			}
		};
		
		tseq_to_grpc_tstep(constraints,
		                   [&s_req]() -> ::Scontrol::Time_step*
		                   {return s_req.add_constraints();});
		
		grpc::Status stat = _stub->solve(& context,
		                                 s_req,
		                                 & s_resp);
		
		if(!stat.ok())
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't create new time solver. " << "\n\t\t (" << +stat.error_code() << ")"
				             << stat.error_details() << " : " << stat.error_message();
			}
			return std::make_tuple<bool,
			                       bool, //satisfiable
			                       Time_sequence_sp,//Fluent
			                       Time_sequence_sp,//Act_actuation
			                       Time_sequence_sp,//Act_sensing
			                       Time_sequence_sp,//outcome
			                       Time_sequence_sp,//additional
			                       uint32_t,//clingo time
			                       uint32_t>(false,
			                                 false,
			                                 nullptr,
			                                 nullptr,
			                                 nullptr,
			                                 nullptr,
			                                 nullptr,
			                                 0,
			                                 0);
		}
		else
		{
			//=========================================================
			auto grpc_tstep_to_tseq = [](uint32_t size,
			                             std::function<Scontrol::Time_step const&(uint32_t)> get_field) -> DHCP::Time_sequence_sp
			{
				auto         cons = std::make_shared<DHCP::Time_sequence>();
				//				cons->try_reserve(static_cast<unsigned long>(size));
				//Create time sequence for each
				for(uint32_t t    = 0;
				    t < size;
				    t++)
				{
					cons->emplace_back(std::make_shared<DHCP::Time_step>());
					Scontrol::Time_step const& cur_time_step = get_field(t);
					
					for(int i = 0;
					    i < cur_time_step.atoms_size();
					    i++)
					{
						cons->at(t)->emplace(cur_time_step.atoms(i));
					}
				}
				return cons;
			};
			
			DHCP::Time_sequence_sp fluent = grpc_tstep_to_tseq(static_cast<uint32_t>(s_resp.fluent_size()),
			                                                   [&s_resp](uint32_t index) -> Scontrol::Time_step const&
			                                                   {
				                                                   return s_resp.fluent(index);
			                                                   });
			
			DHCP::Time_sequence_sp act_actuation = grpc_tstep_to_tseq(static_cast<uint32_t>(s_resp.act_actuation_size()),
			                                                          [&s_resp](uint32_t index) -> Scontrol::Time_step const&
			                                                          {
				                                                          return s_resp.act_actuation(index);
			                                                          });
			
			DHCP::Time_sequence_sp act_sensing = grpc_tstep_to_tseq(static_cast<uint32_t>(s_resp.act_sensing_size()),
			                                                        [&s_resp](uint32_t index) -> Scontrol::Time_step const&
			                                                        {
				                                                        return s_resp.act_sensing(index);
			                                                        });
			
			DHCP::Time_sequence_sp outcome = grpc_tstep_to_tseq(static_cast<uint32_t>(s_resp.outcome_size()),
			                                                    [&s_resp](uint32_t index) -> Scontrol::Time_step const&
			                                                    {
				                                                    return s_resp.outcome(index);
			                                                    });
			
			DHCP::Time_sequence_sp additional = grpc_tstep_to_tseq(static_cast<uint32_t>(s_resp.additional_size()),
			                                                       [&s_resp](uint32_t index) -> Scontrol::Time_step const&
			                                                       {
				                                                       return s_resp.additional(index);
			                                                       });
			return std::make_tuple<bool,
			                       bool, //satisfiable
			                       Time_sequence_sp,//Fluent
			                       Time_sequence_sp,//Act_actuation
			                       Time_sequence_sp,//Act_sensing
			                       Time_sequence_sp,//outcome
			                       Time_sequence_sp,//additional
			                       uint32_t,//clingo time
			                       uint32_t>(true,
			                                 s_resp.satisfiable(),
			                                 std::move(fluent),
			                                 std::move(act_actuation),
			                                 std::move(act_sensing),
			                                 std::move(outcome),
			                                 std::move(additional),
			                                 s_resp.clingo_time_ms(),
			                                 s_resp.processing_time_ms());
		}
	}
	
	bool
	Server_stub::rpc_cancel(uint32_t solver_id)
	{
		Scontrol::Cancel_req  c_req;
		Scontrol::Cancel_resp c_resp;
		grpc::ClientContext   context;
		context.set_deadline(std::chrono::system_clock::time_point(std::chrono::system_clock::now() + std::chrono::seconds(1)));
		
		c_req.set_solver_id(solver_id);
		
		grpc::Status stat = _stub->cancel(& context,
		                                  c_req,
		                                  & c_resp);
		
		if(!stat.ok())
		{
			if(program_instance_data->_verbosity >= 2)
			{
				LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Server Listener (" << _ip << "): "
				             << "RPC FAIL. Couldn't cancel solving." << "\n\t\t (" << +stat.error_code() << ")"
				             << stat.error_details() << " : " << stat.error_message();
			}
		}
		
		return stat.ok();
	}
}