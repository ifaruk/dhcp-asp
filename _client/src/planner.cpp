//
// Created by f on 16/10/17.
//

#include <algorithm>

#include <planner.h>

namespace DHCP::Client
{
	Step::Step(State_type s_type,
	           Atom_set const& s_fluents,
	           Atom_set const& s_class,
	           Action_type a_type,
	           Atom_set const& actions,
	           Atom_set const& possible_outcomes,
	           Atom_set const& additional,
	           uint32_t level,
	           uint32_t min_steps_to_goal)
		: _s_type(s_type),
		  _s_fluents(s_fluents),
		  _s_class(s_class),
		  _a_type(a_type),
		  _actions(actions),
		  _additional(additional),
		  _min_level(level),
		  _min_no_steps_to_goal(min_steps_to_goal),
		  _step_type(Step_type::ALIVE),
		  _duplicate_of(Step_sp())
	{
		_fluents_str    = ::DHCP::to_string<Atom_set>(_s_fluents);
		_class_str      = ::DHCP::to_string<Atom_set>(_s_class);
		_additional_str = ::DHCP::to_string<Atom_set>(_additional);
		
		if(s_type != State_type::INTERMEDIATE)
		{
			_actions.clear();// No actions at GOAL or DEAD END are acceptable
		}
		_actions_str = ::DHCP::to_string<Atom_set>(_actions);
		
		//For Actuation actions include a pointer with dummy empty outcome
		if(possible_outcomes.empty())
		{
			_outcomes.try_emplace(Atom(),
			                      Step_wp());
		}
		
		//For Sensing action include a pointer for every possible outcome
		for(auto const& a:possible_outcomes)
		{
			_outcomes.try_emplace(a,
			                      Step_wp());
		}
		
		for(auto const& a:_outcomes)
		{
			_outcomes_str += a.first.to_string() + ",";
		}
		if(!_outcomes.empty())
		{
			_outcomes_str.pop_back();
		}
		
		//@formatter:off
		if(((_s_type == State_type::GOAL) != (min_steps_to_goal == 0))
		   || ((_s_type == State_type::DEAD_END) != (min_steps_to_goal == UINT32_MAX))
		   || ((_s_type == State_type::INTERMEDIATE) != (min_steps_to_goal > 0)))
		{
			if(program_instance_data->_verbosity>=1)
			{
				LOG(FATAL) << "State type and goal distance does not match for the Step \n" + to_string();
			}
			throw std::logic_error("State type and goal distance does not match for the Step \n" + to_string());
		}
		//@formatter:on
	}
	
	bool
	Step::set_outcome(Atom outcome,
	                  Step_sp step)
	{
		auto f = _outcomes.find(outcome);
		if(_outcomes.end() != f)
		{
			Step_sp cur_step = f->second.lock();
			/* If current corresponding connection is empty for current outcome
			 * Or we already containing same connection. Bind them (even though
			 * may be redundant) then return true
			 */
			if(!cur_step || cur_step == step)
			{
				f->second = step;
				return true;
			}
			//TODO In the future, let each outcome to hold more than one connection
		}
		else
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "No such outcome " + outcome.to_string() + " exists for Step \n" + to_string();
			}
			throw std::logic_error("No such outcome " + outcome.to_string() + " exists for Step \n" + to_string());
		}
		return false;
	}
	
	void
	Step::set_duplicate_of(Step_sp d)
	{
		_step_type    = Step_type::DUPLICATE;
		_duplicate_of = d;
		//Clear all content to save memory
		_s_fluents.clear();
		_s_class.clear();
		_actions.clear();
		_outcomes.clear();
		_additional.clear();
		
		_fluents_str.clear();
		_class_str.clear();
		_actions_str.clear();
		_outcomes_str.clear();
		_additional_str.clear();
	}
	
	std::string
	Step::to_string()
	{
		static std::array<std::string,
		                  3> s_type_str{"GOAL",
		                                "INTERMEDIATE",
		                                "DEAD END"};
		std::stringstream    ss;
		ss << "Step ID: " << std::hex << this << std::endl;
		ss << "State Type: " << s_type_str[static_cast<std::underlying_type_t<State_type>>(_s_type)] << std::endl;
		ss << "State Fluents: " << _fluents_str << std::endl;
		ss << "State Class: " << _class_str << std::endl;
		ss << "Actions: " << _actions_str << std::endl;
		ss << "Outcomes: " << _outcomes_str << std::endl;
		ss << "Additional: " << _additional_str << std::endl;
		return ss.str();
	}
	
	size_t
	Step::hash() const
	{
		//@formatter:off
		return std::hash<std::size_t>()(static_cast<std::underlying_type_t<State_type>>(_s_type))
		       ^ std::hash<std::size_t>()(static_cast<std::underlying_type_t<State_type>>(_a_type))
		       ^ std::hash<std::string>()(_class_str);
		//@formatter:on
	}
	
	bool
	Step::operator==(Step const& other) const
	{
		return std::tie(_s_type,
		                _a_type,
		                _class_str) == std::tie(other._s_type,
		                                        other._a_type,
		                                        other._class_str);
	}
	
	
	//===================================================================================
	//===================================================================================
	//===================================================================================
	//===================================================================================
	
}

namespace DHCP::Client
{
	Planner::Planner(Makespan_queue_sp makespan_taskQ)
		: _makespan_taskQ(makespan_taskQ),
		  _goal({{"goal", uint8_t(0)}}),
		  _waypoint({{"waypoint", 0}}),
		  _done(false),
		  _existing_level_zero_tasks(0),
		  _stat_total_planning_start(Time_point::min()),
		  _stat_total_planning_duration(0),
		  _recently_computed_tasks(),
		  _task_processor_th(& Planner::task_processor,
		                     this)
	{
	}
	
	Planner::~Planner()
	{
		_done = true;
		_recently_computed_tasks.invalidate();
		try
		{
			//Joint _task_processor_th if can
			_task_processor_th.join();
			_makespan_sampler.join();
		}
		catch(std::system_error& e)
		{
		}
	}
	
	void
	Planner::new_task(Time_sequence_sp state_constraints,
	                  uint32_t makespan_from,
	                  uint32_t makespan_to,
	                  uint32_t level,
	                  uint32_t priority)
	{
		_initial_tasks.push_back(std::make_shared<Task>(state_constraints,
		                                                makespan_from,
		                                                makespan_to,
		                                                level,
		                                                priority,
		                                                Task_sp()));//TODO add clingo parameters
	}
	
	void
	Planner::start()
	{
		_stat_total_planning_start = std::chrono::steady_clock::now();
		if(program_instance_data->_verbosity >= 4)
		{
			_makespan_sampler = std::thread([this]()
			                                {
				                                auto next_wakeup = std::chrono::steady_clock::now();
				                                while(!_done)
				                                {
					                                auto              sizes = _makespan_taskQ->get_sizes();
					                                uint32_t          size  = 0;
					                                std::stringstream out;
					                                out << "TaskQ Sizes" << std::endl;
					                                for(auto s:sizes)
					                                {
						                                out << "Makespan " << size << " size: " << s << std::endl;
						                                size++;
					                                }
					                                LOG(INFO) << out.str();
					                                next_wakeup += std::chrono::milliseconds(100);
					                                std::this_thread::sleep_until(next_wakeup);
				                                }
			                                });
		}
		
		task_to_handler(_initial_tasks);
	}
	
	void
	Planner::wait_for_completion(std::chrono::seconds timeout_sec)
	{
		std::mutex                   wait_mtx;
		std::unique_lock<std::mutex> wait_lock(wait_mtx);
		if(timeout_sec == std::chrono::seconds(0))
		{
			_done_cv.wait(wait_lock,
			              [this]() -> bool
			              {return _done;});
		}
		else
		{
			auto now = std::chrono::system_clock::now();
			_done_cv.wait_until(wait_lock,
			                    now + timeout_sec,
			                    [this]() -> bool
			                    {return _done;});
		}
		_stat_total_planning_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _stat_total_planning_start);
	}
	
	void
	Planner::task_to_handler(std::vector<Task_sp> tasks)
	{
		//Find the number of new main tasks
		std::for_each(tasks.begin(),
		              tasks.end(),
		              [this](Task_sp& t)
		              {
			              if(t->get_level() == 0)
			              {
				              _existing_level_zero_tasks++;
			              }
			              auto th = std::make_shared<Task_handler>(t,
			                                                       _makespan_taskQ,
			                                                       [this](Task_sp t)
			                                                       {
				                                                       task_completion(t);
			                                                       },
			                                                       "dhcp-asp-server -df " + program_instance_data->_domain_file_name + " -gf " + program_instance_data->_goal_file_name + " -dif " + program_instance_data->_domain_insight_file_name + " -cts 1 -c \"" + to_argument(* t->get_constraints()) + "\" ");
			              _task_handler_map.try_emplace(th->id(),
			                                            th);
			
			              th->init();
		              });
	}
	
	void
	Planner::task_completion(Task_sp t)
	{
		_recently_computed_tasks.push(std::move(t));
	}
	
	void
	Planner::task_processor()
	{
		Task_sp task;
		while(!_done && _recently_computed_tasks.wait_pop(task))
		{
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "Task Processor: Waiting computed tasks: " << _recently_computed_tasks.size();
			}
			auto new_tasks = add_to_plan_and_generate_new_task(task);
			task_to_handler(new_tasks);
			//Add to completed tasks set (for statistics)
			_completed_task_list.push_back(task);
			//Remove from existing task_handlers
			//			_task_handler_map.erase(task->id());
			
			//Update w.r.t current task level
			if(task->get_level() == 0)
			{
				_existing_level_zero_tasks--;
			}
			//If no more main tasks remained than start termination
			if(_existing_level_zero_tasks == 0)
			{
				_done      = true;
				_last_task = task;
				break;
			}
		}
		//Terminate all alive tasks
		std::for_each(_task_handler_map.begin(),
		              _task_handler_map.end(),
		              [](std::pair<uint32_t,
		                           Task_handler_sp> p)
		              {
			              p.second->cancel();
		              });
		_done_cv.notify_all();
	}
	
	/**
	 * @brief Generates the pair ... preN [--E--> N]....
	 *
	 * @param fluent
	 * @param act_actuation
	 * @param act_sensing
	 * @param outcome
	 * @param additional
	 * @param pre_e
	 * @param distance_to_goal
	 * @return leading edge of edge->node pair
	 */
	Step_sp
	Planner::form_step(Time_step const& fluent,
	                   Time_step const& state_class,
	                   Time_step const& act_actuation,
	                   Time_step const& act_sensing,
	                   Time_step const& additional,//TODO find what to do with additonal
	                   uint32_t const& level,
	                   uint32_t distance_to_goal)
	{
		
		if(!act_sensing.empty() && !act_actuation.empty())
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL)
					<< "There must be either a single sensing action or single/multiple actuation actions at any single time step.";
			}
			throw std::invalid_argument("There must be either a single sensing action or single/multiple actuation actions at any single time step.");
		}
		
		Atom_set outcomes;
		if(!act_sensing.empty())
		{
			outcomes = program_instance_data->_dif.get_sensing_to_outcome().at(* act_sensing.begin());
		}
		
		auto state_type = !filter(fluent,
		                          _goal).empty() ? Step::State_type::GOAL : (act_actuation.empty() && act_sensing.empty() ? Step::State_type::DEAD_END : Step::State_type::INTERMEDIATE);
		return std::make_shared<Step>(state_type,
		                              fluent,
		                              state_class,
		                              act_sensing.empty() ? Step::Action_type::ACTUATION : Step::Action_type::SENSING,
		                              act_sensing.empty() ? act_actuation : act_sensing,
		                              outcomes,
		                              additional,
		                              level,
		                              distance_to_goal);
	}
	
	/**
	 *
	 * @param task
	 *
	 * @warning this function is not threadsafe
	 */
	std::vector<Task_sp>
	Planner::add_to_plan_and_generate_new_task(Task_sp task)
	{
		
		std::vector<Task_sp> ret;
		if(task && task->one_solution_found())
		{
			//===============================================================================
			//Create branch structure
			Step_sp  prev_step;
			bool     prev_visited_before = false;
			Step_sp  prev_redundant;
			uint32_t priority            = task->get_priority();
			
			auto optimal_solution = task->get_optimum_solution();
			
			//find first goal
			uint32_t goal_makespan = 0;
			for(auto f:* (optimal_solution->_fluent))
			{
				if(!filter(* f,
				           _goal).empty())
				{
					break;
				}
				goal_makespan++;
			}
			if(program_instance_data->_verbosity >= 2)
			{
				if(goal_makespan != task->get_optimum_makespan())
				{
					LOG(WARNING)
						<< "TaskID:" + std::to_string(task->id()) + " returned non-optimal solution " + std::to_string(task->get_optimum_makespan()) + " instead of " + std::to_string(goal_makespan)
						<< std::endl << optimal_solution->to_string();
				}
			}
			
			optimal_solution->_act_actuation->at(goal_makespan)->clear();
			optimal_solution->_act_sensing->at(goal_makespan)->clear();
			
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "Solution to current TaskID:" << std::to_string(task->id()) << std::endl
				          << optimal_solution->to_string();
			}
			
			for(uint32_t t = 0;
			    t <= goal_makespan;
			    t++)
			{
				/*---------------------------------------
				 * Get cur step
				 */
				
				//TODO fix adhoc solution
				Time_step state_class = * optimal_solution->_fluent->at(t);
				
				for(auto atom : * optimal_solution->_additional->at(t))
				{
					//Extract parameter
					auto redundant_atom_str = atom.to_string();
					auto from               = redundant_atom_str.find("(");
					auto to                 = redundant_atom_str.rfind(")");
					redundant_atom_str = redundant_atom_str.substr(from + 1,
					                                               to - from - 1);
					auto redundant_atom = Atom(redundant_atom_str);
					state_class.erase(redundant_atom);
				}
				
				bool    cur_step_visited = false;
				auto    cur_step         = form_step(* optimal_solution->_fluent->at(t),
				                                     state_class,
				                                     * optimal_solution->_act_actuation->at(t),
				                                     * optimal_solution->_act_sensing->at(t),
				                                     * optimal_solution->_additional->at(t),
				                                     task->get_level(),
				                                     goal_makespan - t);//TODO define min distance to goal
				Step_sp redundant_step;
				
				
				//See if this state is visited before
				auto cur_step_hash = Hashed_sp<Step>(cur_step);
				{
					auto f = _visited_state_steps.find(cur_step_hash);
					if(_visited_state_steps.end() != f)
					{
						redundant_step   = cur_step;
						cur_step         = f->second;
						cur_step_visited = true;
						cur_step->set_level(task->get_level());//update level to best one
					}
				}
				
				/*---------------------------------------
				 * Bind previous step to current new step,
				 * If it is a sensing action we will bind with an outcome
				 * Bind cur_step as one of the outcomes
				 * Even both of them are visited before
				 */
				if(t > 0)
				{
					Atom_set outcomes_at_t = * optimal_solution->_outcome->at(t);
					if(outcomes_at_t.size() > 1)
					{
						if(program_instance_data->_verbosity >= 1)
						{
							LOG(FATAL) << "At any time step there should be a single outcome " + to_string<Atom_set>(outcomes_at_t);
						}
						throw std::logic_error("At any time step there should be a single outcome " + to_string<Atom_set>(outcomes_at_t));
					}
					auto current_outcome = outcomes_at_t.empty() ? Atom() : * outcomes_at_t.begin();
					bool able_to_bind_current;
					if(!prev_visited_before || (prev_redundant && prev_redundant->actions() == prev_step->actions()))
					{
						able_to_bind_current = prev_step->set_outcome(current_outcome,
						                                              cur_step);
					}
					
					
					
					/*---------------------------------------
						 * Generate New Problems
						 * if previous step was not visited before then we will need to generate new problem
						 * instances to complete the plan
						 */
					if(!prev_visited_before && Step::Action_type::SENSING == prev_step->action_type())
					{
						//Form new problems from previous problem where outcome is missing
						Time_sequence constraints;
						constraints.emplace_back(std::make_shared<Time_step>(prev_step->fluents()));
						constraints.back()->insert(prev_step->actions().begin(),
						                           prev_step->actions().end());
						
						for(auto const& op:prev_step->outcome())
						{
							if(op.first != current_outcome)
							{
								//TODO see that all Time_sequences of "constraints" are not sharing data they are not supposed to
								Time_sequence cur_constraints = constraints;
								cur_constraints.emplace_back(std::make_shared<Time_step>());
								cur_constraints.back()->insert(op.first);
								
								ret.push_back(std::make_shared<Task>(std::make_shared<Time_sequence>(cur_constraints),
								                                     prev_step->min_step_distance_to_goal(),
								                                     program_instance_data->_makespan_limit,
								                                     task->get_level(),
								                                     priority,
								                                     task));
								
								/* We set search start from 'prev_step's distance to goal, since
								 * no other outcome can be closer than that, otherwise that outcome
								 * would be returned with current branch, since out branch length
								 * is optimized for minimum time span.
								 */
							}
						}
					}
					
					/* If current step is visited before, then remaining part
					 * of the current branch is redundant, we will still
					 * keep it with fewer priority.
					 *
					 * @warning should be updated after new problems generated for prev_step
					 */
					if(cur_step_visited)
					{
						priority++; // With each visited, reduce priority of remaining part of current branch
					}
					
					/* If couldn't bound 'cur_step' to 'prev_step', add it to hanging roots
					 * */
					if(!able_to_bind_current && !cur_step_visited)
					{
						_hanging_roots.emplace(cur_step);
					}
				}
				else//(!prev_step)
				{
					/* This is time=0
					 * If cur_step is visited before this is normal and do nothing
					 * Since our problem generation is formed to include a visited state
					 *
					 * If not then either this is the beginning of global plan
					 * or a hanging_root of an experiment.
					 * */
					if(!cur_step_visited)
					{
						// If no initial has been set yet
						if(!_initial && task->get_level() == 0)
						{
							_initial = cur_step;///TODO make sure cur_step is plans initial state instead of a quick landmark
						}
						_hanging_roots.insert(cur_step);//including global initial '_initial'
					}
				}
				
				if(!cur_step_visited)
				{
					_visited_state_steps.try_emplace(cur_step_hash,
					                                 cur_step);
				}
				
				//update previous
				prev_step           = cur_step;
				prev_visited_before = cur_step_visited;
				prev_redundant      = redundant_step;
			}
			
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << std::endl << "Returned TaskID:" << task->id()
				          << "-" << task->get_optimum_makespan() << std::endl << to_string(* task->get_constraints())
				          << std::endl << optimal_solution->to_string() << std::endl << "Generated New Tasks:" << std::endl
				          << std::accumulate(ret.begin(),
				                             ret.end(),
				                             std::string(""),
				                             [](std::string sum,
				                                Task_sp& t)
				                             {
					                             sum += "TaskID:" + std::to_string(t->id()) + " From makespan " + std::to_string(t->get_makespan_from()) + "\n" + to_string(* t->get_constraints()) + "\n";
					                             return sum;
				                             });
			}
		}
		return ret;
	}
}
	