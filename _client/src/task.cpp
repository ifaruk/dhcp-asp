//
// Created by f on 18/01/18.
//

#include <task.h>
#include <logger.h>
#include <makespan_queue.h>

namespace DHCP::Client
{
	Makespan_record::Makespan_record(bool redundant,
	                                 bool satisfiable,
	                                 const std::chrono::milliseconds& queue_duration,
	                                 const std::chrono::milliseconds& server_duration,
	                                 const std::chrono::milliseconds& cancelled_server_duration,
	                                 const std::chrono::milliseconds& in_server_clingo_duration,
	                                 const std::chrono::milliseconds& in_server_process_duration)
		: _redundant(redundant),
		  _satisfiable(satisfiable),
		  _queue_duration(queue_duration),
		  _server_duration(server_duration),
		  _canceled_server_duration(cancelled_server_duration),
		  _in_server_clingo_duration(in_server_clingo_duration),
		  _in_server_process_duration(in_server_process_duration)
	{}
	
	bool
	Task_sp_comparator::operator()(const Task_sp& lhs,
	                               const Task_sp& rhs) const
	{
		auto ret = * lhs > * rhs;
		return ret;
	}
	
	bool
	Task_handler_sp_comparator::operator()(const Task_handler_sp& lhs,
	                                       const Task_handler_sp& rhs) const
	{
		return lhs->id() > rhs->id();
	}
	
	bool
	Makespan_task_sp_comparator::operator()(const Makespan_task_sp& lhs,
	                                        const Makespan_task_sp& rhs) const
	{
		return * lhs > * rhs;
	}
}
namespace DHCP::Client
{
	std::atomic<uint64_t> Task::_next_id(0);
	
	Task::Task(Time_sequence_sp constraints,
	           uint32_t makespan_from,
	           uint32_t makespan_to,
	           uint32_t level,
	           uint32_t priority,
	           Task_sp parent)
	
	//scales between 1000 and 0
		: _constraints(std::move(constraints)),
		  _makespan_from(makespan_from),
		  _makespan_to(makespan_to),
		  _level(level),
		  _priority(priority),
		  _parent_task(parent),
		  _optimum_makespan(NO_MAKE_SPAN),
		  _optimum_solution(nullptr)
	{_id = _next_id++;}
	
	uint32_t
	Task::id() const
	{return _id;}
	
	Time_sequence_sp
	Task::get_constraints() const
	{
		return _constraints;
	}
	
	uint32_t
	Task::get_makespan_from() const
	{
		return _makespan_from;
	}
	
	uint32_t
	Task::get_makespan_to() const
	{
		return _makespan_to;
	}
	
	uint32_t
	Task::get_level() const
	{
		return _level;
	}
	
	uint32_t
	Task::get_priority() const
	{
		return _priority;
	}
	
	bool
	Task::one_solution_found()
	{return NO_MAKE_SPAN != _optimum_makespan;}
	
	uint32_t
	Task::get_optimum_makespan()
	{
		if(!one_solution_found())
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Can't read optimum makespan since no solution found!";
			}
			throw std::logic_error("Can't read optimum makespan since no solution found!");
		}
		return _optimum_makespan;
	}
	
	Solution_sp
	Task::get_optimum_solution()
	{
		if(!one_solution_found())
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Can't read optimum makespan since no solution found!";
			}
			throw std::logic_error("Can't read optimum makespan since no solution found!");
		}
		return _optimum_solution;
	}
	
	Task_wp
	Task::get_parent()
	{return _parent_task;}
	
	bool
	Task::completed(uint32_t makespan)
	{
		if(makespan < _makespan_from || makespan - _makespan_from >= _all_makespan_records.size())
		{
			return false;
		}
		return true && _all_makespan_records[makespan - _makespan_from];
	}
	
	bool
	Task::completed_satisfiable(uint32_t makespan)
	{
		return completed(makespan) && _all_makespan_records[makespan - _makespan_from]->_satisfiable;
	}
	
	bool
	Task::completed_unsatisfiable(uint32_t makespan)
	{
		return completed(makespan) && !_all_makespan_records[makespan - _makespan_from]->_satisfiable;
	}
	
	uint32_t
	Task::get_no_makespans(bool used,
	                       bool redundant)
	{
		return sum_makespan<uint32_t>(used,
		                              redundant,
		                              uint32_t(0),
		                              [](Makespan_record_sp m)
		                              {return uint32_t(1);});
	}
	
	std::chrono::milliseconds
	Task::get_total_queue_duration(bool used,
	                               bool redundant)
	{
		return sum_makespan<std::chrono::milliseconds>(used,
		                                               redundant,
		                                               std::chrono::milliseconds(0),
		                                               [](Makespan_record_sp m)
		                                               {return m->_queue_duration;});
	}
	
	std::chrono::milliseconds
	Task::get_total_server_duration(bool used,
	                                bool redundant)
	{
		return sum_makespan<std::chrono::milliseconds>(used,
		                                               redundant,
		                                               std::chrono::milliseconds(0),
		                                               [](Makespan_record_sp m)
		                                               {return m->_server_duration;});
	}
	
	std::chrono::milliseconds
	Task::get_total_canceled_duration(bool used,
	                                  bool redundant)
	{
		return sum_makespan<std::chrono::milliseconds>(used,
		                                               redundant,
		                                               std::chrono::milliseconds(0),
		                                               [](Makespan_record_sp m)
		                                               {return m->_canceled_server_duration;});
	}
	
	std::chrono::milliseconds
	Task::get_total_in_server_clingo_duration(bool used,
	                                          bool redundant)
	{
		return sum_makespan<std::chrono::milliseconds>(used,
		                                               redundant,
		                                               std::chrono::milliseconds(0),
		                                               [](Makespan_record_sp m)
		                                               {return m->_in_server_clingo_duration;});
	}
	
	std::chrono::milliseconds
	Task::get_total_in_server_process_duration(bool used,
	                                           bool redundant)
	{
		return sum_makespan<std::chrono::milliseconds>(used,
		                                               redundant,
		                                               std::chrono::milliseconds(0),
		                                               [](Makespan_record_sp m)
		                                               {return m->_in_server_process_duration;});
	}
	
	Makespan_record_sp
	Task::get_makespan_record(uint32_t makespan)
	{
		return (makespan > _makespan_from && makespan - _makespan_from < _all_makespan_records.size() && _all_makespan_records[makespan - _makespan_from]) ? _all_makespan_records[makespan - _makespan_from] : Makespan_record_sp();
	}
	
	bool
	Task::operator>(Task const& other) const
	{
		return std::tie(_level,
		                _priority,
		                _id) > std::tie(other._level,
		                                other._priority,
		                                other._id);
	}
	
	void
	Task::set_makespan_record(uint32_t makespan,
	                          Solution_sp sol,
	                          Makespan_record_sp rec)
	{
		
		if(makespan < _makespan_from || makespan > _makespan_to)
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Outside of defined makespan region!";
			}
			throw std::logic_error("Outside of defined makespan region!");
		}
		
		if(_all_makespan_records.size() < makespan - _makespan_from + 1)
		{
			_all_makespan_records.resize(makespan - _makespan_from + 1);
		}
		
		if(!_all_makespan_records.at(makespan - _makespan_from))
		{
			_all_makespan_records[makespan - _makespan_from] = rec;
		}
		else
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Record for same makespan!";
			}
			throw std::logic_error("Record for same makespan!");
		}
		
		if(rec->_satisfiable && makespan < _optimum_makespan)
		{
			_optimum_makespan = makespan;
			_optimum_solution = sol;
		}
	}
	
	void
	Task::set_task_record(DHCP::Client::Task_record_sp tr)
	{
		_task_record = tr;
	}
	
	Task_record const&
	Task::get_task_record() const
	{
		return * _task_record;
	}
}
namespace DHCP::Client
{
	Task_handler::Task_handler(Task_sp task,
	                           Makespan_queue_sp makespan_taskQ,
	                           std::function<void(Task_sp)> completion_callback,
	                           std::string single_shot_command)
		: _task(task),
		  _makespan_taskQ(makespan_taskQ),
		  _completion_callback(completion_callback),
		  _single_shot_command(single_shot_command),
		  _next_makespan(task->get_makespan_from()),
		  _done_solution_found(false),
		  _task_record(std::make_shared<Task_record>())
	{
	}
	
	void
	Task_handler::init()
	{
		_task_record->_task_start = std::chrono::steady_clock::now();
		generate_new_makespan(3);//Make parametric
	}
	
	uint32_t
	Task_handler::id()
	{return _task->id();}
	
	uint32_t
	Task_handler::level()
	{return _task->get_level();}
	
	void
	Task_handler::makespan_return(uint32_t makespan,
	                              Solution_sp sol,
	                              Makespan_record_sp rec)
	{
		
		std::unique_lock<std::mutex> lock(_return_mtx);
		if(program_instance_data->_verbosity >= 3)
		{
			LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << "TaskID:" << _task->id() << "-" << makespan
			          << " PROCESSING SOLUTION: " << (rec && rec->_satisfiable ? "SATISFIABLE" : "UNSATISFIABLE") << " in "
			          << rec->_server_duration.count() << "ms";
		}
		
		_task->set_makespan_record(makespan,
		                           sol,
		                           rec);
		if(!_done_solution_found && _task->one_solution_found() && //
		   (_task->get_optimum_makespan() == _task->get_makespan_from() //If checked lower bound and it is not satisfiable then return false
		    || (_task->one_solution_found() && _task->completed_unsatisfiable(_task->get_optimum_makespan() - 1)) //OR If there is no better solution then this is the result.
		    || _task->completed_unsatisfiable(_task->get_makespan_to())))
		{
			_done_solution_found = true;
			
			_task_record->_task_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _task_record->_task_start);
			_task->set_task_record(_task_record);
			
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << "Marked solution found of TaskID:"
				          << _task->id() << "-" << makespan << "in " << _task_record->_task_duration.count() << " ms";
			}
			_completion_callback(_task);
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << "Returned solution to TaskID:" << _task->id()
				          << "-" << makespan;
			}
			
			//Kill all remaining
			std::for_each(_running_makespan_tasks_map.begin(),
			              _running_makespan_tasks_map.end(),
			              [](std::pair<uint32_t,
			                           Makespan_task_sp> m)
			              {
				              m.second->cancel();
			              });
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << "Cancelled all makespan task of TaskID:"
				          << _task->id() << "-" << makespan;
			}
		}
		
		//Remove makespan from running
		_running_makespan_tasks_map.erase(makespan);
		if(!_done_solution_found && _running_makespan_tasks_map.empty() && !generate_new_makespan(3))
		{
			//We reached to makespan limit, return a null solution
			_completion_callback(_task);
		}
		
		//		if(_running_makespan_tasks_map.empty())
		//		{
		//			if(!_done_solution_found)
		//			{
		//				if(!generate_new_makespan(5))
		//				{
		//					//We reached to makespan limit, return a null solution
		//					_completion_callback(_task);
		//				}
		//			}
		//			else
		//			{
		////				if(program_instance_data->_verbosity >= 3)
		////				{
		////					LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << "Returning solution to TaskID:"
		////					          << _task->id() << "-" << makespan;
		////				}
		//				//There is nothing more to do by this task
		//				_completion_callback(_task);
		//				if(program_instance_data->_verbosity >= 3)
		//				{
		//					LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << "Returned solution to TaskID:"
		//					          << _task->id() << "-" << makespan;
		//				}
		//			}
		//			if(!rec->_redundant)
		//			{
		//				//No longer need to look upper
		//				std::for_each(_running_makespan_tasks_map.begin(),
		//				              _running_makespan_tasks_map.end(),
		//				              [upper = rec->_satisfiable](std::pair<uint32_t,
		//				                                                    Makespan_task_sp>& m)
		//				              {
		//					              if((upper && m.first > makespan)//
		//					                 || ((!upper && m.first < makespan)))
		//					              {
		//						              m.second->cancel();
		//					              }
		//				              });
		//			}
		
		//	}
	}
	
	void
	Task_handler::cancel()
	{
		std::unique_lock<std::mutex> lock(_return_mtx);
		_done_solution_found = true;
		//kill existing ones
		std::for_each(_running_makespan_tasks_map.begin(),
		              _running_makespan_tasks_map.end(),
		              [](std::pair<uint32_t,
		                           Makespan_task_sp> m)
		              {
			              m.second->cancel();
		              });
	}
	
	Makespan_task_sp
	Task_handler::new_makespan_task(uint32_t makespan)
	{
		if(!_done_solution_found)
		{
			//			auto self = shared_from_this();
			auto mst = std::make_shared<Makespan_task>(_task->id(),
			                                           _task->get_constraints(),
			                                           makespan,
			                                           _task->get_level(),
			                                           _task->get_priority(),
			                                           [this,
			                                            makespan](Solution_sp sol,
			                                                      Makespan_record_sp rec)
			                                           {
				                                           makespan_return(makespan,
				                                                           sol,
				                                                           rec);
			                                           },
			                                           _single_shot_command + " -pts " + std::to_string(makespan) + " ");
			mst->queued([this,
			             &mst]()
			            {
				            _makespan_taskQ->push(mst);
			            });
			
			return mst;
		}
		else
		{
			return Makespan_task_sp();
		}
	}
	
	bool
	Task_handler::generate_new_makespan(uint32_t amount)
	{
		bool retval = false;
		
		if(program_instance_data->_verbosity >= 3)
		{
			LOG(INFO) << "Thread ID(" << std::this_thread::get_id() << ") " << " TaskID:" << std::to_string(_task->id())
			          << " Generating " << amount << " more makespans [" << _next_makespan << "-"
			          << _next_makespan + amount - 1 << "]";
		}
		while(amount-- && _next_makespan <= _task->get_makespan_to())//TODO check if true statement, consider 1
		{
			auto ms_t = new_makespan_task(_next_makespan);
			if(ms_t)
			{
				_running_makespan_tasks_map.try_emplace(_next_makespan,
				                                        ms_t);
				_next_makespan++;
				retval = true;
			}
		}
		return retval;
	}
}
namespace DHCP::Client
{
	Makespan_task::Makespan_task(uint32_t task_id,
	                             Time_sequence_sp constraints,
	                             uint32_t makespan,
	                             uint32_t level,
	                             uint32_t priority,
	                             std::function<void(Solution_sp sol,
	                                                Makespan_record_sp rec)> callback,
	                             std::string single_shot_command)
		: _task_id(task_id),
		  _constraints(std::move(constraints)),
		  _makespan(makespan),
		  _level(level),
		  _priority(priority),
		  _solution_callback(std::move(callback)),
		  _single_shot_command(std::move(single_shot_command)),
		  _state(Makespan_task_state::NOT_STARTED),
		  _cancel_start(Time_point::min()),
		  _cancelled_duration(0),
		  _queue_start(Time_point::min()),
		  _queue_duration(0),
		  _server_start(Time_point::min()),
		  _server_duration(0)
	{
	}
	
	uint32_t
	Makespan_task::task_id()
	{return _task_id;}
	
	Time_sequence const&
	Makespan_task::get_constraints()
	{return * _constraints;}
	
	uint32_t
	Makespan_task::get_makespan()
	{return _makespan;}
	
	uint32_t
	Makespan_task::get_level()
	{return _level;}
	
	uint32_t
	Makespan_task::get_priority()
	{return _priority;}
	
	std::string
	Makespan_task::get_single_shot_command()
	{return _single_shot_command;}
	
	//	bool
	//	Makespan_task::is_not_started()
	//	{return Makespan_task_state::NOT_STARTED == _state;}
	//
	//	bool
	//	Makespan_task::is_in_queue()
	//	{return Makespan_task_state::IN_QUEUE == _state;}
	//
	//	bool
	//	Makespan_task::is_computing()
	//	{return Makespan_task_state::COMPUTING == _state;}
	//
	//	bool
	//	Makespan_task::is_completed()
	//	{return Makespan_task_state::COMPLETED == _state;}
	//
	//	bool
	//	Makespan_task::is_canceled()
	//	{return Makespan_task_state::CANCELED == _state;}
	//
	//	bool
	//	Makespan_task::is_redundant()
	//	{return Makespan_task_state::REDUNDANT == _state;}
	
	bool
	Makespan_task::queued(std::function<void()> op)
	{
		std::lock_guard<std::mutex> lock(_state_spl);
		if(Makespan_task_state::NOT_STARTED == _state)
		{
			_queue_start = std::chrono::steady_clock::now();
			op();
			_state = Makespan_task_state::IN_QUEUE;
			return true;
		}
		else
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Makespan in wrong state!";
			}
			throw std::logic_error("Makespan in wrong state!");
		}
	}
	
	bool
	Makespan_task::finding_solver()
	{
		std::lock_guard<std::mutex> lock(_state_spl);
		if(Makespan_task_state::IN_QUEUE == _state)
		{
			return true;
		}
		else if(Makespan_task_state::REDUNDANT == _state)
		{
			return false;
		}
		else
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Makespan in wrong state!";
			}
			throw std::logic_error("Makespan in wrong state!");
		}
	}
	
	bool
	Makespan_task::start(std::function<void()> op,
	                     std::function<void()> terminate_computation)
	{
		std::unique_lock<std::mutex> lock(_state_spl);
		_queue_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _queue_start);
		if(Makespan_task_state::IN_QUEUE == _state)
		{
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "Starting TaskID:" << _task_id << "-" << _makespan
				          << " Server command:" << std::endl << get_single_shot_command();
			}
			_server_start          = std::chrono::steady_clock::now();
			_terminate_computation = terminate_computation;
			op();
			_state = Makespan_task_state::COMPUTING;
			return true;
		}
		else if(Makespan_task_state::REDUNDANT == _state)
		{
			return false;
		}
		else
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Makespan in wrong state!";
			}
			throw std::logic_error("Makespan in wrong state!");
		}
	}
	
	bool
	Makespan_task::solution(bool satisfiable,
	                        Time_sequence_sp fluent,
	                        Time_sequence_sp act_actuation,
	                        Time_sequence_sp act_sensing,
	                        Time_sequence_sp outcome,
	                        Time_sequence_sp additional,
	                        std::chrono::milliseconds in_server_clingo_duration,
	                        std::chrono::milliseconds in_server_process_duration)
	{
		
		std::unique_lock<std::mutex> lock(_state_spl);
		_server_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _server_start);
		if(Makespan_task_state::COMPUTING == _state)
		{
			_state = Makespan_task_state::COMPLETED;
			lock.unlock();
			_solution_callback(std::make_shared<Solution>(fluent,
			                                              act_actuation,
			                                              act_sensing,
			                                              outcome,
			                                              additional),
			                   std::make_shared<Makespan_record>(false,
			                                                     satisfiable,
			                                                     _queue_duration,
			                                                     _server_duration,
			                                                     std::chrono::milliseconds(0),
			                                                     in_server_clingo_duration,
			                                                     in_server_process_duration));
			
			return true;
		}
		else if(Makespan_task_state::CANCELED == _state)
		{
			_cancelled_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _cancel_start);
			lock.unlock();
			_solution_callback(std::make_shared<Solution>(),
			                   std::make_shared<Makespan_record>(true,
			                                                     false,
			                                                     _queue_duration,
			                                                     _server_duration,
			                                                     _cancelled_duration,
			                                                     in_server_clingo_duration,
			                                                     in_server_process_duration));
			return false;
		}
		else
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "Makespan in wrong state!";
			}
			throw std::logic_error("Makespan in wrong state!");
		}
	}
	
	bool
	Makespan_task::cancel()
	{
		std::unique_lock<std::mutex> lock(_state_spl);
		_cancel_start = std::chrono::steady_clock::now();
		if(Makespan_task_state::IN_QUEUE == _state)
		{
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "TaskID:" << _task_id << "-" << _makespan << " cancelled in queue!";
			}
			_state = Makespan_task_state::REDUNDANT;
			//Return solution now since this makespan task will never be computed
			//Do async to prevent deadlock of nested _solution_callback calls
			lock.unlock();
			std::thread([this]()
			            {
				            _solution_callback(std::make_shared<Solution>(),
				                               std::make_shared<Makespan_record>(true,
				                                                                 false,
				                                                                 _queue_duration,
				                                                                 _server_duration,
				                                                                 std::chrono::milliseconds(0),
				                                                                 std::chrono::milliseconds(0),
				                                                                 std::chrono::milliseconds(0)));
			            }).detach();
			
			return true;
		}
		else if(Makespan_task_state::COMPUTING == _state)
		{
			if(program_instance_data->_verbosity >= 3)
			{
				LOG(INFO) << "TaskID:" << _task_id << "-" << _makespan << " cancelled during computing!";
			}
			_state = Makespan_task_state::CANCELED;
			lock.unlock();
			_terminate_computation();
			//Don't stop timing since the set_completed will be called
			return true;
		}
	}
	
	bool
	Makespan_task::operator>(Makespan_task const& other) const
	{
		//Lower has higher priority except makespan
		return std::tie(_level,
		                _priority,
		                other._makespan,//higher makespan has higher priority
		                _task_id) > std::tie(other._level,
		                                     other._priority,
		                                     _makespan,
		                                     other._task_id);
	}
}