//
// Created by f on 25/01/18.
//

#include <fstream>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <planner.h>

namespace DHCP::Client
{
	
	void
	Planner::report_statistics()
	{
		uint32_t title_size = 40;
		uint32_t max_size   = 40;
		uint32_t value_size = 20;
		
		std::string output;
		
		auto print_space = [&output]()
		{
			output += "\n";
		};
		auto print_sep   = [title_size,
		                    max_size,
		                    value_size,
		                    &output]()
		{
			
			output += std::string(title_size,
			                      '=') + "+" + std::string(max_size,
			                                               '=') + "\n";
		};
		
		auto print_partial_sep = [title_size,
		                          max_size,
		                          value_size,
		                          &output]()
		{
			
			output += std::string(title_size,
			                      '-') + "+" + std::string(max_size,
			                                               '-') + "\n";
		};
		
		auto print_title = [title_size,
		                    max_size,
		                    value_size,
		                    &output](std::string title)
		{
			
			std::stringstream outputs;
			outputs << std::left << std::setw(title_size) << title << "|" << std::endl;
			output += outputs.str();
		};
		
		auto print_val = [title_size,
		                  max_size,
		                  value_size,
		                  &output](std::string title,
		                           auto value)
		{
			
			std::stringstream outputs;
			outputs << std::left << std::setw(title_size) << title << ":" //
			        << std::right << std::setw(value_size) << value << std::endl;
			output += outputs.str();
		};
		
		if(!_initial)
		{
			output = "NO plan found!";
		}
		else
		{
			
			
			//----------------------------------------------------------------------------------------
			//----------------------------------------------------------------------------------------
			
			print_sep();
			print_sep();
			print_title("PLANNING");
			print_sep();
			print_val("Planning Duration",
			          _stat_total_planning_duration.count());
			
			print_sep();
			print_sep();
			print_title("Longest Main Task Chain");
			Task_sp                   cur_task = _last_task;
			std::chrono::milliseconds total_in_server_clingo_dur(0);
			while(cur_task)
			{
				auto dur = cur_task->get_total_in_server_clingo_duration(true,
				                                                         true);
				total_in_server_clingo_dur += dur;
				print_val("TaskID " + std::to_string(cur_task->id()),
				          dur.count());
				cur_task = cur_task->get_parent().lock();
			}
			print_val("Total Clingo Duration:",
			          total_in_server_clingo_dur.count());
			
			print_sep();
			print_sep();
			print_title("PLAN STATISTICS");
			print_sep();
			
			auto cumulative_graph_record = [this](std::function<uint64_t(Step_sp)> get_value) -> uint64_t
			{
				uint64_t cur_sum = 0;
				for(auto& s:_visited_state_steps)
				{
					cur_sum += get_value(s.second);
				}
				return cur_sum;
			};
			
			auto cumulative_tree_record = [this](std::function<uint64_t(Step_sp)> get_value) -> uint64_t
			{
				uint64_t             ret = 0;
				std::vector<Step_sp> visit;
				visit.push_back(_initial);
				
				while(!visit.empty())
				{
					auto cur_step = visit.back();
					ret += get_value(cur_step);
					visit.pop_back();
					
					for(auto p:cur_step->get_outcome())
					{
						auto new_step = p.second.lock();
						if(new_step)
						{
							visit.push_back(new_step);
						}
					}
				}
				return ret;
			};
			
			print_title("Tree");
			auto gsens_act = cumulative_tree_record([](Step_sp s) -> uint64_t
			                                        {
				                                        return Step::State_type::INTERMEDIATE == s->state_type() && Step::Action_type::SENSING == s->action_type() ? 1 : 0;
			                                        });
			
			auto gactu_act = cumulative_tree_record([](Step_sp s) -> uint64_t
			                                        {
				                                        return Step::State_type::INTERMEDIATE == s->state_type() && Step::Action_type::ACTUATION == s->action_type() ? 1 : 0;
			                                        });
			
			auto gleaft = cumulative_tree_record([](Step_sp s) -> uint64_t
			                                     {
				                                     return Step::State_type::GOAL == s->state_type() ? 1 : 0;
			                                     });
			
			print_val("# Sensing Act.",
			          gsens_act);
			print_val("# Actuation Act.",
			          gactu_act);
			print_val("Tree Size",
			          gsens_act + gactu_act);
			print_val("# Leaves",
			          gleaft);
			
			print_title("Graph");
			print_val("# Sensing Act.",
			          cumulative_graph_record([](Step_sp s) -> uint64_t
			                                  {return s->get_level() == 0 && Step::State_type::INTERMEDIATE == s->state_type() && s->action_type() == Step::Action_type::SENSING ? 1 : 0;}));
			
			print_val("# Actuation Act.",
			          cumulative_graph_record([](Step_sp s) -> uint64_t
			                                  {return s->get_level() == 0 && Step::State_type::INTERMEDIATE == s->state_type() && s->action_type() == Step::Action_type::ACTUATION ? 1 : 0;}));
			
			
			//----------------------------------------------------------------------------------------
			//----------------------------------------------------------------------------------------
			
			
			
			print_sep();
			print_sep();
			print_title("TASK STATISTICS");
			print_partial_sep();
			
			std::vector<std::string> main_landmark_titles{"",
			                                              "Main",
			                                              "Landmark",
			                                              "Main+Landmark"};
			
			for(uint32_t main_landmark = 1;
			    main_landmark < 4;
			    main_landmark++)
			{
				bool                     show_main     = (main_landmark & 0x1) > 0;
				bool                     show_landmark = (main_landmark & 0x2) > 0;
				std::vector<std::string> used_redundant_titles{"",
				                                               "Used Task",
				                                               "Redundant Task",
				                                               "All Task"};
				
				auto sum_tasks = [show_main,
				                  show_landmark,
				                  this](std::function<auto(Task_sp)> f,
				                        auto zero_val)
				{
					return std::accumulate(_completed_task_list.begin(),
					                       _completed_task_list.end(),
					                       zero_val,
					                       [&](auto sum,
					                           Task_sp t)
					                       {
						                       return (show_main && t->get_level() == 0) ||//
						                              (show_landmark && t->get_level() > 0) //
						                              ? sum + f(t) : sum;
					                       });
				};
				
				auto task = sum_tasks(std::function<uint32_t(Task_sp)>([](Task_sp t) -> uint32_t
				                                                       {return uint32_t(1);}),
				                      uint32_t(0));
				
				print_space();
				print_space();
				print_sep();
				print_sep();
				print_title(main_landmark_titles[main_landmark]);
				print_val("# Tasks",
				          task);
				
				for(uint32_t used_redundant = 1;
				    used_redundant < 4;
				    used_redundant++)
				{
					bool show_used      = (used_redundant == 1 || used_redundant == 3);
					bool show_redundant = (used_redundant == 2 || used_redundant == 3);
					print_title(main_landmark_titles[main_landmark] + " " + used_redundant_titles[used_redundant]);
					print_partial_sep();
					
					auto makespan = sum_tasks(std::function<uint32_t(Task_sp)>([show_used,
					                                                            show_redundant](Task_sp t) -> uint32_t
					                                                           {
						                                                           return t->get_no_makespans(show_used,
						                                                                                      show_redundant);
					                                                           }),
					                          uint32_t(0));
					
					print_val("# Makespan",
					          makespan);
					print_val("Avg. # Makespan/Task",
					          double(makespan) / task);
					
					//----------------------------------------
					{
						auto queue_time = sum_tasks(std::function<std::chrono::milliseconds(Task_sp)>([show_used,
						                                                                               show_redundant](Task_sp t) -> std::chrono::milliseconds
						                                                                              {
							                                                                              return t->get_total_queue_duration(show_used,
							                                                                                                                 show_redundant);
						                                                                              }),
						                            std::chrono::milliseconds(0)).count();
						print_val("In Queue",
						          queue_time);
						
						print_val("Avg. In Queue",
						          queue_time / double(makespan));
					}
					
					//----------------------------------------
					{
						auto server_time = sum_tasks(std::function<std::chrono::milliseconds(Task_sp)>([show_used,
						                                                                                show_redundant](Task_sp t) -> std::chrono::milliseconds
						                                                                               {
							                                                                               return t->get_total_server_duration(show_used,
							                                                                                                                   show_redundant);
						                                                                               }),
						                             std::chrono::milliseconds(0)).count();
						print_val("Server",
						          server_time);
						
						print_val("Avg. Server",
						          server_time / double(makespan));
					}
					
					
					//----------------------------------------
					{
						auto cancel_time = sum_tasks(std::function<std::chrono::milliseconds(Task_sp)>([show_used,
						                                                                                show_redundant](Task_sp t) -> std::chrono::milliseconds
						                                                                               {
							                                                                               return t->get_total_canceled_duration(show_used,
							                                                                                                                     show_redundant);
						                                                                               }),
						                             std::chrono::milliseconds(0)).count();
						print_val("Cancel",
						          cancel_time);
						
						print_val("Avg. Cancel",
						          cancel_time / double(makespan));
					}
					//----------------------------------------
					{
						auto in_server_clingo = sum_tasks(std::function<std::chrono::milliseconds(Task_sp)>([show_used,
						                                                                                     show_redundant](Task_sp t) -> std::chrono::milliseconds
						                                                                                    {
							                                                                                    return t->get_total_in_server_clingo_duration(show_used,
							                                                                                                                                  show_redundant);
						                                                                                    }),
						                                  std::chrono::milliseconds(0)).count();
						print_val("In Server Clingo",
						          in_server_clingo);
						
						print_val("Avg. In Server Clingo",
						          in_server_clingo / double(makespan));
					}
					//----------------------------------------
					{
						auto in_server_process = sum_tasks(std::function<std::chrono::milliseconds(Task_sp)>([show_used,
						                                                                                      show_redundant](Task_sp t) -> std::chrono::milliseconds
						                                                                                     {
							                                                                                     return t->get_total_in_server_process_duration(show_used,
							                                                                                                                                    show_redundant);
						                                                                                     }),
						                                   std::chrono::milliseconds(0)).count();
						print_val("In Server Process",
						          in_server_process);
						
						print_val("Avg. In Server Process",
						          in_server_process / double(makespan));
					}
					
					//----------------------------------------
					
					print_title("Clingo durations");
					for(uint32_t m = 0;
					    m <= ::DHCP::Client::program_instance_data->_makespan_limit;
					    m++)
					{
						auto sat_ms_dur = sum_tasks(std::function<std::chrono::milliseconds(Task_sp)>([show_used,
						                                                                               show_redundant,
						                                                                               m](Task_sp t) -> std::chrono::milliseconds
						                                                                              {
							                                                                              auto mr = t->get_makespan_record(m);
							                                                                              if(mr)//weird that checking mr failes in the beginning of the following if check
							                                                                              {
								                                                                              if(mr->_satisfiable && ((mr->_redundant && show_redundant) || (!mr->_redundant && show_used)))
								                                                                              {
									                                                                              return mr->_in_server_clingo_duration;
								                                                                              }
							                                                                              }
							                                                                              else
							                                                                              {
								                                                                              return std::chrono::milliseconds(0);
							                                                                              }
						                                                                              }),
						                            std::chrono::milliseconds(0));
						
						auto sat_ms_no = sum_tasks(std::function<uint32_t(Task_sp)>([show_used,
						                                                             show_redundant,
						                                                             m](Task_sp t) -> uint32_t
						                                                            {
							                                                            auto mr = t->get_makespan_record(m);
							                                                            if(mr)//weird that checking mr failes in the beginning of the following if check
							                                                            {
								                                                            if(mr->_satisfiable && ((mr->_redundant && show_redundant) || (!mr->_redundant && show_used)))
								                                                            {
									                                                            return uint32_t(1);
								                                                            }
							                                                            }
							                                                            else
							                                                            {
								                                                            return uint32_t(0);
							                                                            }
						                                                            }),
						                           uint32_t(0));
						
						auto unsat_ms_dur = sum_tasks(std::function<std::chrono::milliseconds(Task_sp)>([show_used,
						                                                                                 show_redundant,
						                                                                                 m](Task_sp t) -> std::chrono::milliseconds
						                                                                                {
							                                                                                auto mr = t->get_makespan_record(m);
							                                                                                if(mr)//weird that checking mr failes in the beginning of the following if check
							                                                                                {
								                                                                                if(!mr->_satisfiable && ((mr->_redundant && show_redundant) || (!mr->_redundant && show_used)))
								                                                                                {
									                                                                                return mr->_in_server_clingo_duration;
								                                                                                }
							                                                                                }
							                                                                                else
							                                                                                {
								                                                                                return std::chrono::milliseconds(0);
							                                                                                }
						                                                                                }),
						                              std::chrono::milliseconds(0));
						
						auto unsat_ms_no = sum_tasks(std::function<uint32_t(Task_sp)>([show_used,
						                                                               show_redundant,
						                                                               m](Task_sp t) -> uint32_t
						                                                              {
							                                                              auto mr = t->get_makespan_record(m);
							                                                              if(mr)//weird that checking mr failes in the beginning of the following if check
							                                                              {
								                                                              if(!mr->_satisfiable && ((mr->_redundant && show_redundant) || (!mr->_redundant && show_used)))
								                                                              {
									                                                              return uint32_t(1);
								                                                              }
							                                                              }
							                                                              else
							                                                              {
								                                                              return uint32_t(0);
							                                                              }
						                                                              }),
						                             uint32_t(0));
						
						if(sat_ms_dur.count() != 0 || sat_ms_no != 0 || unsat_ms_dur.count() != 0 || unsat_ms_no != 0)
						{
							print_partial_sep();
							print_val("Makespan " + std::to_string(m) + " sat. duration",
							          sat_ms_dur.count());
							print_val("Makespan " + std::to_string(m) + " sat. number",
							          sat_ms_no);
							print_val("Makespan " + std::to_string(m) + " sat. avg. dur.",
							          sat_ms_no == 0 ? 0 : (sat_ms_dur.count() / double(sat_ms_no)));
							print_space();
							print_val("Makespan " + std::to_string(m) + " unsat. duration",
							          unsat_ms_dur.count());
							print_val("Makespan " + std::to_string(m) + " unsat. number",
							          unsat_ms_no);
							print_val("Makespan " + std::to_string(m) + " unsat. avg. dur.",
							          unsat_ms_no == 0 ? 0 : (unsat_ms_dur.count() / double(unsat_ms_no)));
						}
					}
					print_partial_sep();
				}
			}
			std::cout << output << std::endl;
		}
		std::ofstream ofile("stats.txt");
		ofile << output;
		ofile.close();
		if(program_instance_data->_verbosity >= 1)
		{
			LOG(INFO) << std::endl << output;
		}
	}
	
	void
	Planner::report_graph()
	{
		struct Edge
		{
			std::string label;
			std::string color;
		};
		struct Vertex
		{
			uint64_t    id;
			std::string name;
			std::string color;
			std::string style;
			std::string shape;
		};
		/**Container for all visited steps*/
		//		std::unordered_map<Hashed_sp<Step>,
		//		                   Step_sp,
		//		                   Hashed_sp_operators<Step>> _visited_state_steps;
		//
		/**Container for all hanging roots besides problem initial "_initial"*/
		std::set<Step_sp> _hanging_roots;
		
		/**Problem initial step*/
		Step_sp _initial;
		using Graph = boost::adjacency_list<boost::listS,
		                                    boost::listS,
		                                    boost::directedS,
		                                    Vertex,
		                                    Edge>;
		Graph g;
		
		
		
		using G_vertex_desc = boost::graph_traits<Graph>::vertex_descriptor;
		using G_edge_desc = boost::graph_traits<Graph>::edge_descriptor;
		
		std::unordered_map<Hashed_sp<Step>,
		                   G_vertex_desc,
		                   Hashed_sp_operators<Step>> vertices;
		std::for_each(_visited_state_steps.begin(),
		              _visited_state_steps.end(),
		              [&](std::pair<Hashed_sp<Step>,
		                            Step_sp> const& p)
		              {
			              Step_sp     s = p.second;
			              std::string fillcolor;
			              std::string shape;
			              if(Step::State_type::GOAL == s->state_type())
			              {
				              fillcolor = "mediumseagreen";
				              shape     = "invtriangle";
			              }
			              else if(Step::Action_type::SENSING == s->action_type())
			              {
				              fillcolor = "firebrick2";
				              shape     = "ellipse";
			              }
			              else if(Step::Action_type::ACTUATION == s->action_type())
			              {
				              fillcolor = "bisque1";
				              shape     = "ellipse";
			              }
			
			              vertices[p.first] = boost::add_vertex(Vertex{reinterpret_cast< std::uint64_t > (s.get()),
			                                                           s->to_string_actions(),
			                                                           fillcolor,
			                                                           "filled",
			                                                           shape},
			                                                    g);
			              //			              v_name[v]         = s->to_string_actions();
		              });
		
		//		auto _init_hash = Hashed_sp<Step>(_initial);
		//		auto init_it = vertices.find(_init_hash);
		//		if(vertices.end() != init_it)
		//		{
		//			v_color[*init_it]=
		//		}
		//		else
		//		{
		//			std::logic_error("Initial state is not in visited_states");
		//		}
		
		std::for_each(_visited_state_steps.begin(),
		              _visited_state_steps.end(),
		              [&](std::pair<Hashed_sp<Step>,
		                            Step_sp> const& p)
		              {
			              Step_sp  s  = p.second;
			              auto     v1 = vertices[Hashed_sp<Step>(s)];
			              //			              if(vertices.end() == v1)
			              //			              {
			              //				              throw std::runtime_error("vertex v1 not found");
			              //			              }
			              for(auto op:s->get_outcome())
			              {
				              auto label = op.first.to_string();
				              auto to    = op.second.lock();
				              if(to)
				              {
					              auto v2 = vertices[Hashed_sp<Step>(to)];
					              //				              if(vertices.end() == v2)
					              //				              {
					              //					              throw std::runtime_error("vertex v2 not found");
					              //				              }
					              boost::add_edge(v1,
					                              v2,
					                              Edge{label,
					                                   label.empty() ? "black" : "red"},
					                              g);
				              }
				              //				              e_name[e1.first] = label;
			              }
		              });
		
		boost::dynamic_properties dp;
		dp.property("vid",
		            boost::get(& Vertex::id,
		                       g));
		dp.property("label",
		            boost::get(& Vertex::name,
		                       g));
		dp.property("style",
		            boost::get(& Vertex::style,
		                       g));
		dp.property("fillcolor",
		            boost::get(& Vertex::color,
		                       g));
		
		dp.property("shape",
		            boost::get(& Vertex::shape,
		                       g));
		
		dp.property("label",
		            boost::get(& Edge::label,
		                       g));
		
		dp.property("color",
		            boost::get(& Edge::color,
		                       g));
		
		std::ofstream dotfile("test.dot");
		boost::write_graphviz_dp(dotfile,
		                         g,
		                         dp,
		                         std::string("vid"));
	}
}
