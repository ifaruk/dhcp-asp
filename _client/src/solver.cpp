//
// Created by f on 13/02/18.
//
#include <solver.h>

namespace DHCP::Client
{
	
	Solver::Solver(uint32_t solver_id,
	               uint32_t plan_time_span,
	               uint32_t constraint_time_span,
	               Server_stub_sp server_stub)
		: _solver_id(solver_id),
		  _problem_makespan(plan_time_span),
		  _constraint_makespan(constraint_time_span),
		  _server_stub(std::move(server_stub)),
		  _use_count(0),
		  _reserved(false)
	{
		_reserved.clear();
	}
	
	std::tuple<bool,//satisfiable
	           Time_sequence_sp,//Fluent
	           Time_sequence_sp,//Act_actuation
	           Time_sequence_sp,//Act_sensing
	           Time_sequence_sp,//outcome
	           Time_sequence_sp,//additional
	           std::chrono::milliseconds,//clingo time
	           std::chrono::milliseconds>//server total time
	Solver::solve(Time_sequence const& constraints)
	{
		increment_use_count();
		auto res = _server_stub->rpc_solve(_solver_id,
		                                   constraints);
		//		release();
		return std::make_tuple<bool,//satisfiable
		                       Time_sequence_sp,//Fluent
		                       Time_sequence_sp,//Act_actuation
		                       Time_sequence_sp,//Act_sensing
		                       Time_sequence_sp,//outcome
		                       Time_sequence_sp,//additional
		                       std::chrono::milliseconds,//clingo time
		                       std::chrono::milliseconds>//server total time
			(std::move(std::get<1>(res)),//satisfiable
			 std::move(std::get<2>(res)),//fluent
			 std::move(std::get<3>(res)),//act_actuation
			 std::move(std::get<4>(res)),//act_sensing
			 std::move(std::get<5>(res)),//outcome
			 std::move(std::get<6>(res)),//additional
			 std::chrono::milliseconds(std::get<7>(res)),//clingo time
			 std::chrono::milliseconds(std::get<8>(res)));//solver time
	}
	
	void
	Solver::cancel()
	{
		_server_stub->rpc_cancel(_solver_id);
		release();
	}
}