//
// Created by f on 01/11/17.
//

#include <server_handler.h>
#include <makespan_queue.h>

namespace DHCP::Client
{
	
	Server_handler::Server_handler(std::vector<std::string> machine_ip,
	                               Makespan_queue_sp makespan_taskQ_handler)
		: _machine_ip(std::move(machine_ip)),
		  _makespan_taskQ(std::move(makespan_taskQ_handler)),
		  _done(false)
	{
		
		//Create Server configuration
		//Generate New Server Objects
		for(auto mip:      _machine_ip)
		{
			auto     sstub   = std::make_shared<Server_stub>(mip);
			uint32_t next_id = 0;
			if(sstub->is_alive())
			{
				auto s = std::make_shared<Server>(program_instance_data->_domain_file_content,
				                                  program_instance_data->_goal_file_content,
				                                  program_instance_data->_domain_insight_file_content,
				                                  next_id++,
				                                  sstub);
				//Contact Server
				//Get Server Properties such as No. Cores, CPU speed, Network Speed etc.
				if(s)
				{
					_servers.emplace_back(s);
					_server_feeders.emplace_back(& Server_handler::server_feeder,
					                             this,
					                             s);
					//					for(int i = 0;
					//					    i < s->get_max_workload();
					//					    i++)
					//					{
					//						_server_feeders.emplace_back(& Server_handler::server_feeder,
					//						                             this,
					//						                             s);
					//					}
					
					if(program_instance_data->_verbosity >= 3)
					{
						LOG(INFO) << "Server found:" << mip << std::endl << "No Processors: " << s->get_no_processors()
						          << std::endl << "No Cores/Processor: " << s->get_no_cores_per_processor() << std::endl
						          << "No Threads/Core: " << s->get_no_threads_per_core() << std::endl << "CPU MHz: "
						          << s->get_cpu_MHz() << std::endl << "Memory MB: " << s->get_memory_MB() << std::endl
						          << "Available workload: " << s->get_max_workload();
					}
				}
			}
			else
			{
				_dead_servers.push_back(mip);
			}
		}
		
		std::sort(_servers.begin(),
		          _servers.end());
		
		if(_servers.empty())
		{
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << "No usable server exists!";
			}
			throw std::invalid_argument("No usable server exists!");
		}
		
		
		
		//		//Initially create solvers up to
		//		for(auto s:_servers)
		//		{
		//			for(uint32_t ms = 0;
		//			    ms <= _pd->_initial_server_solves_makespan;
		//			    ms++)
		//			{
		//				s->add_and_reserve_solver(ms,
		//				              s->available_workload());
		//			}
		//		}
	}
	
	Server_handler::~Server_handler()
	{
		_done = true;
		for(auto& sf:_server_feeders)
		{
			try
			{
				sf.join();
			}
			catch(std::exception const& e)
			{
				if(program_instance_data->_verbosity >= 2)
				{
					LOG(WARNING) << "Thread ID(" << std::this_thread::get_id() << ") " << "Couldn't join server feeder"
					             << e.what();
				}
			}
		}
		//		for(auto& s:_servers)
		//		{
		//			s->signal_available_load();
		//		}
		
		//		_load_balancer_th.join();
	}
	
	void
	Server_handler::server_feeder(Server_sp server)
	{
		
		auto new_solver = [&](uint32_t makespan,
		                      bool dummy_solve)
		{
			auto solver = server->add_and_reserve_solver(makespan);
			if(dummy_solve)
			{
				server->blocking_reserve_workload();
				solver->solve(Time_sequence());
				server->release_workload();
			}
			//Create new solver thread
			std::thread([&,
			             server,
			             solver,
			             makespan]()
			            {
				            auto my_makespan = makespan;
				
				            Makespan_task_sp new_makespan_task;
				            while(!_done && _makespan_taskQ->wait_pop(my_makespan,
				                                                      new_makespan_task))
				            {
					            if(new_makespan_task->finding_solver())
					            {
						            std::tuple<bool,//satisfiable
						                       Time_sequence_sp,//Fluent
						                       Time_sequence_sp,//Act_actuation
						                       Time_sequence_sp,//Act_sensing
						                       Time_sequence_sp,//outcome
						                       Time_sequence_sp,//additional
						                       std::chrono::milliseconds,//clingo time
						                       std::chrono::milliseconds>//server total time
							            ret;
						            if(new_makespan_task->start([&solver,
						                                         &new_makespan_task,
						                                         &ret,
						                                         &server]()
						                                        {
							                                        server->blocking_reserve_workload();
							                                        ret = solver->solve(new_makespan_task->get_constraints());
							                                        server->release_workload();
						                                        },
						                                        [&solver]()
						                                        {
							                                        solver->cancel();
						                                        }))
						            {
							            new_makespan_task->solution(std::get<0>(ret),
							                                        std::get<1>(ret),
							                                        std::get<2>(ret),
							                                        std::get<3>(ret),
							                                        std::get<4>(ret),
							                                        std::get<5>(ret),
							                                        std::get<6>(ret),
							                                        std::get<7>(ret));
						            }
					            }
				            }
			            }).detach();
		};
		
		uint32_t excluding_solver_limit = 0;
		while(!_done)
		{
			auto q_sizes = _makespan_taskQ->get_sizes();
			auto biggest = std::find_if(q_sizes.rbegin(),
			                            q_sizes.rend(),
			                            [](uint32_t t)
			                            {return t > 0;});
			auto dist    = std::distance(biggest,
			                             q_sizes.rend());
			
			while(excluding_solver_limit <= dist)
			{
				new_solver(excluding_solver_limit++,
				           false);
			}
			while(excluding_solver_limit <= dist + 5)
			{
				new_solver(excluding_solver_limit++,
				           false);
				//give dummy task to spend time
			}
			for(uint32_t i = 0;
			    i < q_sizes.size();
			    i++)
			{
				if(q_sizes.at(i) > 3)
				{
					new_solver(i,
					           false);
				}
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}
	
	/*//			uint32_t time = 0;
	//			while(_timed_taskQ_handler_sp->lock_time(time))//This is blocking function until time is released for others
	//			{
	//				auto& time_queue = _timed_taskQ_handler_sp->get_time(time);
	//				bool overwhelmed = false;
	//				while(!_done_solution_found && !overwhelmed && !time_queue.empty())
	//				{
	//					auto new_job = time_queue.front();
	//					if(!new_job->_job_desc->is_canceled())
	//					{
	//						std::function<Makespan_task_sp(void)> op;
	//						auto                                  ret = server->get_solver(new_job->_job_desc,
	//						                                                                  op);
	//						if(Server::Get_solver_return::NOT_ENOUGH_SOLVER == ret)
	//						{
	//							//Add new solver if there isn't enough
	//							server->configure(time,
	//							             true);
	//							ret = server->get_solver(new_job->_job_desc,
	//							                            op);
	//						}
	//
	//						if(Server::Get_solver_return::OK == ret)
	//						{
	//							#ifdef DHCP_DEBUG
	//							LOG(DEBUG) << "Thread ID(" << std::this_thread::get_id() << ") " <<"Sending to Server " << server->get_ip() << ": Problem Instance Time request time="
	//							           << new_job->_job_desc->_search_time_step << ":" << new_job->_job_desc.get();
	//							#endif
	//							time_queue.pop();//remove job
	//							new_job->_solver = std::async(std::launch::async,
	//							                              [this,
	//							                               &server,
	//							                               new_job, solver{std::move(op)},
	//							                               time]()
	//							                              {
	//								                              new_job->_job_desc->set_computing();
	//								                              auto solution = solver();
	//								                              server->signal_available_load();
	//								                              new_job->_return_solution_callback(solution);
	//							                              });
	//						}
	//						else if(Server::Get_solver_return::NOT_ENOUGH_CORE == ret)
	//						{
	//							overwhelmed = true;
	//							server->wait_until_available_load();//blocking function, should be notified during deconstruction
	//						}
	//					}
	//					else
	//					{
	//						time_queue.pop();
	//					}
	//				}
	//				_timed_taskQ_handler_sp->unlock_time(time);
	//				time++;
	//			}
	}
	}
	
	//	void
	//	Server_handler::load_balancer(std::chrono::milliseconds period)
	//	{
	//		auto next_wake_up = std::chrono::steady_clock::now();
	//		while(!_done_solution_found)
	//		{
	//			std::this_thread::sleep_until(next_wake_up);
	//			next_wake_up += period;
	//
	//			_timed_taskQ_handler_sp->get_average_load()
	//			for(auto& s:_servers)
	//			{
	//				//Configure
	//
	//			}
	//		}
	//	}
	
	//	std::pair<bool,
	//	          std::function<Makespan_task_sp(void)>>
	//	Server_handler::wait_create_job_solver(Timed_task_req_sp job_desc)
	//	{
	//		//If desired time step is not exists in the records yet
	//		{
	//			std::shared_lock<std::shared_mutex> slock(_time_record_mtx);
	//			if(_time_to_server.size() <= job_desc->_search_time_step)
	//			{
	//				slock.unlock();
	//				std::unique_lock<std::shared_mutex> ulock(_time_record_mtx);
	//				while(_time_to_server.size() <= job_desc->_search_time_step)
	//				{
	//					_time_to_server.emplace_back(std::vector<Server_sp>());
	//					_time_to_smtx.emplace_back(std::make_unique<std::shared_mutex>());
	//					_time_to_cv.emplace_back(std::make_unique<std::condition_variable_any>());
	//				}
	//			}
	//		}
	//
	//		//Lock time step records
	//		std::shared_lock<std::shared_mutex> lock(* _time_to_smtx[job_desc->_search_time_step]);
	//		while(true)
	//		{
	//			if(job_desc->is_canceled() || _done_solution_found)
	//			{
	//				return std::make_pair(false,
	//				                      []() -> Makespan_task_sp
	//				                      {
	//					                      return Makespan_task_sp(nullptr);
	//				                      });
	//			}
	//
	//			std::function<Makespan_task_sp(void)> job_processor;
	//			Server_sp                        available_server;
	//			//sleep on in
	//			_time_to_cv[job_desc->_search_time_step]->wait(lock,
	//			                                        [&]()
	//			                                        {
	//				                                        return !_done_solution_found || !std::any_of(_time_to_server[job_desc->_search_time_step].begin(),
	//				                                                                      _time_to_server[job_desc->_search_time_step].end(),
	//				                                                                      [&](Server_sp s)
	//				                                                                      {
	//					                                                                      auto sp = s->get_and_reserve_solver(job_desc);
	//					                                                                      if(sp.first)
	//					                                                                      {
	//						                                                                      available_server = s;
	//						                                                                      job_processor    = sp.second;
	//						                                                                      return true;//Found
	//					                                                                      }
	//					                                                                      return false;//Keep iterating
	//
	//				                                                                      });// If any of them found get out of wait
	//			                                        });
	//			if(!_done_solution_found)
	//			{
	//
	//				return std::make_pair<bool,
	//				                      std::function<Makespan_task_sp(void)>>(true,
	//				                                                        [&, jp{std::move(job_processor)}, server(available_server)]() -> Makespan_task_sp
	//				                                                        {
	//					                                                        //Run the main operation
	//					                                                        auto r = jp();
	//
	//					                                                        //After main operation is_completed
	//					                                                        //Notify all the time_id's this machine can add_to_plan_and_generate_new_task
	//					                                                        auto m_time = server->can_process_makespan();
	//					                                                        std::for_each(m_time.begin(),
	//					                                                                      m_time.end(),
	//					                                                                      [&](uint32_t t)
	//					                                                                      {
	//						                                                                      _time_to_cv[t]->notify_all();
	//					                                                                      });
	//					                                                        return r;
	//				                                                        });
	//			}
	//			else
	//			{
	//				return std::make_pair(false,
	//				                      std::function<Makespan_task_sp(void)>());
	//			}
	//		}
	//	}
	
	//	void
	//	Server_handler::load_balancer()
	//	{
	//		//TODO implement strategy
	//		//Regularly watch  machine usage and modify machine server profile accordingly
	//		std::vector<uint32_t> prof(_time_span,
	//		                           1);
	//		for(auto& m:_servers)
	//		{
	//			//First remove all existing links
	//			for(auto& ml:_time_to_server)
	//			{
	//				std::remove_if(ml.begin(),
	//				               ml.end(),
	//				               [&](Server_sp const& mlm)
	//				               {return mlm == m;});
	//			}
	//			//Configure
	//			m->configure(prof);
	//
	//			//Create new links
	//			for(auto const& ts:m->can_process_makespan())
	//			{
	//				std::unique_lock<std::shared_mutex> lock(* _time_to_smtx[ts]);
	//				_time_to_server[ts].push_back(m);
	//				std::sort(_time_to_server[ts].begin(),
	//				          _time_to_server[ts].end());
	//				lock.unlock();
	//				_time_to_cv[ts]->notify_all();
	//			}
	//		}
	//		while(!_done_solution_found)
	//		{
	//			using std::chrono_literals::operator ""s;
	//			std::this_thread::sleep_for(1s);
	//		}
	//	}*/
}
