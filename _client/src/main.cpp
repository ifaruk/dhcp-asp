// Created by f on 11/10/17.
//

#include <fstream>
#include <iostream>
#include <sstream>
#include <locale>
#include <unordered_map>
#include <unordered_set>
#include <chrono>
#include <boost/program_options.hpp>

#include <logger.h>
#include <planner.h>
#include <server_handler.h>
#include <makespan_queue.h>

void
read_command_line(int argc,
                  char** argv,
                  int32_t& port_no,
                  std::string& server_ips,
                  std::string& domain_file,
                  std::string& goal_file,
                  std::string& initial_constraints,
                  std::string& domain_insight,
                  uint32_t& depth_limit,
                  uint64_t& time_limit_sec,
                  std::string& clingo_parameters,
                  uint32_t& verbosity)
{
	namespace bpo=boost::program_options;
	bpo::variables_map vm;
	{
		bpo::options_description            desc("Options");
		bpo::positional_options_description positional_options;
		
		
		//@formatter:off
		desc.add_options()("h","Print help messages")
			
											("port",bpo::value<int32_t>(&port_no),
											 "Activates Server Mode and requires a port number \"--port 10003\"")
			
											("servers",bpo::value<std::string>(&server_ips),
											 "Server IP:port pairs \' --servers \"localhost:12345,192.168.1.1:12345\" \'\n "
												 "Note that arguments must be given as string and separated by comma.\"")
			
											("df",bpo::value<std::string>(&domain_file),
											 "Domain File")
			
											("gf",bpo::value<std::string>(&goal_file),
											 "Goal File")
			
											("c",bpo::value<std::string>(&initial_constraints),
											 "Initial constraints\n "
												"Time steps are indicated with curly brace encapsulation \"{abc(x);def(y)}\". First time step is considered '0'.\n"
												"Each atom in a time step will be separated by semicolon (';').\n"
												"\"--c \"{}{at(1);move(2,loc(1))}{place(3)}\".")
			
											("dif",bpo::value<std::string>(&domain_insight),
											 "DHCP Domain Insight File")
			
											("dlimit",bpo::value<uint32_t>(&depth_limit),
											 "Depth Limit")
			
											("tlimit",bpo::value<uint64_t>(&time_limit_sec),
											 "Time Limit in seconds")
			
											("cp",bpo::value<std::string>(&clingo_parameters),
											 "Clingo parameters \"--cp \"--quite=1;--stats=4\".\n "
												 "Note that arguments must be separated by semicolon.\"")
									
											("verbose",bpo::value<uint32_t>(&verbosity),
											   "Verbosity of running program to log. Default is 1\n"
												 "0: No Log\n"
												 "1: Fatal\n"
												 "2: Warning\n"
												 "3: Info\n");
		//@formatter:on
		
		
		bpo::store(bpo::command_line_parser(argc,
		                                    const_cast<char const* const*>(argv)).options(desc).style(bpo::command_line_style::default_style | bpo::command_line_style::allow_long_disguise).run(),
		           vm);
		bpo::notify(vm);
		
		if(vm.count("port") == 0)
		{
			//Default value
			port_no = 10000;
		}
		
		if(vm.count("servers") == 0)
		{
			std::cerr << "At least a single server must be provided" << std::endl;
		}
		
		if(vm.count("df") == 0)
		{
			std::cerr << "'Domain File'(--df) are necessary" << std::endl;
		}
		
		if(vm.count("gf") == 0)
		{
			std::cerr << "'Goal File'(--gf) are necessary" << std::endl;
		}
		
		if(vm.count("c") == 0)
		{
			std::cerr << "'Initial Conditions'(--c) are necessary" << std::endl;
		}
		
		if(vm.count("dif") == 0)
		{
			std::cerr << "'Domain Insight File'(--dif) are necessary" << std::endl;
		}
		
		if(vm.count("dlimit") == 0)
		{
			depth_limit = UINT32_MAX;//todo make 0 and control it everywhere used
		}
		
		if(vm.count("tlimit") == 0)
		{
			time_limit_sec = 0;
		}
		
		if(vm.count("cp") == 0)
		{
			clingo_parameters = "";
		}
		
		if(vm.count("verbose") == 0)
		{
			verbosity = 1;
		}
		else
		{
			verbosity = std::min(3U,
			                     verbosity);
		}
	}
}

namespace DHCP::Client
{
	Problem_data_sp program_instance_data;
}

int
main(int argc,
     char** argv)
{
	int32_t     port_no;
	std::string server_ips;
	std::string domain_file;
	std::string goal_file;
	std::string initial_constraints_str;
	std::string domain_insight_file;
	uint32_t    time_step_limit;
	uint64_t    time_limit_sec;
	std::string clingo_parameter;
	uint32_t    verbosity;
	
	read_command_line(argc,
	                  argv,
	                  port_no,
	                  server_ips,
	                  domain_file,
	                  goal_file,
	                  initial_constraints_str,
	                  domain_insight_file,
	                  time_step_limit,
	                  time_limit_sec,
	                  clingo_parameter,
	                  verbosity);
	
	if(verbosity > 0)
	{
		initialize_logger("client");
	}
	
	using namespace DHCP::Client;
	using namespace DHCP;
	{
		//Parse servers
		Planner_sp planner;
		DHCP::Client::program_instance_data = std::make_shared<Problem_data>(std::vector<std::string>(),
		                                                                     domain_file,
		                                                                     goal_file,
		                                                                     domain_insight_file,
		                                                                     time_step_limit,
		                                                                     time_limit_sec,
		                                                                     verbosity);
		
		try
		{
			//			auto makespan_taskQ = std::make_shared<Makespan_taskQ>();
			auto makespan_taskQ = std::make_shared<Makespan_queue>();
			
			auto server_handler = std::make_shared<Server_handler>(parse_server_list_str(server_ips),
			                                                       makespan_taskQ);
			planner = std::make_shared<Planner>(makespan_taskQ);
			//Add initial state
			planner->new_task(parse_initial_constraints(initial_constraints_str),
			                  0,
			                  DHCP::Client::program_instance_data->_makespan_limit,
			                  0,//Main problem
			                  0);//Highest priority
			
			//Add landmarks
			//TODO add landmarks
			
			planner->start();
			planner->wait_for_completion(std::chrono::seconds(time_limit_sec));
			makespan_taskQ->invalidate();
			//server_handler will be killed here
			//makespan_taskQ will be killed here
		}
		catch(std::exception& e)
		{
			std::cout << e.what() << std::endl;
			planner->report_statistics();
			planner->report_graph();
			if(program_instance_data->_verbosity >= 1)
			{
				LOG(FATAL) << e.what();
			}
			throw e;
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
		planner->report_statistics();
		planner->report_graph();
	}
	
	//Print plan
	
	//Print visited states
	
	//Print statistics
	
	return 0;
}