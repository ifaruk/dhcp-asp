//
// Created by f on 13/02/18.
//

#ifndef DHCP_ASP_SOLVER_H_
#define DHCP_ASP_SOLVER_H_

#include <utils.h>
#include <task.h>
#include <server_stub.h>

namespace DHCP::Client
{
	class Solver
	{
	public:
		Solver(uint32_t solver_id,
		       uint32_t plan_time_span,
		       uint32_t constraint_time_span,
		       Server_stub_sp server_stub);
		
		std::tuple<bool,//satisfiable
		           Time_sequence_sp,//Fluent
		           Time_sequence_sp,//Act_actuation
		           Time_sequence_sp,//Act_sensing
		           Time_sequence_sp,//outcome
		           Time_sequence_sp,//additional
		           std::chrono::milliseconds,//clingo time
		           std::chrono::milliseconds>//total time
		solve(Time_sequence const& constraints);
		
		void
		cancel();
		
		inline uint32_t
		use_count() const
		{return _use_count;};
		
		uint32_t
		solver_id() const
		{return _solver_id;}
		
		uint32_t
		plan_makespan() const
		{return _problem_makespan;}
		
		uint32_t
		constraint_makespan() const
		{return _constraint_makespan;}
		
		inline void
		increment_use_count()
		{_use_count++;}
		
		inline bool
		try_reserve()
		{return !_reserved.test_and_set(std::memory_order_acquire);}
		
		inline void
		release()
		{_reserved.clear(std::memory_order_acquire);}
	
	private:
		Server_stub_sp   _server_stub;
		uint32_t         _use_count;
		uint32_t         _solver_id;
		uint32_t         _problem_makespan;
		uint32_t         _constraint_makespan;
		std::atomic_flag _reserved;
	};
	
	using Solver_sp = std::shared_ptr<Solver>;
}

#endif //DHCP_ASP_SOLVER_H
