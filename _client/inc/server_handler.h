//
// Created by f on 01/11/17.
//

#ifndef DHCP_ASP_SERVER_HANDLER_H
#define DHCP_ASP_SERVER_HANDLER_H

#include <functional>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>

#include <server.h>
#include <utils.h>
#include <thread_safe_queue.h>
#include <consumer_producer_sync.h>
#include <makespan_queue.h>

namespace DHCP::Client
{
	extern Problem_data_sp program_instance_data;
}

namespace DHCP::Client
{
	class Server_handler
	{
	public:
		Server_handler(std::vector<std::string> machine_ip,
		               Makespan_queue_sp makespan_taskQ);
		
		Server_handler(Server_handler const& that) = delete;
		
		virtual ~Server_handler();
		
		void
		server_feeder(Server_sp server);
		
		//		void
		//		load_balancer(std::chrono::milliseconds period);
	
	private:
		//Conf
		std::vector<std::string> _machine_ip;
		Makespan_queue_sp        _makespan_taskQ;
		
		std::mutex _cur_max_makespan_mtx;
		uint32_t   _cur_max_makespan;
		
		//State
		bool                     _done;
		//		std::thread              _load_balancer_th;
		//List of machines managed by Managers
		std::vector<Server_sp>   _servers;
		std::vector<std::thread> _server_feeders;
		std::vector<std::string> _dead_servers;
	};
	
	using Work_power_sp = std::shared_ptr<Server_handler>;
}
#endif //DHCP_ASP_SERVER_HANDLER_H
