//
// Created by f on 13/10/17.
//

#ifndef DHCP_ASP_SERVER_H
#define DHCP_ASP_SERVER_H

#include <unordered_map>
#include <memory>
#include <condition_variable>
#include <atomic>
#include <future>
#include <deque>
#include <numeric>
#include <set>
#include <chrono>

#include <solver.h>
#include <server_stub.h>

#include <logger.h>
#include <utils.h>
#include <task.h>

namespace DHCP::Client
{
	class Server
	{
	public:
		Server(std::string domain,
		       std::string goal,
		       std::string dif,
		       uint32_t id,
		       Server_stub_sp server_stub);
		
		~Server();
		
		Server(const Server& that) = delete;
		
		std::shared_ptr<Solver>
		add_and_reserve_solver(uint32_t makespan);
		
		bool
		blocking_reserve_workload();
		
		void
		release_workload();
		
		enum class Get_solver_return
		{
			OK, NOT_ENOUGH_SOLVER, NOT_ENOUGH_CORE
		};
		
		//Remote Server Features
		uint32_t
		get_no_processors()
		{return _no_processors;}
		
		uint32_t
		get_no_cores_per_processor()
		{return _no_cores_per_processor;}
		
		uint32_t
		get_no_threads_per_core()
		{return _no_threads_per_core;}
		
		uint32_t
		get_cpu_MHz()
		{return _cpu_MHz;}
		
		uint32_t
		get_memory_MB()
		{return _memory_MB;}
		
		uint32_t
		get_max_workload()
		{return _max_workload;}
		
		
		//		inline int
		//		available_workload()
		//		{return _max_workload - _cur_workload;}
		//
		//		inline double
		//		average_available_load()
		//		{return _cur_max_workload_capacity - _avg_load_one_min / 100.0;}
		//
		//		void
		//		wait_until_available_load()
		//		{
		//			std::mutex                   temp;
		//			std::unique_lock<std::mutex> l(temp);
		//			_wait_for_ready_cv.wait(l,
		//			                        [this]()
		//			                        {return average_available_load() > 0;});
		//		}
		//
		//		void
		//		signal_available_load()
		//		{
		//			_wait_for_ready_cv.notify_all();
		//		}
		
		/**@brief Defines an order w.r.t Machine performance,
		 *
		 * @param other
		 * @return
		 */
		bool
		operator<(Server const& other);
	
	private:
		//Configuration
		uint32_t const _id;
		
//		static constexpr std::chrono::milliseconds _max_delay = 1s;
		
		std::string const _domain;
		std::string const _goal;
		std::string const _dif;
		
		Server_stub_sp _server_stub;
		
		//State
		bool _done;
		
		std::mutex              _workload_mtx;
		std::condition_variable _workload_cv;
		uint32_t                _max_workload;
		uint32_t                _available_workload;
		
		//		uint64_t _abs_max_workload_capacity;
		//		double   _cur_max_workload_capacity;
		//
		//		uint64_t             _cur_workload;
		//		std::deque<uint64_t> _last_minute_workload;
		//
		//		std::thread _machine_listener_th;
		
		
		
		//Remote Server Features
		uint32_t _no_processors;
		uint32_t _no_cores_per_processor;
		uint32_t _no_threads_per_core;
		uint32_t _cpu_MHz;
		uint32_t _memory_MB;
		
		//		Remote Server State
		std::chrono::duration<long,
		                      std::ratio<1,
		                                 1000>> _com_delay;
		//		uint32_t                                _avg_load_one_min;
		//		uint32_t                                _avg_load_five_min;
		//		uint32_t                                _avg_load_fifteen_min;
		//		uint32_t                                _available_memory_MB;
		
		//State
		std::mutex                          _solver_record_mtx;
		std::set<uint32_t>                  _can_process_time;
		std::vector<uint32_t>               _solver_counts;
		std::vector<std::vector<Solver_sp>> _solvers;
	
	private:
		void
		server_listener();
	};
	
	using Server_sp = std::shared_ptr<Server>;
}

#endif //DHCP_ASP_SERVER_H
