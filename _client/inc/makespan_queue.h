//
// Created by f on 06/02/18.
//

#ifndef DHCP_ASP_MAKESPAN_QUEUE_H
#define DHCP_ASP_MAKESPAN_QUEUE_H

#include <utils.h>
#include <task.h>

namespace DHCP::Client
{
	
	class Makespan_queue
	{
	public:
		Makespan_queue()
			: _is_valid(true)
		{}
		
		void
		push(Makespan_task_sp task)
		{
			auto                         makespan = task->get_makespan();
			std::unique_lock<std::mutex> lock(_makespan_queues_mtx);
			while(makespan >= _makespan_queues.size())
			{
				_makespan_queues.emplace_back(std::make_shared<Makespan_taskQ>());
			}
			auto q = _makespan_queues[makespan];
			lock.unlock();
			q->push(task);
		}
		
		bool
		wait_pop(uint32_t makespan,
		         Makespan_task_sp& task)
		{
			std::unique_lock<std::mutex> lock(_makespan_queues_mtx);
			while(makespan >= _makespan_queues.size())
			{
				_makespan_queues.emplace_back(std::make_shared<Makespan_taskQ>());
			}
			auto q = _makespan_queues[makespan];
			lock.unlock();
			return q->wait_pop(task);
		}
		
		bool
		try_pop(uint32_t makespan,
		        Makespan_task_sp& task)
		{
			std::unique_lock<std::mutex> lock(_makespan_queues_mtx);
			while(makespan >= _makespan_queues.size())
			{
				_makespan_queues.emplace_back(std::make_shared<Makespan_taskQ>());
			}
			auto q = _makespan_queues[makespan];
			lock.unlock();
			return q->try_pop(task);
		}
		
		std::vector<uint32_t>
		get_sizes()
		{
			std::vector<uint32_t>        sizes;
			std::unique_lock<std::mutex> lock(_makespan_queues_mtx);
			for(auto& q : _makespan_queues)
			{
				sizes.emplace_back(q->size());
			}
			return sizes;
		}
		
		bool
		invalidate()
		{
			std::unique_lock<std::mutex> lock(_makespan_queues_mtx);
			for(auto& q:_makespan_queues)
			{
				q->invalidate();
			}
			_is_valid = false;
		}
		
		bool
		is_valid()
		{return _is_valid;}
	
	private:
		
		using Makespan_taskQ = ::DHCP::Threadsafe_priority_queue<Makespan_task_sp,
		                                                         std::vector<Makespan_task_sp>,
		                                                         Makespan_task_sp_comparator>;
		using Makespan_taskQ_sp = std::shared_ptr<Makespan_taskQ>;
		
		std::mutex                     _makespan_queues_mtx;
		std::vector<Makespan_taskQ_sp> _makespan_queues;
		
		bool _is_valid;
	};
	
	using Makespan_queue_sp = std::shared_ptr<Makespan_queue>;
}

#endif //DHCP_ASP_MAKESPAN_QUEUE_H
