//
// Created by f on 13/02/18.
//

#ifndef DHCP_ASP_SERVER_STUB_H
#define DHCP_ASP_SERVER_STUB_H

#include <grpc++/grpc++.h>
#include <client_server.grpc.pb.h>

#include <utils.h>

namespace DHCP::Client
{
	class Server_stub
	{
	public:
		Server_stub(std::string ip);
		
		bool
		is_alive();
		
		/***************************************************************
		 *
		 * @return
		 */
		std::pair<bool,
		          std::chrono::duration<long,
		                                std::ratio<1,
		                                           1000>>>
		rpc_ping();
		
		/***************************************************************
		 *
		 * @return
		 */
		std::tuple<bool, //rpc_success
		           uint32_t,//no_processors
		           uint32_t,//no_cores_per_processor
		           uint32_t,//no_threads_per_core
		           uint32_t,//cpu_MHz
		           uint32_t>//memory_MB
		rpc_get_server_features();
		
		/***************************************************************
		 * @brief Retrieves the state of the remote machine
		 *
		 * @returns a tuple (rpc_success,avg_load,avg_no_free_cores,
		 * free_memory_MB)
		 */
		std::tuple<bool,//rpc_success
		           uint32_t,//avg_load_one_min
		           uint32_t,//avg_load_five_min
		           uint32_t,//avg_load_fifteen_min
		           uint32_t>//available_memory_mb
		rpc_get_server_state();
		
		/***************************************************************
		 *
		 * @param clingo_arguments
		 * @param domain
		 * @param goal
		 * @param dif
		 * @param parallel_capacity
		 * @return
		 */
		bool
		rpc_configure_server(std::vector<std::string> clingo_arguments,
		                     std::string domain,
		                     std::string goal,
		                     std::string dif,
		                     uint32_t parallel_capacity);
		
		/***************************************************************
		 *
		 * @return
		 */
		bool
		rpc_reset_server();
		
		/***************************************************************
		 * @brief Create new time solver
		 *
		 * @param plan_makespan
		 * @param constraint_makespan
		 * @return solver_id
		 */
		int64_t
		rpc_new_makespan_solver(uint32_t plan_makespan,
		                        uint32_t constraint_makespan);
		
		/***************************************************************
		 * @brief Kill time solver
		 *
		 * @param solver_id
		 * @return
		 */
		bool
		rpc_kill_solver(uint32_t solver_id);
		
		std::tuple<bool,//operation successful
		           bool,//satisfiable
		           Time_sequence_sp,//Fluent
		           Time_sequence_sp,//Act_actuation
		           Time_sequence_sp,//Act_sensing
		           Time_sequence_sp,//outcome
		           Time_sequence_sp,//additional
		           uint32_t,//clingo time
		           uint32_t>//total time
		rpc_solve(uint32_t solver_id,
		          Time_sequence const& constraints);
		
		/**Interrupt current running (or next at the beginning) solving
		 *
		 * @param solver_id
		 * @return
		 */
		bool
		rpc_cancel(uint32_t solver_id);
	
	private:
		//conf
		std::string const                       _ip;
		//gRPC
		std::shared_ptr<grpc::Channel>          _channel;
		std::unique_ptr<Scontrol::Server::Stub> _stub;
		//state
		bool                                    _alive;
	};
	
	using Server_stub_sp = std::shared_ptr<Server_stub>;
}
#endif //DHCP_ASP_SERVER_STUB_H
