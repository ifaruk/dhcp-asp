//
// Created by f on 18/01/18.
//

#ifndef DHCP_ASP_TASK_H
#define DHCP_ASP_TASK_H

#include <memory>
#include <map>
#include <sstream>
#include <numeric>

#include <utils.h>

#include <thread_safe_queue.h>
//#include <makespan_queue.h>

namespace DHCP::Client
{
	extern Problem_data_sp program_instance_data;
}

namespace DHCP::Client
{
	
	static const uint32_t NO_MAKE_SPAN = UINT32_MAX;
	using Time_point = std::chrono::time_point<std::chrono::steady_clock>;
	
	//=================================================================================
	class Solution;
	
	using Solution_sp = std::shared_ptr<Solution>;
	
	class Makespan_record;
	
	using Makespan_record_sp = std::shared_ptr<Makespan_record>;
	
	class Task_record;
	
	using Task_record_sp = std::shared_ptr<Task_record>;
	
	class Task;
	
	using Task_sp= std::shared_ptr<Task>;
	using Task_wp= std::weak_ptr<Task>;
	
	class Makespan_task;
	
	using Makespan_task_sp = std::shared_ptr<Makespan_task>;
	using Makespan_task_wp = std::weak_ptr<Makespan_task>;
	
	class Task_handler;
	
	using Task_handler_sp= std::shared_ptr<Task_handler>;
	using Task_handler_wp= std::weak_ptr<Task_handler>;
	
	struct Task_sp_comparator
	{
		/**Non landmarks and smaller priorities are first*/
		bool
		operator()(const Task_sp& lhs,
		           const Task_sp& rhs) const;
	};
	
	struct Task_handler_sp_comparator
	{
		/**Non landmarks and smaller priorities are first*/
		bool
		operator()(const Task_handler_sp& lhs,
		           const Task_handler_sp& rhs) const;
	};
	
	struct Makespan_task_sp_comparator
	{
		/**Non landmarks and smaller priorities are first*/
		bool
		operator()(const Makespan_task_sp& lhs,
		           const Makespan_task_sp& rhs) const;
	};
	
	//	using Makespan_taskQ = ::DHCP::Threadsafe_priority_queue<Makespan_task_sp,
	//	                                                         std::vector<Makespan_task_sp>,
	//	                                                         Makespan_task_sp_comparator>;
	//	using Makespan_taskQ_sp = std::shared_ptr<Makespan_taskQ>;
	
	class Solution
	{
	public:
		Solution()
			: _fluent(nullptr),
			  _act_actuation(nullptr),
			  _act_sensing(nullptr),
			  _outcome(nullptr),
			  _additional(nullptr)
		{}
		
		Solution(const Time_sequence_sp& _fluent,
		         const Time_sequence_sp& _act_actuation,
		         const Time_sequence_sp& _act_sensing,
		         const Time_sequence_sp& _outcome,
		         const Time_sequence_sp& _additional)
			: _fluent(_fluent),
			  _act_actuation(_act_actuation),
			  _act_sensing(_act_sensing),
			  _outcome(_outcome),
			  _additional(_additional)
		{}
		
		std::string
		to_string()
		{
			std::stringstream all_solution;
			for(auto const& tseq:{std::make_pair(_fluent,
			                                     "Fluents"),
			                      std::make_pair(_act_actuation,
			                                     "Actuation Actions"),
			                      std::make_pair(_act_sensing,
			                                     "Sensing Actions"),
			                      std::make_pair(_outcome,
			                                     "Outcomes"),
			                      std::make_pair(_additional,
			                                     "Additional")})
			{
				all_solution << "----------------------------------------------------" << std::endl;
				all_solution << tseq.second << std::endl;
				all_solution << ::DHCP::to_string(* tseq.first) << std::endl;
			}
			return all_solution.str();
		}
		
		Time_sequence_sp _fluent;
		Time_sequence_sp _act_actuation;
		Time_sequence_sp _act_sensing;
		Time_sequence_sp _outcome;
		Time_sequence_sp _additional;
	};
	
	class Makespan_record
	{
	public:
		
		Makespan_record(bool redundant,
		                bool satisfiable,
		                const std::chrono::milliseconds& queue_duration,
		                const std::chrono::milliseconds& server_duration,
		                const std::chrono::milliseconds& cancelled_server_duration,
		                const std::chrono::milliseconds& in_server_clingo_duration,
		                const std::chrono::milliseconds& in_server_process_duration);
		
		bool                      _redundant;
		bool                      _satisfiable;
		std::chrono::milliseconds _queue_duration;
		std::chrono::milliseconds _server_duration;
		std::chrono::milliseconds _canceled_server_duration;
		std::chrono::milliseconds _in_server_clingo_duration;
		std::chrono::milliseconds _in_server_process_duration;
	};
	
	class Task_record
	{
	public:
		std::chrono::time_point<std::chrono::steady_clock> _task_start;
		std::chrono::milliseconds                          _task_duration;
	};
}

namespace DHCP::Client
{
	/**
	 * Task record
	 * @warning None of the members are thread safe
	 */
	class Task: public std::enable_shared_from_this<Task>
	{
	public:
		Task(Time_sequence_sp constraints,
		     uint32_t makespan_from,
		     uint32_t makespan_to,
		     uint32_t level,
		     uint32_t priority,
		     Task_sp parent_task);
		
		Task(const Task& that) = delete;
		
		uint32_t
		id() const;
		
		Time_sequence_sp
		get_constraints() const;
		
		uint32_t
		get_makespan_from() const;
		
		uint32_t
		get_makespan_to() const;
		
		uint32_t
		get_level() const;
		
		uint32_t
		get_priority() const;
		
		bool
		one_solution_found();
		
		uint32_t
		get_optimum_makespan();
		
		Solution_sp
		get_optimum_solution();
		
		Task_wp
		get_parent();
		
		/**
		 * @returns True if optimum is found
		 */
		void
		set_makespan_record(uint32_t makespan,
		                    Solution_sp sol,
		                    Makespan_record_sp stat);
		
		void
		set_task_record(Task_record_sp tr);
		
		Task_record const &
		get_task_record() const;
		
		/**@brief true if there is a record of 'makespan'
		 */
		bool
		completed(uint32_t makespan);
		
		/**@brief true if there is a record of 'makespan' and the 'solution'
		 * is satisfiable
		 */
		bool
		completed_satisfiable(uint32_t makespan);
		
		/**@brief true if there is a record of 'makespan' and the 'solution'
		 * is unsatisfiable
		 */
		bool
		completed_unsatisfiable(uint32_t makespan);
		
		//Total Numbers
		uint32_t
		get_no_makespans(bool used,
		                 bool redundant);
		
		//Total Durations
		std::chrono::milliseconds
		get_total_queue_duration(bool used,
		                         bool redundant);
		
		std::chrono::milliseconds
		get_total_server_duration(bool used,
		                          bool redundant);
		
		std::chrono::milliseconds
		get_total_canceled_duration(bool used,
		                            bool redundant);
		
		std::chrono::milliseconds
		get_total_in_server_clingo_duration(bool used,
		                                    bool redundant);
		
		std::chrono::milliseconds
		get_total_in_server_process_duration(bool used,
		                                     bool redundant);
		
		Makespan_record_sp
		get_makespan_record(uint32_t makespan);
		
		bool
		operator>(Task const& other) const;
	
	private:
		static std::atomic<uint64_t> _next_id;
		uint64_t                     _id;
		
		Time_sequence_sp _constraints;
		uint32_t const   _makespan_from;
		uint32_t const   _makespan_to;
		uint32_t const   _level; //level 0 has highest priority on plan
		uint32_t const   _priority;//Priority in same level tasks, 0 is highest
		
		std::vector<Makespan_record_sp> _all_makespan_records;
		uint32_t                        _optimum_makespan;
		Solution_sp                     _optimum_solution;
		Task_record_sp                  _task_record;
		
		Task_wp _parent_task;
		
		template<typename T>
		T
		sum_makespan(bool used,
		             bool redundant,
		             T const& zero_val,
		             std::function<T(Makespan_record_sp m)> f)
		{
			std::accumulate(_all_makespan_records.begin(),
			                _all_makespan_records.end(),
			                zero_val,
			                [used,
			                 redundant,
			                 &zero_val,
			                 &f](T sum,
			                     Makespan_record_sp m) -> T
			                {return sum + ( m && ((m->_redundant && redundant) || (!m->_redundant && used)) ? f(m) : zero_val);});
		}
	};
	
	class Makespan_queue;
	
	using Makespan_queue_sp = std::shared_ptr<Makespan_queue>;
	
	class Task_handler: public std::enable_shared_from_this<Task_handler>
	{
	public:
		Task_handler(Task_sp task,
		             Makespan_queue_sp makespan_taskQ,
		             std::function<void(Task_sp)> completion_callback,
		             std::string single_shot_command);
		
		void
		init();
		
		uint32_t
		id();
		
		uint32_t
		level();
		
		void
		makespan_return(uint32_t makespan,
		                Solution_sp sol,
		                Makespan_record_sp rec);
		
		void
		cancel();
	
	private:
		friend class Makespan_task;
		
		Task_sp                      _task;
		Makespan_queue_sp            _makespan_taskQ;
		std::function<void(Task_sp)> _completion_callback;
		std::string                  _single_shot_command;
		
		uint32_t          _next_makespan;
		std::atomic<bool> _done_solution_found;
		std::mutex        _return_mtx;
		
		std::mutex                    _running_makespan_tasks_map_mtx;
		std::map<uint32_t,
		         Makespan_task_sp,
		         std::less<uint32_t>> _running_makespan_tasks_map;
		
		Task_record_sp _task_record;
		
		Makespan_task_sp
		new_makespan_task(uint32_t makespan);
		
		/**
		 *
		 * @param amount
		 * @return True if at leas one new makespan is added
		 */
		bool
		generate_new_makespan(uint32_t amount);
	};
	
	class Makespan_task: public std::enable_shared_from_this<Makespan_task>
	{
	
	private://Declerations
		enum class Makespan_task_state
		{
			NOT_STARTED,  //Not queued yet
			IN_QUEUE, //In queue
			COMPUTING, //Out of queue Sent to server
			COMPLETED, //Received solution from server
			CANCELED,  //Terminated during COMPUTING phase
			REDUNDANT //Terminated before COMPUTING phase
		};
	
	public:
		
		Makespan_task(uint32_t task_id,
		              Time_sequence_sp constraints,
		              uint32_t makespan,
		              uint32_t level,
		              uint32_t priority,
		              std::function<void(Solution_sp sol,
		                                 Makespan_record_sp rec)> callback,
		              std::string single_shot_command);
		
		Makespan_task(const Makespan_task& that) = delete;
		
		uint32_t
		task_id();
		
		Time_sequence const&
		get_constraints();
		
		uint32_t
		get_makespan();
		
		uint32_t
		get_level();
		
		uint32_t
		get_priority();
		
		std::string
		get_single_shot_command();
		
		//		bool
		//		is_not_started();
		//
		//		bool
		//		is_in_queue();
		//
		//		bool
		//		is_computing();
		//
		//		bool
		//		is_completed();
		//
		//		bool
		//		is_canceled();
		//
		//		bool
		//		is_redundant();
		
		/** Called by Task Handler when placed in a queue*/
		bool
		queued(std::function<void()> op);
		
		/** Called by computing thread when a solver is about to be reserved for this makespan task*/
		bool
		finding_solver();
		
		/** Called by computing thread when task is sent to the server*/
		bool
		start(std::function<void()> op,
		      std::function<void()> terminate_computation);
		
		/** Called by computing thread when solution is received from the server*/
		bool
		solution(bool satisfiable,
		         Time_sequence_sp fluent,
		         Time_sequence_sp act_actuation,
		         Time_sequence_sp act_sensing,
		         Time_sequence_sp outcome,
		         Time_sequence_sp additional,
		         std::chrono::milliseconds clingo_duration,
		         std::chrono::milliseconds server_duration);
		
		/** Called by Task Handler when canceled*/
		bool
		cancel();
		
		bool
		operator>(Makespan_task const& other) const;
	
	private://Fields
		//Input
		
		uint32_t                                    _task_id;
		Time_sequence_sp                            _constraints;
		uint32_t                                    _makespan;
		uint32_t                                    _level;
		uint32_t                                    _priority;
		std::function<void(Solution_sp sol,
		                   Makespan_record_sp rec)> _solution_callback;
		std::string                                 _single_shot_command;
		
		std::function<void()> _terminate_computation;
		
		//Conf
		Makespan_task_state _state;
		std::mutex          _state_spl;
		
		/**When the task is canceled*/
		Time_point                _cancel_start;//Time point placed in queue
		/**Time spent in queue*/
		std::chrono::milliseconds _cancelled_duration;//Time spend in queue
		
		/**When the task sent into queue*/
		Time_point                _queue_start;//Time point placed in queue
		/**Time spent in queue*/
		std::chrono::milliseconds _queue_duration;//Time spend in queue
		
		/**When the task sent to server*/
		Time_point                _server_start;
		/**Time between sending to server and receiving the answer*/
		std::chrono::milliseconds _server_duration;
	};
}

#endif //DHCP_ASP_TASK_H
