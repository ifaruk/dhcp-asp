//
// Created by f on 15/10/17.
//

#ifndef DHCP_ASP_PLANNER_H
#define DHCP_ASP_PLANNER_H

#include <string>
#include <vector>
#include <array>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <functional>

#include <logger.h>
#include <utils.h>
#include <hash_helper.h>
#include <thread_safe_queue.h>
#include <makespan_queue.h>
#include <task.h>

namespace DHCP::Client
{
	extern Problem_data_sp program_instance_data;
}

namespace DHCP::Client
{
	class Step;
	
	using Step_sp = std::shared_ptr<Step>;
	using Step_wp = std::weak_ptr<Step>;
	
	class Step
	{
	public:
		enum class Step_type
		{
			ALIVE, DUPLICATE
		};
		enum class State_type
		{
			GOAL = 0, INTERMEDIATE, DEAD_END
		};
		
		enum class Action_type
		{
			ACTUATION, SENSING
		};
	
	public:
		Step(State_type s_type,
		     Atom_set const& s_fluents,
		     Atom_set const& s_class,
		     Action_type a_type,
		     Atom_set const& actions,
		     Atom_set const& possible_outcomes = Atom_set(),
		     Atom_set const& additional = Atom_set(),
		     uint32_t level = UINT32_MAX,
		     uint32_t min_steps_to_goal = UINT32_MAX);
		
		inline Atom_set const&
		fluents()
		{return _s_fluents;}
		
		inline Action_type
		action_type() const
		{return _a_type;}
		
		inline Atom_set const&
		actions()
		{return _actions;}
		
		inline std::map<Atom,
		                Step_wp>
		get_outcome()
		{return _outcomes;}
		
		bool
		set_outcome(Atom outcome,
		            Step_sp step);
		
		inline std::map<Atom,
		                Step_wp> const&
		outcome()
		{return _outcomes;}
		
		inline void
		set_min_step_distance_to_goal(uint32_t dist)
		{
			_min_no_steps_to_goal = std::min(dist,
			                                 _min_no_steps_to_goal);
		}
		
		inline uint32_t
		min_step_distance_to_goal()
		{return _min_no_steps_to_goal;}
		
		//----------------------
		State_type
		state_type() const
		{return _s_type;}
		
		Step_type
		step_type() const
		{return _step_type;}
		
		Step_sp
		duplicate_of() const
		{return _duplicate_of;};
		
		void
		set_duplicate_of(Step_sp d);
		
		void
		set_level(uint32_t level)
		{
			_min_level = std::min(level,
			                      _min_level);
		}
		
		uint32_t
		get_level()
		{return _min_level;}
	
	public:
		
		std::string
		to_string();
		
		inline std::string
		to_string_actions()
		{return _actions_str;}
		
		size_t
		hash() const;
		
		bool
		operator==(Step const& other) const;
	
	private://Configurations
		State_type _s_type;
		Atom_set   _s_fluents;
		Atom_set   _s_class;
		
		Action_type       _a_type;
		Atom_set          _actions;
		std::map<Atom,
		         Step_wp> _outcomes;
		
		Atom_set _additional;
		
		uint32_t _min_no_steps_to_goal;
	
	private://State
		Step_type _step_type;
		Step_sp   _duplicate_of;
		uint32_t  _min_level;
		
		std::string _fluents_str;
		std::string _class_str;
		std::string _actions_str;
		std::string _outcomes_str;
		std::string _additional_str;
	};
}

namespace DHCP::Client
{
	class Planner: public std::enable_shared_from_this<Planner>
	{
	public:
		/**
		 * @warning Predicate Time and Goal are reserved keywords
		 *
		 * @param fluents
		 * @param sensing_act
		 * @param actuation_act
		 */
		Planner(Makespan_queue_sp makespan_taskQ);
		
		~Planner();
		
		void
		new_task(Time_sequence_sp state_constraints,
		         uint32_t makespan_from,
		         uint32_t makespan_to,
		         uint32_t level,
		         uint32_t priority);
		
		void
		start();
		
		void
		wait_for_completion(std::chrono::seconds timeout_sec);
		
		void
		report_graph();
		
		void
		report_statistics();
	
	private:
		//Configuration
		Makespan_queue_sp _makespan_taskQ;
		std::thread       _makespan_sampler;
		
		//State
		std::vector<Task_sp>     _initial_tasks;
		std::atomic<uint32_t>    _existing_level_zero_tasks;
		bool                     _done;
		std::condition_variable  _done_cv;
		/**Currently executing task_handlers*/
		std::map<uint32_t,
		         Task_handler_sp,
		         std::greater<>> _task_handler_map;
		
		/**Most recently computed tasks*/
		Threadsafe_priority_queue<Task_sp,
		                          std::vector<Task_sp>,
		                          Task_sp_comparator> _recently_computed_tasks;
		
		Predicate_set const _goal;
		Predicate_set const _waypoint;
		
		std::thread _task_processor_th;
	
	private:
		
		void
		task_completion(Task_sp t);
		
		Step_sp
		form_step(Time_step const& fluent,
		          Time_step const& state_class,
		          Time_step const& act_actuation,
		          Time_step const& act_sensing,
		          Time_step const& additional,
		          uint32_t const& level,
		          uint32_t distance_to_goal);
		
		/**@brief process new computed branch and integrate into the Plan
		 *
		 * @param branch
		 *
		 * @warning not thread safe
		 */
		std::vector<Task_sp>
		add_to_plan_and_generate_new_task(Task_sp task);
		
		void
		task_to_handler(std::vector<Task_sp> tasks);
		
		/**@brief Processes each computed task and determines completion conditions
		 *
		 */
		void
		task_processor();
		
		//---------------------------------------------------------------
		//---------------------------------------------------------------
		//Plan data
	private:
		/** Container for tasks that are processed and included to plan
		 * and new tasks are generated from them
		 */
		std::vector<Task_sp> _completed_task_list;
		
		/**Container for all visited steps*/
		std::unordered_map<Hashed_sp<Step>,
		                   Step_sp,
		                   Hashed_sp_operators<Step>> _visited_state_steps;
		
		/**Container for all hanging roots besides problem initial "_initial"*/
		std::set<Step_sp> _hanging_roots;
		
		/**Problem initial step*/
		Step_sp _initial;
	
	private:
		std::chrono::time_point<std::chrono::steady_clock> _stat_total_planning_start;
		std::chrono::milliseconds                          _stat_total_planning_duration;
		
		Task_sp _last_task;
	};
	
	using Planner_sp = std::shared_ptr<Planner>;
}
#endif //DHCP_ASP_PLANNER_H
