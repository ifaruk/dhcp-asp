//
// Created by f on 18/10/17.
//

#ifndef DHCP_ASP_HASH_HELPER_H
#define DHCP_ASP_HASH_HELPER_H

#include <memory>

namespace DHCP
{
	template<class T>
	class Hashed_operator;
	
	template<class T>
	struct Hashed_sp
	{
		explicit Hashed_sp(std::shared_ptr<T> t)
			: _t(t),
			  _hash_code( _t->hash())
		{
		}
		
		auto
		get_hash() const
		{
			return _hash_code;
		}
		
		auto
		operator==(Hashed_sp<T> const& other) const
		{
			return * _t == * other._t;
		}
	
	private:
		std::shared_ptr<T> _t;
		std::size_t        _hash_code;
	};
	
	template<class T>
	struct Hashed_sp_operators
	{
		auto
		operator()(Hashed_sp<T> const& t) const
		{
			return t.get_hash();
		}
	};
}
#endif //DHCP_ASP_HASH_HELPER_H
