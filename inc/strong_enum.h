//
// Created by f on 15/10/17.
//

#ifndef DHCP_ASP_STRONG_ENUM_H
#define DHCP_ASP_STRONG_ENUM_H

#include <type_traits>
#include <tuple>

template<typename E>
constexpr auto
to_int(E e) noexcept
{
	return static_cast<std::underlying_type_t<E>>(e);
}

#endif //DHCP_ASP_STRONG_ENUM_H
