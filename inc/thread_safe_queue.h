//
// Created by f on 14/10/17.
//

#ifndef DHCP_ASP_THREAD_SAFE_QUEUE_H
#define DHCP_ASP_THREAD_SAFE_QUEUE_H

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <utility>
#include <algorithm>

namespace DHCP
{
	template<typename T>
	class Threadsafe_queue
	{
	public:
		/**
		 * Destructor.
		 */
		~Threadsafe_queue(void)
		{
			invalidate();
		}
		
		/**
		 * Attempt to get the first value in the queue.
		 * Returns true if a value was successfully written to the out parameter, false otherwise.
		 */
		bool
		try_pop(T& out)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			if(m_queue.empty() || !m_valid)
			{
				return false;
			}
			out = std::move(m_queue.front());
			m_queue.pop();
			return true;
		}
		
		/**
		 * Get the first value in the queue.
		 * Will block until a value is available unless clear is called or the instance is destructed.
		 * Returns true if a value was successfully written to the out parameter, false otherwise.
		 */
		bool
		wait_pop(T& out)
		{
			std::unique_lock<std::mutex> lock{m_mutex};
			m_condition.wait(lock,
			                 [this]()
			                 {
				                 return !m_queue.empty() || !m_valid;
			                 });
			/*
			 * Using the condition in the predicate ensures that spurious wakeups with a valid
			 * but empty queue will not proceed, so only need to check for validity before proceeding.
			 */
			if(!m_valid)
			{
				return false;
			}
			out = std::move(m_queue.front());
			m_queue.pop();
			return true;
		}
		
		/**
		 * Push a new value onto the queue.
		 */
		void
		push(T value)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			m_queue.push(std::move(value));
			m_condition.notify_one();
		}
		
		/**
		 * Check whether or not the queue is empty.
		 */
		bool
		empty(void) const
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			return m_queue.empty();
		}
		
		/**
		 * Clear all items from the queue.
		 */
		void
		clear(void)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			while(!m_queue.empty())
			{
				m_queue.pop();
			}
			m_condition.notify_all();
		}
		
		/**
		 * Invalidate the queue.
		 * Used to ensure no conditions are being waited on in waitPop when
		 * a thread or the application is trying to exit.
		 * The queue is invalid after calling this method and it is an error
		 * to continue using a queue after this method has been called.
		 */
		void
		invalidate(void)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			m_valid = false;
			m_condition.notify_all();
		}
		
		/**
		 * Returns whether or not this queue is valid.
		 */
		bool
		isValid(void) const
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			return m_valid;
		}
	
	private:
		std::atomic_bool        m_valid{true};
		mutable std::mutex      m_mutex;
		std::queue<T>           m_queue;
		std::condition_variable m_condition;
	};
	
	template<typename _T,
	         typename _Sequence = std::vector<_T>,
	         typename _Compare  = std::less<typename _Sequence::value_type> >
	class Threadsafe_priority_queue
	{
	public:
		/**
		 * Destructor.
		 */
		~Threadsafe_priority_queue(void)
		{
			invalidate();
		}
		
		/**
		 * Attempt to get the first value in the queue.
		 * Returns true if a value was successfully written to the out parameter, false otherwise.
		 */
		bool
		try_pop(_T& out)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			if(m_queue.empty() || !m_valid)
			{
				return false;
			}
			out = std::move(m_queue.top());
			m_queue.pop();
			return true;
		}
		
		/**
		 * Get the first value in the queue.
		 * Will block until a value is available unless clear is called or the instance is destructed.
		 * Returns true if a value was successfully written to the out parameter, false otherwise.
		 */
		bool
		wait_pop(_T& out)
		{
			std::unique_lock<std::mutex> lock{m_mutex};
			m_condition.wait(lock,
			                 [this]()
			                 {
				                 return !m_queue.empty() || !m_valid;
			                 });
			/*
			 * Using the condition in the predicate ensures that spurious wakeups with a valid
			 * but empty queue will not proceed, so only need to check for validity before proceeding.
			 */
			if(!m_valid)
			{
				return false;
			}
			out = std::move(m_queue.top());
			m_queue.pop();
			return true;
		}
		
		/**
		 * Push a new value onto the queue.
		 */
		void
		push(_T value)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			m_queue.push(std::move(value));
			m_condition.notify_one();
		}
		
		/**
		 * Push a new value onto the queue.
		 */
		void
		push(std::vector<_T> value_vec)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			for(auto&& v:value_vec)
			{
				m_queue.push(v);
				m_condition.notify_one();
			}
		}
		
		/**
		 * Check whether or not the queue is empty.
		 */
		bool
		empty(void) const
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			return m_queue.empty();
		}
		
		uint32_t
		size(void) const
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			return m_queue.size();
		}
		
		/**
		 * Clear all items from the queue.
		 */
		void
		clear(void)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			while(!m_queue.empty())
			{
				m_queue.pop();
			}
			m_condition.notify_all();
		}
		
		/**
		 * Invalidate the queue.
		 * Used to ensure no conditions are being waited on in waitPop when
		 * a thread or the application is trying to exit.
		 * The queue is invalid after calling this method and it is an error
		 * to continue using a queue after this method has been called.
		 */
		void
		invalidate(void)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			m_valid = false;
			m_condition.notify_all();
		}
		
		/**
		 * Returns whether or not this queue is valid.
		 */
		bool
		isValid(void) const
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			return m_valid;
		}
	
	private:
		std::atomic_bool              m_valid{true};
		mutable std::mutex            m_mutex;
		std::priority_queue<_T,
		                    _Sequence,
		                    _Compare> m_queue;
		std::condition_variable       m_condition;
	};
	
	/**Max Heap keeping only Unique Elements
	 *
	 * @tparam _T
	 * @tparam _Sequence
	 * @tparam _Compare
	 */
	template<typename _T,
	         typename _Sequence = std::vector<_T>,
	         typename _Compare  = std::less<typename _Sequence::value_type> >
	class Threadsafe_unique_max_heap
	{
	public:
		/**
		 * Destructor.
		 */
		~Threadsafe_unique_max_heap(void)
		{
			invalidate();
		}
		
		/**
		 * Attempt to get the first value in the queue.
		 * Returns true if a value was successfully written to the out parameter, false otherwise.
		 */
		bool
		try_pop(_T& out)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			if(m_heap.empty() || !m_valid)
			{
				return false;
			}
			std::pop_heap(m_heap.begin(),
			              m_heap.end());
			out = m_heap.back();
			m_heap.pop_back();
			return true;
		}
		
		/**
		 * Get the first value in the queue.
		 * Will block until a value is available unless clear is called or the instance is destructed.
		 * Returns true if a value was successfully written to the out parameter, false otherwise.
		 */
		bool
		wait_pop(_T& out)
		{
			std::unique_lock<std::mutex> lock{m_mutex};
			m_condition.wait(lock,
			                 [this]()
			                 {
				                 return !m_heap.empty() || !m_valid;
			                 });
			/*
			 * Using the condition in the predicate ensures that spurious wakeups with a valid
			 * but empty queue will not proceed, so only need to check for validity before proceeding.
			 */
			if(!m_valid)
			{
				return false;
			}
			std::pop_heap(m_heap.begin(),
			              m_heap.end());
			out = m_heap.back();
			m_heap.pop_back();
			return true;
		}
		
		/**
		 * Push a new value onto the queue.
		 */
		void
		push(_T value)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			auto                        i = std::find(m_heap.begin(),
			                                          m_heap.end(),
			                                          value);
			if(i == m_heap.end())
			{
				m_heap.push_back(std::move(value));
				std::make_heap(m_heap.begin(),
				               m_heap.end());
				m_condition.notify_one();
			}
		}
		
		/**
		 * Check whether or not the queue is empty.
		 */
		bool
		empty(void) const
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			return m_heap.empty();
		}
		
		/**
		 * Clear all items from the queue.
		 */
		void
		clear(void)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			m_heap.clear();
			m_condition.notify_all();
		}
		
		/**
		 * Invalidate the queue.
		 * Used to ensure no conditions are being waited on in waitPop when
		 * a thread or the application is trying to exit.
		 * The queue is invalid after calling this method and it is an error
		 * to continue using a queue after this method has been called.
		 */
		void
		invalidate(void)
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			m_valid = false;
			m_condition.notify_all();
		}
		
		/**
		 * Returns whether or not this queue is valid.
		 */
		bool
		isValid(void) const
		{
			std::lock_guard<std::mutex> lock{m_mutex};
			return m_valid;
		}
	
	private:
		std::atomic_bool        m_valid{true};
		mutable std::mutex      m_mutex;
		std::vector<_T>         m_heap;
		std::condition_variable m_condition;
	};
}
#endif //DHCP_ASP_THREAD_SAFE_QUEUE_H
