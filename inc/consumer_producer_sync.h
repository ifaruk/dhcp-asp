/**
 * Created by f on 04/12/17.
 * Consumer-Producer Synchronizer
 *
 */

#ifndef DHCP_ASP_CONSUMER_PRODUCER_SYNC_H
#define DHCP_ASP_CONSUMER_PRODUCER_SYNC_H

#include <set>
#include <map>
#include <shared_mutex>
#include <condition_variable>
#include <memory>

#include <thread_safe_queue.h>

class CP_sync;

class CP_sync_accessor
{
public:
	void
	signal_self();
	
	void
	signal_all();
	
	void
	wait_signal();

private:
	friend class CP_sync;
	CP_sync_accessor(uint64_t key,
	                 CP_sync& obj);
	
	uint64_t _key;
	CP_sync& _obj;
};

using CP_sync_accessor_sp = std::shared_ptr<CP_sync_accessor>;

class CP_sync
{
public:
	inline CP_sync_accessor_sp
	register_thread(uint64_t key)
	{
		std::unique_lock<std::shared_mutex> lock(_cv_mtx);
		_cvs.try_emplace(key,
		                 std::make_shared<std::condition_variable>());
		
		return CP_sync_accessor_sp(new CP_sync_accessor(key,
		                                                *this));
	}
	
	inline void
	signal(uint64_t key)
	{
		std::shared_lock<std::shared_mutex> lock(_cv_mtx);
		_cvs[key]->notify_all();
	}
	
	inline void
	signal_all()
	{
		std::shared_lock<std::shared_mutex> lock(_cv_mtx);
		for(auto& c:_cvs)
		{
			c.second->notify_all();
		}
	}
	
	inline void
	wait_signal(uint64_t key)
	{
		//		std::shared_lock<std::shared_mutex> lock(_cv_mtx);
		//TODO check if access can be dangerous, if the container '_queue' may change in the future
		std::mutex                   temp;
		std::unique_lock<std::mutex> lock(temp);
		_cvs[key]->wait(lock);
	}

private:
	std::shared_mutex                                  _cv_mtx;
	std::map<uint64_t,
	         std::shared_ptr<std::condition_variable>> _cvs;
};

using CP_sync_sp = std::shared_ptr<CP_sync>;

#endif //DHCP_ASP_CONSUMER_PRODUCER_SYNC_H
