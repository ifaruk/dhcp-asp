//
// Created by f on 18/11/17.
//

#ifndef DHCP_ASP_LOGGER_H
#define DHCP_ASP_LOGGER_H

#include <string>

#include <g3log/g3log.hpp>

void
initialize_logger(std::string file_name="",
                  std::string path="");

void
deinitialize_logger(void);

#endif //DHCP_ASP_LOGGER_H
