//
// Created by f on 27/10/17.
//

#ifndef DHCP_ASP_UTILS_H
#define DHCP_ASP_UTILS_H

#include <string>
#include <vector>
#include <memory>
#include <chrono>
#include <atomic>
#include <mutex>
#include <unordered_map>
#include <unordered_set>
#include <set>

#include <clingo.hh>

namespace DHCP
{
	
	//================================================================================
	/**@warning None of the atoms and predicates should contain Time argument
	 *
	 */
	class Predicate
	{
	public:
		Predicate(std::string name = "",
		          uint32_t no_param = 0)
			: _name(name),
			  _no_param(no_param)
		{
			_str = std::string(_name + "/" + std::to_string(_no_param));
		}
		
		std::string const&
		name() const
		{return _name;}
		
		uint32_t const&
		no_param() const
		{return _no_param;}
		
		std::string const&
		to_string() const
		{return _str;};
		
		bool
		operator==(Predicate const& other) const
		{return _str == other._str;}
		
		bool
		operator<(Predicate const& other) const
		{return _str < other._str;}
	
	private:
		std::string _name;
		uint32_t    _no_param;
		
		std::string _str;
	};
	
	struct Hash_predicate
	{
		size_t
		operator()(Predicate const& tt) const
		{
			return std::hash<std::string>()(tt.to_string());
		}
	};
	
	//================================================================================
	class Predicate_set: public std::unordered_set<Predicate,
	                                               Hash_predicate>
	{
	public:
		using std::unordered_set<Predicate,
		                         Hash_predicate>::unordered_set;
		
		std::string
		to_string() const;
	};
	
	using Predicate_set_sp = std::shared_ptr<Predicate_set>;
	
	
	//================================================================================
	/**@warning None of the atoms and predicates should contain Time argument
	 *
	 */
	class Atom
	{
	public:
		
		Atom()
			: _str(""),
			  _predicate()
		{}
		
		Atom(std::string str)
			: _str(std::move(str)),
			  _predicate(generate_predicate(_str))
		{
		}
		
		std::string
		to_string() const
		{
			return _str;
		}
		
		Predicate const&
		get_predicate() const
		{return _predicate;}
		
		bool
		operator<(Atom const& other) const
		{return _str < other._str;}
		
		bool
		operator==(Atom const& other) const
		{return _str == other._str;}
		
		bool
		operator!=(Atom const& other) const
		{return _str != other._str;}
		
		static Predicate
		generate_predicate(std::string atom_str);
	
	private:
		std::string _str;
		Predicate   _predicate;
	};
	
	using Atom_sp = std::shared_ptr<Atom>;
	//	using Atom_vec = std::vector<Atom>;
	using Atom_set= std::set<Atom>;
	using Atom_set_sp= std::shared_ptr<Atom_set>;
	//	using Atom_sp_vec = std::vector<Atom_sp>;
	//	using Atom_set_sp = std::shared_ptr<Atom_vec>;
	//	using Atom_sp_vec_sp = std::shared_ptr<Atom_sp_vec>;
	
	//---------------
	
	struct Hash_atom
	{
		size_t
		operator()(Atom const& tt) const
		{
			return std::hash<std::string>()(tt.to_string()); //It is enough to hash atom, its predicate can not be different if it would then the language would be ambiguous.
		}
	};
	
	//================================================================================
	
	using Time_step = Atom_set;
	using Time_step_sp = std::shared_ptr<Time_step>;
	using Time_sequence = std::vector<Time_step_sp>;
	using Time_sequence_sp =  std::shared_ptr<Time_sequence>;
	
	//================================================================================
	
	
	template<class T>
	std::string
	to_string(T const& tstep,
	          char sep = ',')
	{
		std::string output;
		for(auto const& a:tstep)
		{
			output += a.to_string() + sep;
		}
		if(!tstep.empty())
		{
			output.pop_back();
		}
		return output;
	}
	
	std::string
	to_string(Time_sequence const& ts);
	
	std::string
	to_argument(Time_sequence const& ts);
	
	//================================================================================
	
	Time_step
	filter(Time_step const& tseq,
	       Predicate_set const& filter_by);
	
	Time_sequence
	filter(Time_sequence const& tseq,
	       Predicate_set const& filter_by);
	
	//=================================================================================
	
	class Sensing_to_outcome_map: public std::unordered_map<Atom,
	                                                        Atom_set,
	                                                        Hash_atom>
	{
	public:
		using std::unordered_map<Atom,
		                         Atom_set,
		                         Hash_atom>::unordered_map;
		
		std::string
		to_str() const;
	};
	
	//=================================================================================
	class DIF
	{
	public:
		
		DIF(std::string const& file_content);
		
		inline Predicate_set const&
		get_fluent_predicates() const
		{return _fluent_pred;}
		
		inline Predicate_set const&
		get_act_actuation_predicates() const
		{return _act_actuation_pred;}
		
		inline Predicate_set const&
		get_act_sensing_predicates() const
		{return _act_sensing_pred;}
		
		inline Predicate_set const&
		get_outcome_predicates() const
		{return _outcomes_pred;}
		
		inline Predicate_set const&
		get_additional_predicates() const
		{return _additional_pred;}
		
		inline Atom_set const&
		get_fluent_atoms() const
		{return _fluent_atoms;}
		
		inline Atom_set const&
		get_act_actuation_atoms() const
		{return _act_actuation_atoms;}
		
		inline Atom_set const&
		get_act_sensing_atoms() const
		{return _act_sensing_atoms;}
		
		inline Atom_set const&
		get_outcome_atoms() const
		{return _outcomes_atoms;}
		
		inline Atom_set const&
		get_additional_atoms() const
		{return _additional_atoms;}
		
		inline Sensing_to_outcome_map const&
		get_sensing_to_outcome() const
		{return _sensing_to_outcome;}
	
	private:
		Predicate_set _fluent_pred;
		Predicate_set _act_actuation_pred;
		Predicate_set _act_sensing_pred;
		Predicate_set _outcomes_pred;
		Predicate_set _additional_pred;
		
		Atom_set _fluent_atoms;
		Atom_set _act_actuation_atoms;
		Atom_set _act_sensing_atoms;
		Atom_set _outcomes_atoms;
		Atom_set _additional_atoms;
		
		Sensing_to_outcome_map _sensing_to_outcome;
	};
	
	using DIF_c_sp = std::shared_ptr<DIF const>;
	
	//=================================================================================
	
	std::string
	get_file_content(std::string file_name,
	                 bool required = true);
	
	//=================================================================================
	
	Time_sequence_sp
	parse_initial_constraints(std::string initial_constraints);
	
	//=================================================================================
	
	std::vector<std::string>
	parse_server_list_str(std::string server_list_str);
	
	//=================================================================================
	
	std::pair<bool,
	          Clingo::SymbolVector>
	get_solution(Clingo::Control& ctl);
	
	//=================================================================================
	
	Clingo::SymbolVector
	solve_problem(std::string problem,
	              std::string clingo_parameters = "");
	
	//=================================================================================
	
	Atom_set_sp
	solution_to_atoms(Clingo::SymbolVector const& v);
	
	//=================================================================================
	
	class Problem_data
	{
	public:
		Problem_data(std::vector<std::string> clingo_arguments,
		             std::string const& domain_file_name,
		             std::string const& goal_file_name,
		             std::string const& domain_insight_file_name,
		             uint32_t makespan_limit,
		             uint32_t time_limit_sec,
		             uint32_t verbosity)
			: _clingo_arguments(std::move(clingo_arguments)),
			  _domain_file_name(domain_file_name),
			  _domain_file_content(get_file_content(domain_file_name)),
			  _goal_file_name(goal_file_name),
			  _goal_file_content(get_file_content(goal_file_name)),
			  _domain_insight_file_name(domain_insight_file_name),
			  _domain_insight_file_content(get_file_content(domain_insight_file_name)),
			  _makespan_limit(makespan_limit),
			  _time_limit_sec(time_limit_sec),
			  _verbosity(verbosity),
			  _dif(get_file_content(domain_insight_file_name))
		{}
	
	public:
		std::vector<std::string> const _clingo_arguments;
		std::string const              _domain_file_name;
		std::string const              _domain_file_content;
		std::string const              _goal_file_name;
		std::string const              _goal_file_content;
		std::string const              _domain_insight_file_name;
		std::string const              _domain_insight_file_content;
		uint32_t const                 _makespan_limit;
		uint32_t const                 _time_limit_sec;
		uint32_t const                 _verbosity;
		DIF const                      _dif;
	};
	
	using Problem_data_sp = std::shared_ptr<Problem_data>;
}
#endif //DHCP_ASP_UTILS_H

