//
// Created by f on 05/12/17.
//

#include <consumer_producer_sync.h>

void
CP_sync_accessor::signal_self()
{
	_obj.signal(_key);
}

void
CP_sync_accessor::signal_all()
{
	_obj.signal_all();
}

void
CP_sync_accessor::wait_signal()
{
	_obj.wait_signal(_key);
}

CP_sync_accessor::CP_sync_accessor(uint64_t key,
                                   CP_sync& obj)
	: _key(key),
	  _obj(obj)
{
}
