//
// Created by f on 18/11/17.
//

#include <iostream>
#include <chrono>
#include <experimental/filesystem>


#include <g3log/g3log.hpp>
#include <g3log/logworker.hpp>
#include <g3log/std2_make_unique.hpp>

#include <logger.h>

std::unique_ptr<g3::LogWorker>      worker;
std::unique_ptr<g3::FileSinkHandle> handle;


void
initialize_logger(std::string file_name,
                  std::string path)
{
	using namespace std::experimental::filesystem::v1;
	if(file_name=="")
	{
		file_name="log";
	}
	if(path=="")
	{
		path = current_path().string();
	}
	
	worker = g3::LogWorker::createLogWorker();
	handle = worker->addDefaultLogger(file_name,
	                                  path);
	g3::initializeLogging(worker.get());
	
	std::future<std::string> log_file_name = handle->call(& g3::FileSink::fileName);
}

void
deinitialize_logger(void)
{

}