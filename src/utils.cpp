//
// Created by f on 27/10/17.
//
#include <algorithm>
#include <regex>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <utils.h>
#include <logger.h>

namespace DHCP
{
	
	
	//================================================================================
	
	std::string
	Predicate_set::to_string() const
	{
		std::string s("{");
		for(auto const& p:* this)
		{
			s += p.to_string() + ",";
		}
		if(!empty())
		{
			s.pop_back();
		}
		return s + "}";
	}
	
	//================================================================================
	Predicate
	Atom::generate_predicate(std::string atom_str)
	{
		//Remove all whitespaces from string
		atom_str.erase(std::remove_if(atom_str.begin(),
		                              atom_str.end(),
		                              isspace),
		               atom_str.end());
		
		
		//Find first paranthesis
		auto        open_paranthesis_index = atom_str.find('(');
		std::string name;
		auto        no_params              = 0;
		
		if(std::string::npos != open_paranthesis_index)
		{
			name = atom_str.substr(0,
			                       open_paranthesis_index);
			no_params++;
			//Find number of arguments
			int depth = 0;
			
			for(std::string::size_type cur_index = open_paranthesis_index + 1;
			    cur_index < atom_str.size() - 1;
			    cur_index++)
			{
				if(',' == atom_str.at(cur_index) && 0 == depth)
				{
					no_params++;
				}
				else if('(' == atom_str.at(cur_index))
				{
					depth++;
				}
				else if(')' == atom_str.at(cur_index))
				{
					depth--;
				}
			}
		}
		else
		{
			name = atom_str;
		}
		return Predicate(name,
		                 no_params);
	}
	
	//================================================================================
	
	
	
	//================================================================================
	
	std::string
	to_string(Time_sequence const& ts)
	{
		std::stringstream output;
		uint32_t          time_step_count = 0;
		for(auto const& tstep:ts)
		{
			output << std::setw(4) << time_step_count << ":" << to_string<Time_step>(* tstep) << std::endl;
			time_step_count++;
		}
		return output.str();
	}
	
	//================================================================================
	
	std::string
	to_argument(Time_sequence const& ts)
	{
		std::stringstream output;
		for(auto const& tstep:ts)
		{
			output << "{" << to_string<Time_step>(* tstep,
			                                      ';') << "}";
		}
		return output.str();
	}
	
	//================================================================================
	
	Time_step
	filter(Time_step const& tstep,
	       Predicate_set const& filter_by)
	{
		Time_step ret;
		auto      copyIfIterator = std::copy_if(tstep.begin(),
		                                        tstep.end(),
		                                        std::inserter(ret,
		                                                      ret.end()),
		                                        [&filter_by](Atom const& a)
		                                        {
			                                        return filter_by.find(a.get_predicate()) != filter_by.end();
		                                        });
		//		ret.erase(copyIfIterator,
		//		          ret.end());
		//		std::sort(ret.begin(),
		//		          ret.end());
		//
		
		//		constexpr auto func = [](auto const& a,
		//		                         auto const& p) -> bool
		//		{return a.pred_string() < p.pred_string();};
		//
		//		std::set_intersection(tstep.begin(),
		//		                      tstep.end(),
		//		                      filter_by.begin(),
		//		                      filter_by.end(),
		//		                      std::inserter(ret,
		//		                                    ret.begin()),
		//		                      func);
		
		return ret;
	}
	
	Time_sequence
	filter(Time_sequence const& tseq,
	       Predicate_set const& filter_by)
	{
		Time_sequence ret;
		ret.reserve(tseq.size());
		for(Time_step_sp const& tstep:tseq)
		{
			ret.emplace_back(std::make_shared<Time_step>(filter(* tstep.get(),
			                                                    filter_by)));
		}
		return ret;
	}
	
	//	Time_sequence_sp
	//	filter(Time_sequence_sp const& tseq,
	//	       Predicate_set const& filter)
	//	{
	//		Time_sequence_sp ret = std::make_shared<Time_sequence>();
	//		ret->try_reserve(tseq->size());
	//		for(uint32_t i = 0;
	//		    i < tseq->size();
	//		    i++)
	//		{
	//			Time_step_sp tstep = std::make_shared<Time_step>();
	//			std::for_each((* tseq)[i]->begin(),
	//			              (* tseq)[i]->end(),
	//			              [&tstep,
	//			               &filter](Atom const& a)
	//			              {
	//				              if(filter.find(a._predicate) != filter.end())
	//				              {
	//					              tstep->emplace_back(a);
	//				              }
	//			              });
	//			ret->emplace_back(tstep);
	//		}
	//		return ret;
	//	}
	//
	//
	
	
	//================================================================================
	
	
	std::string
	Sensing_to_outcome_map::to_str() const
	{
		std::string s;
		for(auto const& p:* this)
		{
			s += p.first.to_string() + "\n";
			for(auto const& outcome:p.second)
			{
				s += "\t\t" + outcome.to_string() + "\n";
			}
		}
		if(!empty())
		{
			s.pop_back();
		}
		return s;
	}
	
	DIF::DIF(std::string const& file_content)
	{
		namespace bpt = boost::property_tree;
		bpt::ptree pt;
		
		try
		{
			std::stringstream ss;
			ss << file_content;
			bpt::read_xml(ss,
			              pt);
		}
		catch(std::exception const& e)
		{
			throw std::runtime_error("Parsing 'dif' file failed with error: " + std::string(e.what()));
		}
		
		auto generator_common_program = pt.get<std::string>("dhcp.clingo_generator_common");
		
		
		//------------------------------------------------
		//Read Predicates
		using Exop_param_type = decltype(pt.get_child("dhcp").begin()->second);
		auto read_predicates = [&pt,
		                        &generator_common_program](std::string read_of,
		                                                   Predicate_set& place_preds,
		                                                   Atom_set& place_atoms,
		                                                   std::function<void(Exop_param_type&,
		                                                                      Atom_set_sp)> extra_operation)
		{
			try
			{
				for(auto& v:pt.get_child("dhcp"))
				{
					if(read_of == v.first)
					{
						place_preds.emplace(v.second.get<std::string>("<xmlattr>.name"),
						                    v.second.get<uint32_t>("<xmlattr>.no_param"));
						
						//						LOG(INFO) << "Predicate found: " << v.second.get<std::string>("<xmlattr>.name") << "/"
						//						          << v.second.get<uint32_t>("<xmlattr>.no_param") << ".";
						
						auto generator_asp_program = v.second.get<std::string>("clingo_generator");
						if(!generator_asp_program.empty())
						{
							Atom_set_sp sol = solution_to_atoms(solve_problem(generator_asp_program + generator_common_program,
							                                                  "--quite=1"));
							
							place_atoms.insert(sol->begin(),
							                   sol->end());
							extra_operation(v.second,
							                sol);
						}
					}
				}
			}
			catch(const bpt::ptree_error& e)
			{
				std::stringstream ss;
				ss << "DIF file: Failed at parsing " << read_of << " with " << e.what();
				throw std::runtime_error(ss.str());
			}
		};
		
		auto do_nothing_extra = [](Exop_param_type& v,
		                           Atom_set_sp av)
		{};
		
		//		LOG(INFO) << "Reading Fluents";
		
		read_predicates("fluent",
		                _fluent_pred,
		                _fluent_atoms,
		                do_nothing_extra);
		
		//		LOG(INFO) << "Reading Actuation Actions";
		
		read_predicates("act_actuation",
		                _act_actuation_pred,
		                _act_actuation_atoms,
		                do_nothing_extra);
		
		//		LOG(INFO) << "Reading Sensing Actions";
		
		read_predicates("act_sensing",
		                _act_sensing_pred,
		                _act_sensing_atoms,
		                [&](Exop_param_type& v,
		                    Atom_set_sp av)
		                {
			                _outcomes_pred.emplace(v.get<std::string>("outcome.<xmlattr>.name"),
			                                       v.get<uint32_t>("outcome.<xmlattr>.no_param"));
			
			                //			                LOG(INFO) << "Predicate found: " << v.get<std::string>("outcome.<xmlattr>.name") << "/"
			                //			                          << v.get<uint32_t>("outcome.<xmlattr>.no_param") << ".";
			
			                auto generator_asp_program = v.get<std::string>("outcome.clingo_generator");
			
			                for(auto const& a:* av)
			                {
				                //Add sensing action as a fact to generate outcomes specific to it
				                auto cur_fact = a.to_string() + ".\n";
				                auto sol      = solution_to_atoms(solve_problem(generator_asp_program + generator_common_program + cur_fact,
				                                                                "--quite=1"));
				
				                //				                for(auto const& a2:* sol)
				                //				                {
				                //					                LOG(INFO) << "Outcome Atom found for sensing action : " << a._str << "-->" << a2._str;
				                //				                }
				
				                _outcomes_atoms.insert(sol->begin(),
				                                       sol->end());
				                _sensing_to_outcome.try_emplace(a,
				                                                Atom_set(sol->begin(),
				                                                         sol->end()));
			                }
		                });
		
		//		LOG(INFO) << "Reading Additional";
		
		//		read_predicates("additional",
		//		                _additional_pred,
		//		                _additional_atoms,
		//		                do_nothing_extra);
		
		//TODO adhoc fix this
		_additional_pred.insert(Predicate("redundant",
		                                  1));
	}
	
	//================================================================================
	
	
	std::string
	get_file_content(std::string file_name,
	                 bool required)
	{
		std::ifstream     f(file_name);
		if(!f)
		{
			if(required)
			{
				std::stringstream ss;
				ss << "File not found " << file_name;
				throw std::runtime_error(ss.str());
			}
			else
			{
				return "";
			}
		}
		std::stringstream fss;
		fss << f.rdbuf();
		return fss.str();
	}
	
	//================================================================================
	
	Time_sequence_sp
	parse_initial_constraints(std::string initial_constraints_str)
	{
		Time_sequence_sp constraints = std::make_shared<Time_sequence>();
		std::smatch      sm;
		std::regex       time_step_rgx("\\{([^}]+)\\}");
		while(std::regex_search(initial_constraints_str,
		                        sm,
		                        time_step_rgx))
		{
			constraints->push_back(std::make_shared<Time_step>());
			//			LOG(INFO) << "Time Step: " << sm.str();
			auto time_step_str = sm[1].str();
			
			auto found = time_step_str.find(';');
			while(std::string::npos != found)
			{
				constraints->back()->emplace(time_step_str.substr(0,
				                                                  found));
				time_step_str = time_step_str.substr(found + 1);
				found         = time_step_str.find(';');
			}
			if(!time_step_str.empty())
			{
				constraints->back()->emplace(time_step_str);
			}
			initial_constraints_str = sm.suffix();
		}
		return constraints;
	}
	
	//================================================================================
	
	std::vector<std::string>
	parse_server_list_str(std::string server_list_str)
	{
		std::vector<std::string> ip_list;
		std::smatch              sm;
		std::regex               ip_regex(",?((?:\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}|localhost):\\d+),?");
		while(std::regex_search(server_list_str,
		                        sm,
		                        ip_regex))
		{
			//			LOG(INFO) << "Server IP Captured: " << sm[1].str();
			ip_list.emplace_back(sm[1].str());
			server_list_str = sm.suffix();
		}
		return ip_list;
	}
	
	//================================================================================
	
	std::pair<bool,
	          Clingo::SymbolVector>
	get_solution(Clingo::Control& ctl)
	{
		Clingo::SymbolVector latest_model_symbols;
		
		//		#ifdef DHCP_DEBUG
		//		std::string solution_atoms;
		//		#endif
		
		bool     found = false;
		for(auto m: ctl.solve())
		{
			found                = true;
			latest_model_symbols = m.symbols();
			//			#ifdef DHCP_DEBUG
			//			for(auto& atom : latest_model_symbols)
			//			{
			//				solution_atoms += " " + atom.to_string();
			//			}
			//			solution_atoms += "\n";
			//			#endif
		}
		//		LOG(DEBUG) << "Answer Set:" << solution_atoms;
		
		return std::make_pair(found,
		                      latest_model_symbols);
	}
	
	//================================================================================
	
	Clingo::SymbolVector
	solve_problem(std::string problem,
	              std::string clingo_parameters)
	{
		Clingo::Logger  logger([](Clingo::WarningCode,
		                          char const* message)
		                       {
			                       std::cout << "Clingo :" << message;
		                       });
		Clingo::Control m_ctl{{nullptr,///TODO clingo_arguments will be given
		                       size_t(0)},
		                      logger,
		                      100};
		
		//		LOG(INFO) << "Solving problem ======================================================" << std::endl << problem;
		try
		{
			m_ctl.add("base",
			          {},
			          problem.c_str());
			m_ctl.ground({{"base", {}}});
			
			return get_solution(m_ctl).second;
		}
		catch(std::exception& e)
		{
			LOG(FATAL) << e.what() << std::endl << problem;
		}
	}
	
	//================================================================================
	
	Atom_set_sp
	solution_to_atoms(Clingo::SymbolVector const& v)
	{
		Atom_set_sp ret = std::make_shared<Atom_set>();
		for(auto    a:v)
		{
			ret->emplace(a.to_string());
		}
		return ret;
	}
	
	
	//================================================================================
}