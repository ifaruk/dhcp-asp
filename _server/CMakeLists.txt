MESSAGE(STATUS "======================SERVER======================")

set(HEADER_FILES
        ${cs_proto_hdrs}
        ${cs_grpc_hdrs}
        ${CMAKE_SOURCE_DIR}/inc/utils.h
        ${CMAKE_SOURCE_DIR}/inc/logger.h
        ${CMAKE_SOURCE_DIR}/inc/option_printer.h
        ${CMAKE_SOURCE_DIR}/inc/custom_option_description.h
        inc/grpc.h
        inc/solver.h
        inc/server.h)


set(SOURCE_FILES
        ${HEADER_FILES}
        ${cs_proto_srcs}
        ${cs_grpc_srcs}
        ${CMAKE_SOURCE_DIR}/src/utils.cpp
        ${CMAKE_SOURCE_DIR}/src/logger.cpp
        ${CMAKE_SOURCE_DIR}/src/option_printer.cpp
        ${CMAKE_SOURCE_DIR}/src/custom_option_description.cpp
        src/grpc.cpp
        src/solver.cpp
        src/server.cpp
        src/main.cpp)


set_source_files_properties(${cs_grpc_srcs} PROPERTIES GENERATED TRUE)
set_source_files_properties(${cs_grpc_hdrs} PROPERTIES GENERATED TRUE)
set_source_files_properties(${cs_proto_srcs} PROPERTIES GENERATED TRUE)
set_source_files_properties(${cs_proto_hdrs} PROPERTIES GENERATED TRUE)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/inc)
add_executable(dhcp-asp-server ${SOURCE_FILES})
target_include_directories(dhcp-asp-server BEFORE PUBLIC ${Boost_INCLUDE_DIRS})
set_target_properties(dhcp-asp-server PROPERTIES LINKER_LANGUAGE CXX)
add_dependencies(dhcp-asp-server cs_grpc-generate)

target_link_libraries(dhcp-asp-server PRIVATE pthread)
target_link_libraries(dhcp-asp-server PRIVATE ${Protobuf_LIBRARIES})
target_link_libraries(dhcp-asp-server PRIVATE grpc++_unsecure)
target_link_libraries(dhcp-asp-server PRIVATE ${G3LOG})
target_link_libraries(dhcp-asp-server PRIVATE clingo)
target_link_libraries(dhcp-asp-server PRIVATE ${Boost_LIBRARIES})
target_link_libraries(dhcp-asp-server PRIVATE stdc++fs)

INSTALL(TARGETS dhcp-asp-server RUNTIME DESTINATION bin)