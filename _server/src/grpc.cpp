//
// Created by f on 22/11/17.
//

#include <regex>
#include <future>

#include <grpc.h>

std::string
exec(const char* cmd)
{
	
	std::string           result;
	std::shared_ptr<FILE> pipe(popen(cmd,
	                                 "r"),
	                           pclose);
	
	char buffer[128];
	if(!pipe)
	{
		if(DHCP::Server::verbosity >= 1)
		{
			LOG(FATAL) << "popen() failed on command: \"" << cmd << "\"";
		}
		
		throw std::runtime_error("popen() failed!");
	}
	while(!feof(pipe.get()))
	{
		if(fgets(buffer,
		         128,
		         pipe.get()) != nullptr)
		{
			result += buffer;
		}
	}
	return result;
}

namespace Scontrol
{
	
	std::pair<grpc::Status,
	          std::shared_ptr<Pong>>
	CM_ping::generate_response(Ping const& p)
	{
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Ping Request Received";
		}
		std::cout << "Ping Request Received" << std::endl;
		return std::make_pair(grpc::Status::OK,
		                      std::make_shared<Pong>());
	}
	
	
	//==========================================================================================
	
	
	std::pair<grpc::Status,
	          std::shared_ptr<Server_feature_resp>>
	CM_get_server_features::generate_response(Server_feature_req const& p)
	{
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Get Features Request Received";
		}
		
		std::string lscpu_ret = exec("lscpu");
		//		std::string lsmem_ret = exec("lsmem");//TODO fix
		std::string lsmem_ret = "0";
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "lscpu returned:" << std::endl << lscpu_ret << std::endl;
		}
		
		auto get_unsigned_value         = [](std::string inside_of,
		                                     std::string regx,
		                                     std::string looking_for,
		                                     uint32_t& out_val,
		                                     std::string& error) -> bool
		{
			std::smatch sm;
			if(std::regex_search(inside_of,
			                     sm,
			                     std::regex(regx)))
			{
				if(DHCP::Server::verbosity >= 3)
				{
					LOG(INFO) << looking_for << " found:" << std::string(sm[1].first,
					                                                     sm[1].second) << std::endl;
				}
				out_val = std::stoi(std::string(sm[1].first,
				                                sm[1].second));
				return true;
			}
			else
			{
				if(DHCP::Server::verbosity >= 1)
				{
					LOG(FATAL) << "Can't find " << looking_for << "!" << std::endl;
				}
				throw std::runtime_error("Can't find " + looking_for + "!");
				error += "Can't find " + looking_for + "!\n";
				return false;
			}
		};
		
		std::string errors;
		uint32_t    no_sockets          = 0;
		uint32_t    no_cores_per_socket = 0;
		uint32_t    no_threads_per_core = 0;
		uint32_t    cpu_MHz             = 0;
		uint32_t    memory_GB           = 0;
		
		get_unsigned_value(lscpu_ret,
		                   "Socket\\(s\\):\\s*(\\d+)",
		                   "# of sockets (Processor)",
		                   no_sockets,
		                   errors);
		get_unsigned_value(lscpu_ret,
		                   "Core\\(s\\) per socket:\\s*(\\d+)",
		                   "# cores per socket",
		                   no_cores_per_socket,
		                   errors);
		get_unsigned_value(lscpu_ret,
		                   "Thread\\(s\\) per core:\\s*(\\d+)",
		                   "Thread # per core",
		                   no_threads_per_core,
		                   errors);
		get_unsigned_value(lscpu_ret,
		                   "CPU MHz:\\s*(\\d+)",
		                   "CPU MHz",
		                   cpu_MHz,
		                   errors);
//		get_unsigned_value(lsmem_ret,
//		                   "Total online memory:\\s*(\\d+)",
//		                   "Memory Size",
//		                   memory_GB,
//		                   errors);
		
		auto reply = std::make_shared<Server_feature_resp>();
		reply->set_no_processors(no_sockets);
		reply->set_no_cores_per_processor(no_cores_per_socket);
		reply->set_no_threads_per_core(no_threads_per_core);
		reply->set_cpu_mhz(cpu_MHz);
		reply->set_memory_mb(memory_GB * 1024);
		
		if(errors == "")
		{
			return std::make_pair(grpc::Status::OK,
			                      reply);
		}
		else
		{
			return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
			                                   errors),
			                      std::make_shared<Server_feature_resp>());
		}
	}
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<Server_state_resp>>
	CM_get_server_state::generate_response(Server_state_req const& c)
	{
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Get State Request Received" << std::endl;
		}
		
		std::string loadavg_ret = exec("cat /proc/loadavg");
		std::string meminfo_ret = exec("cat /proc/meminfo");
		
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "loadavg returned:" << std::endl << loadavg_ret << std::endl;
			LOG(INFO) << "meminfo returned:" << std::endl << meminfo_ret << std::endl;
		}
		
		auto get_unsigned_value          = [](std::string inside_of,
		                                      std::string regx,
		                                      std::string looking_for,
		                                      double& out_val,
		                                      std::string& error) -> bool
		{
			std::smatch sm;
			if(std::regex_search(inside_of,
			                     sm,
			                     std::regex(regx)))
			{
				if(DHCP::Server::verbosity >= 3)
				{
					LOG(INFO) << looking_for << " found:" << std::string(sm[1].first,
					                                                     sm[1].second) << std::endl;
				}
				out_val = std::stod(std::string(sm[1].first,
				                                sm[1].second));
				return true;
			}
			else
			{
				if(DHCP::Server::verbosity >= 3)
				{
					LOG(FATAL) << "Can't find " << looking_for << "!" << std::endl;
				}
				throw std::runtime_error("Can't find " + looking_for + "!");
				error += "Can't find " + looking_for + "!\n";
				return false;
			}
		};
		
		std::string errors;
		double      one_min_load_avg     = 0;
		double      five_min_load_avg    = 0;
		double      fifteen_min_load_avg = 0;
		double      available_memory     = 0;
		
		get_unsigned_value(loadavg_ret,
		                   R"((\d+\.?\d*)\s+\d+\.?\d*\s+\d+\.?\d*)",
		                   "One minute load avg",
		                   one_min_load_avg,
		                   errors);
		
		get_unsigned_value(loadavg_ret,
		                   R"(\d+\.?\d*\s+(\d+\.?\d*)\s+\d+\.?\d*)",
		                   "Five minute load avg",
		                   five_min_load_avg,
		                   errors);
		
		get_unsigned_value(loadavg_ret,
		                   R"(\d+\.?\d*\s+\d+\.?\d*\s+(\d+\.?\d*))",
		                   "Fifteen minute load avg",
		                   fifteen_min_load_avg,
		                   errors);
		
		get_unsigned_value(meminfo_ret,
		                   "MemAvailable:\\s*(\\d+) kB",
		                   "Free Memory",
		                   available_memory,
		                   errors);
		auto reply = std::make_shared<Server_state_resp>();
		reply->set_avg_load_one_min(static_cast<google::protobuf::uint32>(one_min_load_avg * 100));
		reply->set_avg_load_five_min(static_cast<google::protobuf::uint32>(five_min_load_avg * 100));
		reply->set_avg_load_fifteen_min(static_cast<google::protobuf::uint32>(fifteen_min_load_avg * 100));
		reply->set_available_memory_mb(static_cast<google::protobuf::uint32>(available_memory / 1024));
		
		if(errors.empty())
		{
			return std::make_pair(grpc::Status::OK,
			                      reply);
		}
		else
		{
			return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
			                                   errors),
			                      std::make_shared<Server_state_resp>());
		}
	}
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<Configure_server_resp>>
	CM_configure_server::generate_response(Configure_server_req const& c)
	{
		//TODO add clingo arguments
		std::string separator = "\n\n\n==============================================================\n";
		//formatter:off
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Configure Request Received" << separator << "Domain:" << std::endl << c.domain() << separator
			          << "Goal:" << std::endl << c.goal() << separator << "DIF:" << std::endl << c.dif() << separator
			          << "Parallel Capacity:" << std::endl << c.parallel_capacity() << separator;
		}
		//formatter:on
		
		//It will not be configured simultaneously
		_dhcp_server_mtx.lock();
		if(!(* _dhcp_server))
		{
			//TODO Parse clingo arguments
			try
			{
				* _dhcp_server = std::make_shared<DHCP::Server::Server>(std::vector<std::string>(),
				                                                        c.domain(),
				                                                        c.goal(),
				                                                        std::const_pointer_cast<DHCP::DIF const>(std::make_shared<DHCP::DIF>(c.dif())),
				                                                        c.parallel_capacity());
			}
			catch(std::exception const& e)
			{
				if(DHCP::Server::verbosity >= 2)
				{
					LOG(WARNING) << "Configuration Error:" << e.what();
				}
				_dhcp_server_mtx.unlock();
				return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
				                                   "Configuration error. " + std::string(e.what())),
				                      std::make_shared<Configure_server_resp>());
			}
			_dhcp_server_mtx.unlock();
			_call_manager.set_serving_threads(c.parallel_capacity());
			
			return std::make_pair(grpc::Status::OK,
			                      std::make_shared<Configure_server_resp>());
		}
		else
		{
			_dhcp_server_mtx.unlock();
			return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
			                                   "Server is already configured. Reset before configuring again."),
			                      std::make_shared<Configure_server_resp>());
		}
	}
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<Reset_server_resp>>
	CM_reset_server::generate_response(Reset_server_req const& c)
	{
		//Every other call has their own copy of _dhcp_server
		//changing _dhcp_server this will only effect next calls
		_call_manager.set_serving_threads(0);
		_dhcp_server_mtx.lock();
		* _dhcp_server = DHCP::Server::Server_sp();
		_dhcp_server_mtx.unlock();
		return std::make_pair(grpc::Status::OK,
		                      std::make_shared<Reset_server_resp>());
	}
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<Terminate_server_resp>>
	CM_terminate_server::generate_response(Terminate_server_req const& c)
	{
		//Every other call has their own copy of _dhcp_server
		//changing _dhcp_server this will only effect next calls
		_call_manager._done = true;
		_call_manager._done_cv.notify_all();
		_dhcp_server_mtx.lock();
		* _dhcp_server = DHCP::Server::Server_sp();
		_dhcp_server_mtx.unlock();
		return std::make_pair(grpc::Status::OK,
		                      std::make_shared<Terminate_server_resp>());
	}
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<New_time_solver_resp>>
	CM_new_time_solver::generate_response(New_time_solver_req const& c)
	{
		//If _dhcp_server is changed, cur_server will not be affected
		//And at the end of this call it will be destroyed
		DHCP::Server::Server_sp cur_server = * _dhcp_server;
		if(cur_server)
		{
			auto resp = std::make_shared<New_time_solver_resp>();
			resp->set_solver_id(cur_server->new_time_solver(c.problem_time_span(),
			                                                c.constraint_time_span()));
			return std::make_pair(grpc::Status::OK,
			                      resp);
		}
		else
		{
			return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
			                                   "Server should be configured."),
			                      std::make_shared<New_time_solver_resp>());
		}
	}
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<Kill_time_solver_resp>>
	CM_kill_time_solver::generate_response(Kill_time_solver_req const& c)
	{
		//If _dhcp_server is changed, cur_server will not be affected
		//And at the end of this call it will be destroyed
		DHCP::Server::Server_sp cur_server = * _dhcp_server;
		if(cur_server)
		{
			cur_server->kill_time_solver(c.solver_id());
			return std::make_pair(grpc::Status::OK,
			                      std::make_shared<Kill_time_solver_resp>());
		}
		else
		{
			return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
			                                   "Server should be configured."),
			                      std::make_shared<Kill_time_solver_resp>());
		}
	}
	
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<Solve_resp>>
	CM_solve::generate_response(Solve_req const& req)
	{
		//If _dhcp_server is changed, cur_server will not be affected
		//And at the end of this call it will be destroyed
		DHCP::Server::Server_sp cur_server = * _dhcp_server;
		if(cur_server)
		{
			//=========================================================
			DHCP::Time_sequence_sp constraints = std::make_shared<DHCP::Time_sequence>();
			constraints->reserve(static_cast<unsigned long>(req.constraints_size()));
			//Create time sequence for each
			for(int t   = 0;
			    t < req.constraints_size();
			    t++)
			{
				constraints->emplace_back(std::make_shared<DHCP::Time_step>());
				Scontrol::Time_step const& cur_time_step = req.constraints(t);
				
				for(int i = 0;
				    i < cur_time_step.atoms_size();
				    i++)
				{
					(* constraints)[t]->emplace(cur_time_step.atoms(i));
				}
			}
			auto    sol = cur_server->solve(req.solver_id(),
			                                constraints);
			
			auto resp = std::make_shared<Solve_resp>();
			resp->set_satisfiable(sol->_satisfiable);
			resp->set_clingo_time_ms(sol->_clingo_time);
			resp->set_processing_time_ms(sol->_total_time);
			
			//=========================================================
			auto tseq_to_grpc_tstep = [](DHCP::Time_sequence_sp from,
			                             std::function<::Scontrol::Time_step*(void)> get_field)
			{
				for(DHCP::Time_step_sp const& a:* from)
				{
					auto time_sequence = get_field();
					std::for_each(a->begin(),
					              a->end(),
					              [&](DHCP::Atom const& a)
					              {time_sequence->add_atoms(a.to_string());});
				}
			};
			
			tseq_to_grpc_tstep(sol->_fluent,
			                   [&resp]() -> ::Scontrol::Time_step*
			                   {return resp->add_fluent();});
			tseq_to_grpc_tstep(sol->_act_actuation,
			                   [&resp]() -> ::Scontrol::Time_step*
			                   {return resp->add_act_actuation();});
			tseq_to_grpc_tstep(sol->_act_sensing,
			                   [&resp]() -> ::Scontrol::Time_step*
			                   {return resp->add_act_sensing();});
			tseq_to_grpc_tstep(sol->_outcome,
			                   [&resp]() -> ::Scontrol::Time_step*
			                   {return resp->add_outcome();});
			tseq_to_grpc_tstep(sol->_additional,
			                   [&resp]() -> ::Scontrol::Time_step*
			                   {return resp->add_additional();});
			
			return std::make_pair(grpc::Status::OK,
			                      resp);
		}
		else
		{
			return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
			                                   "Server should be configured."),
			                      std::make_shared<Solve_resp>());
		}
	}
	
	//==========================================================================================
	
	std::pair<grpc::Status,
	          std::shared_ptr<Cancel_resp>>
	CM_cancel::generate_response(Cancel_req const& req)
	{
		
		//If _dhcp_server is changed, cur_server will not be affected
		//And at the end of this call it will be destroyed
		DHCP::Server::Server_sp cur_server = * _dhcp_server;
		if(cur_server)
		{
			auto resp = std::make_shared<Cancel_resp>();
			resp->set_cancelled(cur_server->cancel(req.solver_id()));
			return std::make_pair(grpc::Status::OK,
			                      resp);
		}
		else
		{
			return std::make_pair(grpc::Status(grpc::StatusCode::INTERNAL,
			                                   "Server should be configured."),
			                      std::make_shared<Cancel_resp>());
		}
	}
	
	//==========================================================================================
	//==========================================================================================
	
	GRPC_call_manager::GRPC_call_manager(uint32_t port_no)
		: _dhcp_server(std::make_shared<DHCP::Server::Server_sp>()),
		  _port_no(port_no),
		  _done(false)
	{
	}
	
	GRPC_call_manager::~GRPC_call_manager()
	{
		terminate();
		std::thread([this]()
		            {
			            void* tag;  // uniquely identifies a request.
			            bool ok = true;
			            while(_cq->Next(& tag,
			                            & ok))
			            {
				            //				            delete tag;
			            }
		            }).detach();
	}
	
	// There is no shutdown handling in this code.
	void
	GRPC_call_manager::Run()
	{
		std::string server_address("localhost:" + std::to_string(_port_no));
		
		grpc::ServerBuilder builder;
		// Listen on the given address without any authentication mechanism.
		builder.AddListeningPort(server_address,
		                         grpc::InsecureServerCredentials());
		// Register "_service as the instance through which we'll communicate with
		// clients. In this case it corresponds to an *asynchronous* service.
		builder.RegisterService(& _service);
		// Get hold of the completion queue used for the asynchronous communication
		// with the gRPC runtime.
		_cq     = builder.AddCompletionQueue();
		// Finally assemble the server.
		_server = builder.BuildAndStart();
		std::cout << "Server listening on " << server_address << std::endl;
		
		
		//Server related calls
		{
			auto r = new CM_ping(& _service,
			                     _cq.get());
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_ping) " << r;
			}
		}
		
		{
			auto r = new CM_get_server_features(& _service,
			                                    _cq.get());
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_get_server_features) " << r;
			}
		}
		
		{
			auto r = new CM_get_server_state(& _service,
			                                 _cq.get());
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_get_server_state) " << r;
			}
		}
		
		//Depends on DHCP Server
		{
			auto r = new CM_configure_server(& _service,
			                                 _cq.get(),
			                                 _dhcp_server,
			                                 _dhcp_server_mtx,
			                                 * this);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_configure_server) " << r;
			}
		}
		
		{
			auto r = new CM_reset_server(& _service,
			                             _cq.get(),
			                             _dhcp_server,
			                             _dhcp_server_mtx,
			                             * this);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_reset_server) " << r;
			}
		}
		
		{
			auto r = new CM_terminate_server(& _service,
			                                 _cq.get(),
			                                 _dhcp_server,
			                                 _dhcp_server_mtx,
			                                 * this);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_terminate_server) " << r;
			}
		}
		
		{
			auto r = new CM_solve(& _service,
			                      _cq.get(),
			                      _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_solve) " << r;
			}
		}
		
		{
			auto r = new CM_cancel(& _service,
			                       _cq.get(),
			                       _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_cancel) " << r;
			}
		}
		
		{
			auto r = new CM_new_time_solver(& _service,
			                                _cq.get(),
			                                _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_new_time_solver) " << r;
			}
		}
		
		{
			auto r = new CM_kill_time_solver(& _service,
			                                 _cq.get(),
			                                 _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_kill_time_solver) " << r;
			}
		}
		
		for(uint32_t i = 0;
		    i < _least_no_parallel_cm;
		    i++)
		{
			_main_call_handler_terminate.push_back(std::make_shared<bool>(false));
			_main_call_handlers.emplace_back(std::make_shared<std::thread>(& ::Scontrol::GRPC_call_manager::HandleRpcs,
			                                                               this,
			                                                               _main_call_handler_terminate.back()));
		}
		
		std::mutex                   m;
		std::unique_lock<std::mutex> lock(m);
		_done_cv.wait(lock,
		              [this]()
		              {return _done;});
	}
	
	// This can be run in multiple threads if needed.
	void
	GRPC_call_manager::HandleRpcs(std::shared_ptr<bool> terminate_signal)
	{
		void* tag;  // uniquely identifies a request.
		bool ok = true;
		while(!* terminate_signal)
		{
			if(_cq->Next(& tag,
			             & ok))
			{
				if(ok)
				{
					static_cast<Call_manager*>(tag)->Proceed(true);
				}
				else
				{
					//					delete tag;
				}
			}
		}
	}
	
	void
	GRPC_call_manager::set_serving_threads(uint32_t no)
	{
		// Proceed to the server's main loop.
		//TODO consider _least_number_of_threads
		no *= 2;
		if(* _dhcp_server)
		{
			std::unique_lock<std::mutex> lock(_call_handler_mtx);
			if(no > (* _dhcp_server)->get_parallelism())
			{
				for(uint32_t i = (* _dhcp_server)->get_parallelism();
				    i < no;
				    i++)
				{
					_additional_call_handler_terminate.push_back(std::make_shared<bool>(false));
					_additional_call_handlers.emplace_back(std::make_shared<std::thread>(& ::Scontrol::GRPC_call_manager::HandleRpcs,
					                                                                     this,
					                                                                     _additional_call_handler_terminate.back()));
				}
			}
			else if(no < (* _dhcp_server)->get_parallelism())
			{
				for(uint32_t i = no;
				    i < (* _dhcp_server)->get_parallelism() && _additional_call_handlers.size() > _least_no_parallel_cm;
				    i++)
				{
					auto term = _additional_call_handler_terminate.back();
					* term = true;
					_additional_call_handler_terminate.pop_back();
					auto hand = _additional_call_handlers.back();
					hand->detach();
					_additional_call_handlers.pop_back();
				}
			}
		}
	}
	
	void
	GRPC_call_manager::terminate()
	{
		for(auto& cht:_additional_call_handler_terminate)
		{
			* cht = true;
		}
		for(auto& cht:_main_call_handler_terminate)
		{
			* cht = true;
		}
		_additional_call_handler_terminate.clear();
		_main_call_handler_terminate.clear();
		_server->Shutdown();
		_cq->Shutdown();
		for(auto& ch:_additional_call_handlers)
		{
			ch->detach();
		}
		for(auto& ch:_main_call_handlers)
		{
			ch->detach();
		}
		_additional_call_handlers.clear();
		_main_call_handlers.clear();
	}
}
