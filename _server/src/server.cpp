//
// Created by f on 29/11/17.
//

#include <server.h>

namespace DHCP::Server
{
	Server::Server(const std::vector<std::string>& clingo_arguments,
	               const std::string& domain_data,
	               const std::string& goal_data,
	               DIF_c_sp dif,
	               const uint32_t& parallel_capacity)
		: _domain_data(domain_data),
		  _goal_data(goal_data),
		  _dif(dif),
		  _parallel_capacity(parallel_capacity),
		  _current_workload(0),
		  _next_solver_id(1)
	{
		//Convert std::vector<std::string> to argv
		for(auto const& ca:clingo_arguments)
		{
			auto* arg = new char[ca.size() + 1];
			std::copy(ca.begin(),
			          ca.end(),
			          arg);
			arg[ca.size()] = '\0';
			_args.push_back(arg);
		}
		_args.push_back(0);
		
		//Create dummy solver to test health of problem data
		Solver(_args,
		       _dif,
		       _domain_data,
		       _goal_data,
		       0,
		       0,
		       0);
	}
	
	Server::~Server()
	{
		for(size_t i = 0;
		    i < _args.size();
		    i++)
		{
			delete[] _args[i];
		}
		
		for(auto& s:_id_to_solver)
		{
			s.second->cancel();
		}
	}
	
	Solver::Solution_sp
	Server::solve(uint32_t solver_id,
	              Time_sequence_sp constraints)
	{
		std::unique_lock<std::mutex> lock(_workload_mtx);
		_current_workload++;
		if(_current_workload <= _parallel_capacity)//TODO fix comparison synchronization
		{
			lock.unlock();
			Solver_sp solver;
			{
				std::shared_lock<std::shared_mutex> lock(_id_to_solver_smtx);
				solver = _id_to_solver[solver_id];//TODO fix solver_id may not be available
				solver->reserve();
			}
			if(solver)
			{
				{
					std::lock_guard<std::mutex> lock(_ongoing_solving_mtx);
					_ongoing_solving.insert(solver_id);
				}
				auto solution = solver->solve(constraints);
				solver->release();
				
				if(DHCP::Server::verbosity >= 3)
				{
					LOG(INFO) << "SolverID:" << solver_id << std::endl << "Makespan " << solver->get_problem_makespan()
					          << std::endl << "For constraints" << to_string(* constraints) << std::endl << " Solution: "
					          << std::endl << solution->to_string();
				}
				lock.lock();
				_current_workload--;
				return solution;
			}
			else
			{
				throw std::runtime_error("No available solver.");
			}
		}
		else
		{
			if(DHCP::Server::verbosity >= 1)
			{
				LOG(FATAL) << "Parallel Capacity Overload.";
			}
			throw std::runtime_error("Parallel Capacity Overload.");
		}
	}
	
	bool
	Server::cancel(uint32_t solver_id)
	{
		std::unique_lock<std::mutex> lock(_ongoing_solving_mtx);
		auto                         found = _ongoing_solving.find(solver_id);
		if(_ongoing_solving.end() != found)
		{
			_ongoing_solving.erase(solver_id);
			lock.unlock();
			
			std::unique_lock<std::shared_mutex> ilock(_id_to_solver_smtx);
			_id_to_solver[solver_id]->cancel();//TODO check if solver_id is available in _id_to_solver
			return true;
		}
		else
		{
			return false;
		}
	}
	
	uint32_t
	Server::new_time_solver(uint32_t problem_time_span,
	                        uint32_t constraint_time_span)
	{
		
		uint32_t solver_id = _next_solver_id++;
		auto     ns        = std::make_shared<Solver>(_args,
		                                              _dif,
		                                              _domain_data,
		                                              _goal_data,
		                                              problem_time_span,
		                                              constraint_time_span,
		                                              solver_id);
		
		std::unique_lock<std::shared_mutex> lock(_id_to_solver_smtx);
		_id_to_solver.try_emplace(solver_id,
		                          ns);
		return solver_id;
	}
	
	void
	Server::kill_time_solver(uint32_t solver_id)
	{
		std::unique_lock<std::shared_mutex> lock(_id_to_solver_smtx);
		_id_to_solver.erase(solver_id);
	}
}