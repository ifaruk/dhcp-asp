#include <memory>
#include <iostream>

#include <numeric>

#include <boost/filesystem.hpp>

#include <logger.h>
#include <grpc.h>
#include <option_printer.h>
#include <solver.h>
#include <utils.h>

bool
read_command_line(int argc,
                  char** argv,
                  int32_t& port_no,
                  uint32_t& problem_time_span,
                  uint32_t& constraint_time_span,
                  std::string& domain_file,
                  std::string& goal_file,
                  std::string& domain_insight,
                  std::string& clingo_parameters,
                  std::string& initial_conditions,
                  uint32_t& verbosity)
{
	namespace bpo=boost::program_options;
	bpo::variables_map vm;
	{
		bool                                retval = true;
		bpo::options_description            desc("Options");
		bpo::positional_options_description positional_options;
		
		
		//@formatter:off
		desc.add_options()("h","Print help messages")
											("serve",bpo::value<int32_t>(&port_no),
											 "Activates Server Mode and requires a port number \"--serve 10003\"")
											("df",bpo::value<std::string>(&domain_file),
											 "Domain File")
											("gf",bpo::value<std::string>(&goal_file),
											 "Goal File")
											("dif",bpo::value<std::string>(&domain_insight),
											 "DHCP Domain Insight File")
											("pts",bpo::value<uint32_t>(&problem_time_span),
											 "Problem Time Span: Solution should be reachable at exact time step.")
											("cts",bpo::value<uint32_t>(&constraint_time_span),
											 "Constraint Time Span: Only constraints up to this time step will be considered.")
											("cp",bpo::value<std::string>(&clingo_parameters),
											 "Clingo parameters \"--cp \"--quite=1;--stats=4\".\n "
												 "Note that arguments must be separated by semicolon.\"")
											("c",bpo::value<std::string>(&initial_conditions),
											 "Initial conditions if not 'serve' mode \n "
												"Time steps are indicated with curly brace encapsulation \"{abc(x);def(y)}\". First time step is considered '0'.\n"
												"Each atom in a time step will be separated by semicolon (';').\n"
												"\"--c \"{}{at(1);move(2,loc(1))}{place(3)}\".")
			                ("verbose",bpo::value<uint32_t>(&verbosity),
			                  "Verbosity of running program to log. Default is 1\n"
										    "0: No Log\n"
										    "1: Fatal\n"
										    "2: Warning\n"
										    "3: Info\n");
	//@formatter:on
		
		
		bpo::store(bpo::command_line_parser(argc,
		                                    const_cast<char const* const*>(argv)).options(desc).style(bpo::command_line_style::default_style | bpo::command_line_style::allow_long_disguise).run(),
		           vm);
		bpo::notify(vm);
		
		if(vm.count("serve") == 0)
		{
			port_no = -1;
			if(vm.count("df") == 0)
			{
				retval = false;
				std::cerr << "If not in 'Standalone' mode 'Domain File'(--df) are necessary" << std::endl;
			}
			if(vm.count("gf") == 0)
			{
				retval = false;
				std::cerr << "If not in 'Standalone' mode 'Goal File'(--gf) are necessary" << std::endl;
			}
			if(vm.count("dif") == 0)
			{
				retval = false;
				std::cerr << "If not in 'Standalone' mode 'Domain Insight File'(--dif) are necessary" << std::endl;
			}
			if(vm.count("pts") == 0)
			{
				retval = false;
				std::cerr << "If not in 'Standalone' mode 'Problem Time Span'(--pts) are necessary" << std::endl;
			}
			if(vm.count("cts") == 0)
			{
				retval = false;
				std::cerr << "If not in 'Standalone' mode 'Constraint Time Span'(--cts) are necessary" << std::endl;
			}
			if(vm.count("c") == 0)
			{
				retval = false;
				std::cerr << "If not in 'Standalone' mode 'Initial Conditions'(--c) are necessary" << std::endl;
			}
		}
		if(vm.count("verbose") == 0)
		{
			verbosity = 1;
		}
		else
		{
			verbosity = std::min(3U,
			                     verbosity);
		}
		return retval;
	}
}

namespace DHCP::Server
{
	uint32_t verbosity;
}

int
main(int argc,
     char** argv)
{
	
	int32_t     port_no = 0;
	uint32_t    problem_time_span;
	uint32_t    constraint_time_span;
	std::string domain_file_name;
	std::string initial_file;
	std::string goal_file_name;
	std::string domain_insight_file_name;
	std::string clingo_parameters;
	std::string initial_conditions;
	
	//Received command parameters
	std::string executed_command;
	for(int     i       = 0;
	    i < argc;
	    i++)
	{
		executed_command += argv[i];
		executed_command += " ";
	}
	
	if(read_command_line(argc,
	                  argv,
	                  port_no,
	                  problem_time_span,
	                  constraint_time_span,
	                  domain_file_name,
	                  goal_file_name,
	                  domain_insight_file_name,
	                  clingo_parameters,
	                  initial_conditions,
	                  DHCP::Server::verbosity))
	{
		if(DHCP::Server::verbosity > 0 )
		{
			initialize_logger("server_" + std::to_string(port_no));//TODO add port
		}
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Command line execution: " << executed_command;
		}
		//--------------------------------------------------------------------------------------
		//PARSE CLINGO ARGUMENTS
		std::vector<std::string> clingo_parameters_vec;
		{
			auto found = clingo_parameters.find(';');
			while(std::string::npos != found)
			{
				clingo_parameters_vec.emplace_back(clingo_parameters.substr(0,
				                                                            found));
				clingo_parameters = clingo_parameters.substr(found + 1);
				found             = clingo_parameters.find(';');
			}
			if(!clingo_parameters.empty())
			{
				clingo_parameters_vec.emplace_back(clingo_parameters);
			}
		}  //--------------------------------------------------------------------------------------
		//PARSE INITIAL CONDITIONS
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Parsing Initial Conditions";
		}
		auto constraints = DHCP::parse_initial_constraints(initial_conditions);
		
		//--------------------------------------------------------------------------------------
		
		if(-1 != port_no)
		{
			Scontrol::GRPC_call_manager server(static_cast<uint32_t>(port_no));
			server.Run();
		}
		else
		{
			auto               dif = std::make_shared<DHCP::DIF const>(DHCP::get_file_content(domain_insight_file_name));
			std::vector<char*> args;
			//Convert std::vector<std::string> to argv
			for(auto const& ca:clingo_parameters_vec)
			{
				auto* arg = new char[ca.size() + 1];
				std::copy(ca.begin(),
				          ca.end(),
				          arg);
				arg[ca.size()] = '\0';
				args.push_back(arg);
			}
			args.push_back(0);
			
			DHCP::Server::Solver solver(args,
			                            dif,
			                            DHCP::get_file_content(domain_file_name),
			                            DHCP::get_file_content(goal_file_name),
			                            problem_time_span,
			                            constraint_time_span,
			                            0);
			
			auto solution = solver.solve(constraints);
			
			std::cout << std::endl;
			std::cout << "Total solution problem_time_span: " << solution->_total_time << "(ms)" << std::endl;
			//		LOG(INFO) << "Total solution problem_time_span: " << solution->_total_time << "(ms)";
			std::cout << "CLINGO solution problem_time_span: " << solution->_clingo_time << "(ms)" << std::endl;
			//		LOG(INFO) << "CLINGO solution problem_time_span: " << solution->_clingo_time << "(ms)";
			std::cout << solution->to_string() << std::endl;
			//		LOG(INFO) << solution->to_string();
		}
	}
	return 0;
}