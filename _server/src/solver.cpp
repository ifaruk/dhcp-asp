//
// Created by f on 09/10/17.
//


#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <chrono>
#include <regex>

#include <solver.h>
#include <logger.h>

namespace DHCP::Server
{
	
	Solver::Solver(std::vector<char*> clingo_arguments,
	               DIF_c_sp dif,
	               std::string domain_data,
	               std::string goal_data,
	               std::uint32_t problem_time_span,
	               std::uint32_t constraint_time_span,
	               uint32_t id)
		: _logger([id](Clingo::WarningCode,
		               char const* message)
		          {
			          if(DHCP::Server::verbosity >= 3)
			          {
				          LOG(INFO) << "Solver(" << id << "): " << "Clingo :" << message;
			          }
		          }),
		  _ctl({& clingo_arguments[0],///TODO clingo_arguments will be given
		        clingo_arguments.size()},
		       _logger,
		       100),
		  _dif(dif),
		  _problem_makespan(problem_time_span),
		  _constraint_makespan(constraint_time_span),
		  _id(id),
		  _external_atoms(),
		  _all_problem(prepare_domain(std::move(domain_data),
		                              _external_atoms,
		                              std::move(goal_data),
		                              problem_time_span,
		                              constraint_time_span))
	{
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Solver(" << _id << "): " << "Problem makespan " << _problem_makespan << " ,Prepared domain: "
			          << _all_problem;
		}
		_ctl.add("base",
		         {},
		         _all_problem.c_str());
		
		_ctl.ground({{"base", {}}});
		_ctl.cleanup();
		
		for(auto& a:_external_atoms)
		{
			_ctl.assign_external(Clingo::parse_term(a.to_string().c_str()),
			                     Clingo::TruthValue::False);
		}
		
		//Do a dummy solving to increase the speed of next work.
		//		_ctl.solve();
	}
	
	Solver::~Solver()
	{
		_ctl.interrupt();
	}
	
	/**
	 *
	 * @param initial_state each string of vector is a fluent indicating initial
	 * state of current problem. The Fluents should not have a time argument.
	 * Ex. If structure for Fluent 'Open' is
	 * 		Open(X,Y,T)
	 * where the time is indicated by variable 'T'. Fluents must be provided in
	 * following form
	 * 		Open(X,Y)
	 */
	
	Solver::Solution_sp
	Solver::solve(Time_sequence_sp constraints)
	{
		uint32_t         clingo_duration;
		Time_sequence_sp solution_atoms;
		auto             solve_start = std::chrono::steady_clock::now();
		
		
		//Add time argument
		std::vector<Clingo::Symbol> is_symbols;//initial state symbols
		std::string                 problem_str;
		if(constraints)
		{
			for(uint32_t i = 0;
			    i <= _constraint_makespan && i < constraints->size();
			    i++)
			{
				for(auto const& is:* (* constraints)[i])
				{
					
					auto sym = Clingo::parse_term(get_external(get_timed(is.to_string(),
					                                                     i)).c_str());
					_ctl.assign_external(sym,
					                     Clingo::TruthValue::True);
					is_symbols.push_back(sym);
					problem_str += get_timed(is.to_string(),
					                         i) + ",";
				}
			}
		}
		else
		{
			if(DHCP::Server::verbosity >= 1)
			{
				LOG(FATAL) << "Constraint shared_ptr is empty!";
			}
			throw std::invalid_argument("Constraint shared_ptr is empty!");
		}
		
		//Measure time
		auto clingo_start = std::chrono::steady_clock::now();
		auto resp         = get_solution(_ctl);
		clingo_duration = static_cast<uint32_t>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - clingo_start).count());
		//Clean Up External Atoms
		for(auto s:is_symbols)
		{
			_ctl.assign_external(s,
			                     Clingo::TruthValue::False);
		}
		
		bool                 satisfiable      = resp.first;
		Clingo::SymbolVector solution_symbols = resp.second;
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Solver(" << _id << "): " << "Time: " << clingo_duration << " ms.";
		}
		//Convert set of symbols to time sequence
		solution_atoms = !solution_symbols.empty() ? solution_to_time_sequence(solution_symbols,
		                                                                       _problem_makespan) : std::make_shared<Time_sequence>();
		
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Solver(" << _id << "): " << "Solution Atoms:" << std::endl << to_string(* solution_atoms);
		}
		
		
		//Filter all the atoms after goal statement
		auto goal_time_sequence = std::make_shared<Time_sequence>(filter(* solution_atoms.get(),
		                                                                 Predicate_set({Predicate("goal",//TODO make "goal" parametric
		                                                                                          0)})));
		
		//Find where the goal atom is
		size_t goal_index;
		auto   goal_loc_iter    = std::find_if(goal_time_sequence->begin(),
		                                       goal_time_sequence->end(),
		                                       [](Time_step_sp const& s)
		                                       {return s->size() > 0;});
		
		if(goal_time_sequence->end() != goal_loc_iter)
		{
			goal_index = goal_loc_iter - goal_time_sequence->begin();
			//			//Delete the rest of the atoms
			//			solution_atoms->erase(solution_atoms->begin() + goal_index + 1,
			//			                      solution_atoms->end());
			
			uint32_t solve_duration = static_cast<uint32_t>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - solve_start).count() / 1000.0);
			auto     solution       = std::make_shared<Solution>(satisfiable,
			                                                     std::make_shared<Time_sequence>(filter(* solution_atoms.get(),
			                                                                                            _dif->get_fluent_predicates())),
			                                                     std::make_shared<Time_sequence>(filter(* solution_atoms.get(),
			                                                                                            _dif->get_act_actuation_predicates())),
			                                                     std::make_shared<Time_sequence>(filter(* solution_atoms.get(),
			                                                                                            _dif->get_act_sensing_predicates())),
			                                                     std::make_shared<Time_sequence>(filter(* solution_atoms.get(),
			                                                                                            _dif->get_outcome_predicates())),
			                                                     std::make_shared<Time_sequence>(filter(* solution_atoms.get(),
			                                                                                            _dif->get_additional_predicates())),
			                                                     std::move(clingo_duration),
			                                                     std::move(solve_duration));
			
			
			//Remove the actions occurring in goal state
			solution->_act_actuation->at(goal_index)->clear();
			solution->_act_sensing->at(goal_index)->clear();
			
			return solution;
		}
		else
		{
			uint32_t solve_duration = static_cast<uint32_t>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - solve_start).count() / 1000.0);
			return std::make_shared<Solution>(satisfiable,
			                                  std::make_shared<Time_sequence>(),
			                                  std::make_shared<Time_sequence>(),
			                                  std::make_shared<Time_sequence>(),
			                                  std::make_shared<Time_sequence>(),
			                                  std::make_shared<Time_sequence>(),
			                                  std::move(clingo_duration),
			                                  std::move(solve_duration));
		}
	}
	
	void
	Solver::cancel()
	{
		//TODO needs testing
		_ctl.interrupt();
		
		if(DHCP::Server::verbosity >= 3)
		{
			LOG(INFO) << "Solver(" << _id << "): " << "Cancelled";
		}
		//		_ctl.interrupt();
		//		_ctl.solve();
	}
	
	Time_sequence_sp
	Solver::solution_to_time_sequence(Clingo::SymbolVector const& v,
	                                  uint32_t time)
	{
		Time_sequence_sp ret = std::make_shared<Time_sequence>();
		ret->reserve(time);
		for(uint32_t i = 0;
		    i <= time;
		    i++)
		{
			ret->emplace_back(std::make_shared<Time_step>());
		}
		
		for(auto const& a:v)
		{
			
			//Get the timeIt must always be last argument
			int t = std::stoi(a.arguments().back().to_string());
			
			auto atom = (a.is_negative() ? "-" : "") + std::string(a.name());
			if(a.arguments().size() > 1)
			{
				atom += "(";
				for(int i = 0;
				    i < a.arguments().size() - 1;
				    i++)//-1 to remove time parameters
				{
					atom += a.arguments()[i].to_string() + ",";
				}
				atom.back() = ')';
			}
			
			if(t <= time)
			{
				(* ret)[t]->emplace(atom);
			}
		}
		return ret;
	}
	
	void
	Solver::add_external_atoms(std::string& domain,
	                           Atom_set& external_atoms,
	                           Atom_set const& to_be_external_atoms,
	                           uint32_t time_span,
	                           bool fact)
	{
		//Introduce time span
		domain += "\n";
		for(uint32_t i = 0;
		    i <= time_span;
		    i++)
		{
			std::for_each(to_be_external_atoms.cbegin(),
			              to_be_external_atoms.cend(),
			              [&domain,
			               &external_atoms,
			               &i,
			               fact](Atom const& a)
			              {
				              auto timed = get_timed(a.to_string(),
				                                     i);
				              auto ext   = get_external(timed);
				              external_atoms.emplace(ext);
				              domain += "#external " + ext + ".";
				              if(fact)
				              {
					              domain += timed + ":- " + ext + ".";
				              }
				              else
				              {
					              domain += ":- " + ext + ", not " + timed + ".";
				              }
			              });
			domain += "\n";
		}
	}
	
	void
	Solver::add_ext_show_statement(std::string& domain,
	                               Predicate const& predicate)
	{
		domain += "#show " + predicate.name() + "/" + std::to_string(predicate.no_param()) + ".\n";
	}
	
	void
	Solver::add_show_statements(std::string& domain,
	                            Predicate_set const& predicates)
	{
		for(auto const& p:predicates)
		{
			domain += "#show " + p.name() + "/" + std::to_string(p.no_param() + 1) + ".\n";
		}
	}
	
	std::string
	Solver::prepare_domain(std::string new_domain,
	                       Atom_set& external_atoms,
	                       std::string goal,
	                       uint32_t problem_time_span,
	                       uint32_t constraint_time_span)
	{
		//Remove all #show statements
		{
			std::string::size_type found = 0;
			while(true)
			{
				found          = new_domain.find("#show",
				                                 found);//Start from same location, since we have removed previous one
				if(std::string::npos == found)
				{
					break;
				}
				auto dot_found = new_domain.find('.',
				                                 found + 5);//5 is the size of "#show" string
				//@formatter:off
			new_domain = (found > 0 ? new_domain.substr(0,found - 1) : "")
			             + (dot_found < new_domain.size() ? new_domain.substr(dot_found + 1) : "");
			//@formatter:on
			}
		}
		//Add problem_time_span atom
		new_domain = "time(0.." + std::to_string(problem_time_span) + ").\n" + new_domain;
		
		//Add all external atoms
		add_external_atoms(new_domain,
		                   external_atoms,
		                   _dif->get_fluent_atoms(),
		                   constraint_time_span,
		                   true);
//		add_external_atoms(new_domain,
//		                   external_atoms,
//		                   _dif->get_act_actuation_atoms(),
//		                   constraint_makespan);
		add_external_atoms(new_domain,
		                   external_atoms,
		                   _dif->get_act_sensing_atoms(),
		                   constraint_time_span);
		add_external_atoms(new_domain,
		                   external_atoms,
		                   _dif->get_outcome_atoms(),
		                   constraint_time_span);
//		add_external_atoms(new_domain,
//		                   external_atoms,
//		                   _dif->get_additional_atoms(),
//		                   constraint_makespan);
		//Add Show statements
		add_show_statements(new_domain,
		                    _dif->get_fluent_predicates());
		add_show_statements(new_domain,
		                    _dif->get_act_actuation_predicates());
		add_show_statements(new_domain,
		                    _dif->get_act_sensing_predicates());
		add_show_statements(new_domain,
		                    _dif->get_outcome_predicates());
		add_show_statements(new_domain,
		                    _dif->get_additional_predicates());
		
		//		for(auto& a:external_atoms)
		//		{
		//			add_ext_show_statement(new_domain,
		//			                       a._predicate);
		//		}
		
		new_domain += goal;
		
		std::string::iterator new_end = std::unique(new_domain.begin(),
		                                            new_domain.end(),
		                                            [](char lhs,
		                                               char rhs) -> bool
		                                            {return std::isspace(lhs) && std::isspace(rhs) && lhs == rhs;});
		new_domain.erase(new_end,
		                 new_domain.end());
		
		return new_domain;
	}
	
	std::string
	Solver::get_external(std::string const& atom)
	{
		return atom[0] != '-' ? "ex_" + atom : "ex_n_" + atom.substr(1);
	}
	
	std::string
	Solver::get_timed(std::string const& atom,
	                  uint32_t time)
	{
		return atom.substr(0,
		                   atom.size() - 1) + "," + std::to_string(time) + ")";
	}
}