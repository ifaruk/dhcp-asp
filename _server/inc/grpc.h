//
// Created by f on 22/11/17.
//

#ifndef DHCP_ASP_GRPC_H
#define DHCP_ASP_GRPC_H

#include <logger.h>
#include <server.h>

#include <grpc++/grpc++.h>
#include <client_server.grpc.pb.h>

namespace DHCP::Server
{
	extern uint32_t verbosity;
}

namespace Scontrol
{
	class GRPC_call_manager;
	
	class Call_manager
	{
	public:
		virtual void
		Proceed(bool new_instance)=0;
	};
	
	template<class Request,
	         class Response>
	class Generic_call_manager: public Call_manager
	{
	public:
		
		// Take in the "service" instance (in this case representing an asynchronous
		// server) and the completion queue "cq" used for asynchronous communication
		// with the gRPC runtime.
		Generic_call_manager(Server::AsyncService* service,
		                     grpc::ServerCompletionQueue* cq)
			: _service(service),
			  _cq(cq),
			  _responder(& _ctx),
			  _status(CallStatus::PROCESS)
		{
		}
		
		virtual ~Generic_call_manager()
		{}
		
		void
		Proceed(bool new_instance) final
		{
			if(_status == CallStatus::PROCESS)
			{
				if(new_instance)
				{
					create_new_call();
				}
				_status  = CallStatus::FINISH;
				auto ret = generate_response(_request);
				_responder.Finish(* ret.second,
				                  ret.first,
				                  this);
			}
			else
			{
				if(CallStatus::FINISH != _status)
				{
					std::stringstream ss;
					ss << "State is not 'Finished' of " << this;
					
					if(DHCP::Server::verbosity >= 1)
					{
						LOG(FATAL) << "State is not 'Finished'";
					}
					throw std::runtime_error(ss.str());
				}
				
				// Once in the FINISH state, deallocate ourselves (CallData).
				if(DHCP::Server::verbosity >= 3)
				{
					LOG(INFO) << "Deleting Call Manager " << this;
				}
				delete this;
			}
		}
	
	protected:
		
		virtual void
		create_new_call()=0;
		
		virtual std::pair<grpc::Status,
		                  std::shared_ptr<Response>>
		generate_response(Request const&)=0;
		
		Server::AsyncService       * _service;
		grpc::ServerCompletionQueue* _cq;
		grpc::ServerContext                       _ctx;
		Request                                   _request;
		grpc::ServerAsyncResponseWriter<Response> _responder;
	
	private:
		// Let's implement a tiny state machine with the following states.
		enum class CallStatus
		{
			PROCESS, FINISH
		};
		
		CallStatus _status;  // The current serving state.
	};
	
	//====================================================================+
	
	class CM_ping: public Generic_call_manager<Ping,
	                                           Pong>
	{
	public:
		
		CM_ping(Server::AsyncService* service,
		        grpc::ServerCompletionQueue* cq)
			: Generic_call_manager(service,
			                       cq)
		{
			_service->Requestping(& _ctx,
			                      & _request,
			                      & _responder,
			                      _cq,
			                      _cq,
			                      this);
		}
		
		~CM_ping() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_ping(_service,
			                     _cq);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_ping) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Pong>>
		generate_response(Ping const&) final;
	};
	
	//====================================================================+
	
	class CM_get_server_features: public Generic_call_manager<Server_feature_req,
	                                                          Server_feature_resp>
	{
	public:
		
		CM_get_server_features(Server::AsyncService* service,
		                       grpc::ServerCompletionQueue* cq)
			: Generic_call_manager(service,
			                       cq)
		{
			_service->Requestget_server_features(& _ctx,
			                                     & _request,
			                                     & _responder,
			                                     _cq,
			                                     _cq,
			                                     this);
		}
		
		~CM_get_server_features() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_get_server_features(_service,
			                                    _cq);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_get_server_features) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Server_feature_resp>>
		generate_response(Server_feature_req const&) final;
	};
	
	//====================================================================+
	
	class CM_get_server_state: public Generic_call_manager<Server_state_req,
	                                                       Server_state_resp>
	{
	public:
		
		CM_get_server_state(Server::AsyncService* service,
		                    grpc::ServerCompletionQueue* cq)
			: Generic_call_manager(service,
			                       cq)
		{
			_service->Requestget_server_state(& _ctx,
			                                  & _request,
			                                  & _responder,
			                                  _cq,
			                                  _cq,
			                                  this);
		}
		
		~CM_get_server_state() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_get_server_state(_service,
			                                 _cq);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_get_server_state) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Server_state_resp>>
		generate_response(Server_state_req const&) final;
	};
	
	//====================================================================+
	
	class CM_configure_server: public Generic_call_manager<Configure_server_req,
	                                                       Configure_server_resp>
	{
	public:
		
		CM_configure_server(Server::AsyncService* service,
		                    grpc::ServerCompletionQueue* cq,
		                    DHCP::Server::Server_sp_sp dhcp_server,
		                    std::mutex& dhcp_server_slock,
		                    GRPC_call_manager& call_manager)
			: Generic_call_manager(service,
			                       cq),
			  _dhcp_server(dhcp_server),
			  _dhcp_server_mtx(dhcp_server_slock),
			  _call_manager(call_manager)
		{
			_service->Requestconfigure_server(& _ctx,
			                                  & _request,
			                                  & _responder,
			                                  _cq,
			                                  _cq,
			                                  this);
		}
		
		~CM_configure_server() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_configure_server(_service,
			                                 _cq,
			                                 _dhcp_server,
			                                 _dhcp_server_mtx,
			                                 _call_manager);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_configure_server) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Configure_server_resp>>
		generate_response(Configure_server_req const&) final;
	
	private:
		std::mutex& _dhcp_server_mtx;
		DHCP::Server::Server_sp_sp _dhcp_server;
		GRPC_call_manager& _call_manager;
	};
	
	//====================================================================+
	
	class CM_reset_server: public Generic_call_manager<Reset_server_req,
	                                                   Reset_server_resp>
	{
	public:
		
		CM_reset_server(Server::AsyncService* service,
		                grpc::ServerCompletionQueue* cq,
		                DHCP::Server::Server_sp_sp dhcp_server,
		                std::mutex& dhcp_server_slock,
		                GRPC_call_manager& call_manager)
			: Generic_call_manager(service,
			                       cq),
			  _dhcp_server(dhcp_server),
			  _dhcp_server_mtx(dhcp_server_slock),
			  _call_manager(call_manager)
		{
			_service->Requestreset_server(& _ctx,
			                              & _request,
			                              & _responder,
			                              _cq,
			                              _cq,
			                              this);
		}
		
		~CM_reset_server() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_reset_server(_service,
			                             _cq,
			                             _dhcp_server,
			                             _dhcp_server_mtx,
			                             _call_manager);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_reset_server) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Reset_server_resp>>
		generate_response(Reset_server_req const&) final;
	
	private:
		std::mutex& _dhcp_server_mtx;
		DHCP::Server::Server_sp_sp _dhcp_server;
		GRPC_call_manager& _call_manager;
	};
	
	//====================================================================+
	
	class CM_terminate_server: public Generic_call_manager<Terminate_server_req,
	                                                       Terminate_server_resp>
	{
	public:
		
		CM_terminate_server(Server::AsyncService* service,
		                    grpc::ServerCompletionQueue* cq,
		                    DHCP::Server::Server_sp_sp dhcp_server,
		                    std::mutex& dhcp_server_slock,
		                    GRPC_call_manager& call_manager)
			: Generic_call_manager(service,
			                       cq),
			  _dhcp_server(dhcp_server),
			  _dhcp_server_mtx(dhcp_server_slock),
			  _call_manager(call_manager)
		{
			_service->Requestterminate_server(& _ctx,
			                                  & _request,
			                                  & _responder,
			                                  _cq,
			                                  _cq,
			                                  this);
		}
		
		~CM_terminate_server() override = default;
		
		void
		create_new_call() final
		{
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Terminate_server_resp>>
		generate_response(Terminate_server_req const&) final;
	
	private:
		std::mutex& _dhcp_server_mtx;
		DHCP::Server::Server_sp_sp _dhcp_server;
		GRPC_call_manager& _call_manager;
	};
	
	//====================================================================+
	
	class CM_new_time_solver: public Generic_call_manager<New_time_solver_req,
	                                                      New_time_solver_resp>
	{
	public:
		CM_new_time_solver(Server::AsyncService* service,
		                   grpc::ServerCompletionQueue* cq,
		                   DHCP::Server::Server_sp_sp dhcp_server)
			: Generic_call_manager(service,
			                       cq),
			  _dhcp_server(dhcp_server)
		{
			_service->Requestnew_time_solver(& _ctx,
			                                 & _request,
			                                 & _responder,
			                                 _cq,
			                                 _cq,
			                                 this);
		}
		
		~CM_new_time_solver() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_new_time_solver(_service,
			                                _cq,
			                                _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_new_time_solver) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<New_time_solver_resp>>
		generate_response(New_time_solver_req const&) final;
	
	private:
		DHCP::Server::Server_sp_sp _dhcp_server;
	};
	
	//====================================================================+
	
	class CM_kill_time_solver: public Generic_call_manager<Kill_time_solver_req,
	                                                       Kill_time_solver_resp>
	{
	public:
		CM_kill_time_solver(Server::AsyncService* service,
		                    grpc::ServerCompletionQueue* cq,
		                    DHCP::Server::Server_sp_sp dhcp_server)
			: Generic_call_manager(service,
			                       cq),
			  _dhcp_server(dhcp_server)
		{
			_service->Requestkill_time_solver(& _ctx,
			                                  & _request,
			                                  & _responder,
			                                  _cq,
			                                  _cq,
			                                  this);
		}
		
		~CM_kill_time_solver() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_kill_time_solver(_service,
			                                 _cq,
			                                 _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_kill_time_solver) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Kill_time_solver_resp>>
		generate_response(Kill_time_solver_req const&) final;
	
	private:
		DHCP::Server::Server_sp_sp _dhcp_server;
	};
	
	
	//====================================================================+
	
	class CM_solve: public Generic_call_manager<Solve_req,
	                                            Solve_resp>
	{
	public:
		CM_solve(Server::AsyncService* service,
		         grpc::ServerCompletionQueue* cq,
		         DHCP::Server::Server_sp_sp dhcp_server)
			: Generic_call_manager(service,
			                       cq),
			  _dhcp_server(dhcp_server)
		{
			_service->Requestsolve(& _ctx,
			                       & _request,
			                       & _responder,
			                       _cq,
			                       _cq,
			                       this);
		}
		
		~CM_solve() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_solve(_service,
			                      _cq,
			                      _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_solve) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Solve_resp>>
		generate_response(Solve_req const&) final;
	
	private:
		DHCP::Server::Server_sp_sp _dhcp_server;
	};
	
	//====================================================================+
	
	class CM_cancel: public Generic_call_manager<Cancel_req,
	                                             Cancel_resp>
	{
	public:
		CM_cancel(Server::AsyncService* service,
		          grpc::ServerCompletionQueue* cq,
		          DHCP::Server::Server_sp_sp dhcp_server)
			: Generic_call_manager(service,
			                       cq),
			  _dhcp_server(dhcp_server)
		{
			_service->Requestcancel(& _ctx,
			                        & _request,
			                        & _responder,
			                        _cq,
			                        _cq,
			                        this);
		}
		
		~CM_cancel() override = default;
		
		void
		create_new_call() final
		{
			auto r = new CM_cancel(_service,
			                       _cq,
			                       _dhcp_server);
			if(DHCP::Server::verbosity >= 3)
			{
				LOG(INFO) << "Created Call Manager (CM_cancel) " << r;
			}
		}
		
		std::pair<grpc::Status,
		          std::shared_ptr<Cancel_resp>>
		generate_response(Cancel_req const&) final;
	
	private:
		DHCP::Server::Server_sp_sp _dhcp_server;
	};
	
	
	//====================================================================+
	//====================================================================+
	
	class GRPC_call_manager final
	{
	public:
		explicit GRPC_call_manager(uint32_t port_no);
		
		~GRPC_call_manager();
		
		// There is no shutdown handling in this code.
		void
		Run();
	
	private:
		friend class CM_configure_server;
		
		friend class CM_reset_server;
		
		friend class CM_terminate_server;
		
		static uint32_t const _least_no_parallel_cm = 1;
		
		// This can be run in multiple threads if needed.
		void
		HandleRpcs(std::shared_ptr<bool> terminate_signal);
		
		void
		set_serving_threads(uint32_t no);
		
		void
		terminate();
		
		std::mutex                                   _dhcp_server_mtx;
		DHCP::Server::Server_sp_sp                   _dhcp_server;
		uint32_t                                     _port_no;
		uint32_t                                     _parallel_capacity;
		bool                                         _done;
		std::condition_variable                      _done_cv;
		std::unique_ptr<grpc::ServerCompletionQueue> _cq;
		Server::AsyncService                         _service;
		std::unique_ptr<grpc::Server>                _server;
		
		std::mutex                                _call_handler_mtx;
		std::vector<std::shared_ptr<bool>>        _main_call_handler_terminate;
		std::vector<std::shared_ptr<std::thread>> _main_call_handlers;
		std::vector<std::shared_ptr<bool>>        _additional_call_handler_terminate;
		std::vector<std::shared_ptr<std::thread>> _additional_call_handlers;
	};
}

#endif //DHCP_ASP_GRPC_H
