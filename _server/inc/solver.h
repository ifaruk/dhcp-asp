//
// Created by f on 09/10/17.
//

#ifndef DHCP_ASP_SOLVER_H
#define DHCP_ASP_SOLVER_H

#include <vector>
#include <string>
#include <sstream>
#include <thread>
#include <queue>

#include <logger.h>

#include <clingo.hh>
#include <utils.h>

namespace DHCP::Server
{
	extern uint32_t verbosity;
}

namespace DHCP::Server
{
	
	//===========================================================================
	
	class Solver
	{
	public:
		struct Solution
		{
			Time_sequence_sp _fluent;
			
			Solution(bool satisfiable,
			         Time_sequence_sp fluent,
			         Time_sequence_sp act_actuation,
			         Time_sequence_sp act_sensing,
			         Time_sequence_sp outcome,
			         Time_sequence_sp additional,
			         uint32_t clingo_time,
			         uint32_t total_time)
				: _satisfiable(satisfiable),
				  _fluent(std::move(fluent)),
				  _act_actuation(std::move(act_actuation)),
				  _act_sensing(std::move(act_sensing)),
				  _outcome(std::move(outcome)),
				  _additional(std::move(additional)),
				  _clingo_time(clingo_time),
				  _total_time(total_time)
			{}
			
			std::string
			to_string()
			{
				std::stringstream all_solution;
				all_solution << (_satisfiable ? "SATISFIABLE" : "UNSATISFIABLE") << " with clingo in " << _clingo_time << "ms";
				for(auto const& tseq:{std::make_pair(_fluent,
				                                     "Fluents"),
				                      std::make_pair(_act_actuation,
				                                     "Actuation Actions"),
				                      std::make_pair(_act_sensing,
				                                     "Sensing Actions"),
				                      std::make_pair(_outcome,
				                                     "Outcomes"),
				                      std::make_pair(_additional,
				                                     "Additional")})
				{
					all_solution << "----------------------------------------------------" << std::endl;
					all_solution << tseq.second << std::endl;
					all_solution << ::DHCP::to_string(* tseq.first) << std::endl;
				}
				return all_solution.str();
			}
			
			bool             _satisfiable;
			Time_sequence_sp _act_actuation;
			Time_sequence_sp _act_sensing;
			Time_sequence_sp _outcome;
			Time_sequence_sp _additional;
			uint32_t         _clingo_time;
			uint32_t         _total_time;
		};
	
	public:
		Solver(std::vector<char*> clingo_arguments,
		       DIF_c_sp dif,
		       std::string domain_data,
		       std::string goal_data,
		       uint32_t problem_time_span,
		       uint32_t constraint_time_span,
		       uint32_t id);
		
		~Solver();
		
		using Solution_sp = std::shared_ptr<Solution>;
		
		Solution_sp //total time
		solve(Time_sequence_sp constraints);
		
		void
		cancel();
		
		uint32_t
		get_problem_makespan()
		{return _problem_makespan;}
		
		uint32_t
		get_constraint_makespan()
		{return _constraint_makespan;}
		
		void
		reserve()
		{
			if(!_reserved.try_lock())
			{
				if(DHCP::Server::verbosity >= 1)
				{
					LOG(FATAL)
						<< "Tried to lock again!";
				}
				throw std::logic_error("Tried to lock again!");
			}
		}
		
		void
		release()
		{_reserved.unlock();}
		
		static Time_sequence_sp
		solution_to_time_sequence(Clingo::SymbolVector const& v,
		                          uint32_t time);
		
		static void
		add_external_atoms(std::string& domain,
		                   Atom_set& external_atoms,
		                   Atom_set const& to_be_external_atoms,
		                   uint32_t time_span,
		                   bool fact = false);
		
		static void
		add_show_statements(std::string& domain,
		                    Predicate_set const& predicates);
		
		static void
		add_ext_show_statement(std::string& domain,
		                       Predicate const& predicate);
		
		static std::string
		get_external(std::string const& atom);
		
		static std::string
		get_timed(std::string const& atom,
		          uint32_t time);
	
	private:// Configuration
		Clingo::Logger  _logger;
		Clingo::Control _ctl;
		
		DIF_c_sp       _dif;
		uint32_t       _problem_makespan;
		uint32_t       _constraint_makespan;
		uint32_t const _id;
		Atom_set       _external_atoms;
		std::string    _all_problem;
	
	private: //State
		std::mutex _reserved;
	
	private:
		
		std::string
		prepare_domain(std::string domain,
		               Atom_set& external_atoms,
		               std::string goal,
		               uint32_t problem_time_span,
		               uint32_t constraint_time_span);

	};
	
	using Solver_sp =  std::shared_ptr<Solver>;
}

#endif //DHCP_ASP_SOLVER_H
