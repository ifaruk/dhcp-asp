//
// Created by f on 29/11/17.
//
#ifndef DHCP_ASP_SERVER_SERVER_H
#define DHCP_ASP_SERVER_SERVER_H

#include <string>
#include <vector>
#include <shared_mutex>
#include <set>

#include <utils.h>
#include <logger.h>
#include <solver.h>

namespace DHCP::Server
{
	extern uint32_t verbosity;
}

namespace DHCP::Server
{
	
	class Server
	{
	
	public:
		Server(const std::vector<std::string>& _clingo_arguments,
		       const std::string& _domain_data,
		       const std::string& _goal_data,
		       DIF_c_sp _dif,
		       const uint32_t& parallel_capacity);
		
		~Server();
		
		Solver::Solution_sp
		solve(uint32_t solver_id,
		      Time_sequence_sp constraints);
		
		bool
		cancel(uint32_t solver_id);
		
		/**
		 *
		 * @param problem_time_span:
		 * @param constraint_time_span: Lower the value, faster the solution
		 */
		uint32_t
		new_time_solver(uint32_t problem_time_span,
		                uint32_t constraint_time_span);
		
		void
		kill_time_solver(uint32_t solver_id);
		
		inline uint32_t
		get_parallelism()
		{return _parallel_capacity;}
	
	private:
		//Configuration
		std::string const  _domain_data;
		std::string const  _goal_data;
		DIF_c_sp           _dif;
		uint32_t const     _parallel_capacity;
		std::vector<char*> _args;
		
		std::mutex _workload_mtx;
		uint32_t   _current_workload;
		
		std::atomic<uint32_t> _next_solver_id;
		
		std::shared_mutex   _id_to_solver_smtx;
		std::map<uint32_t,//Solver ID
		         Solver_sp> _id_to_solver; // Solver vector
		
		std::mutex         _ongoing_solving_mtx;
		std::set<uint32_t> _ongoing_solving; //Solve handle
		
	};
	
	using Server_sp = std::shared_ptr<Server>;
	using Server_sp_sp = std::shared_ptr<Server_sp>;
}
#endif //DHCP_ASP_SERVER_SERVER_H
